import com.guy.statistics.yssusage.entity.YssUsage;
import com.guy.statistics.yssusage.service.YssUsageService;
import com.guy.system.areainfo.dao.AreaInfoDao;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.city.dao.CityDao;
import com.guy.system.city.entity.City;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.nio.DoubleBuffer;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author
 * @create 2018-05-04 9:12
 **/
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Transactional(readOnly = true)
public class TestMain {
    @Autowired
    private CityDao cityDao;
    @Autowired
    private AreaInfoDao areaInfoDao;

    @Autowired
    private YssUsageService yssUsageService;
    @Test
    public void Test(){
        List<YssUsage> yssUsageList =yssUsageService.getUsage("2018-05","2018-03-05","2018-05");

        List<Double> sbl=new ArrayList<>(0);
        for (YssUsage yssUsage:yssUsageList) {
           double result= (double)yssUsage.getUserNum()/(double)yssUsage.getQuotaNum();
            BigDecimal bg = new BigDecimal(result);
            double f1 = bg.setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
            sbl.add(f1);
        }
        System.out.println("最大值"+Collections.max(sbl));
        System.out.println("最小值"+Collections.min(sbl));
    }



//    @Test
//    public void testSelect() {
//        cityDao.loadCityByLevel(2);
//        String str = "北戴河";
//        List<AreaInfo> areaList = areaInfoDao.find("from AreaInfo where areaName like ?0 ", "%" + str + "%");
//        int pid = areaList.get(0).getPid();
//        boolean isDo = true;
//        AreaInfo areaInfo= null;
//        do {
//            if (pid != 2) {
//                areaInfo = areaInfoDao.find(pid);
//                pid = areaInfo.getPid();
//                if (pid == 2) {
//                    isDo = false;
//                }
//            }
//
//        } while (isDo);
//
//        System.out.println(areaInfo.getAreaName());
//    }

}
