$(function () {
    loadData(null);
});

function loadData(obj) {
    $('#p00').panel("setTitle", "云搜索0登录人员：");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/noLogjson",
        dataType: 'json',
        data: obj,
        success: function (data) {
            console.log(data);
            if (data != null) {
                var o = 0;
                var wdr = "";
                var column = 14;
                var row = Math.ceil(data.length / 14);//Math.ceil()向上取整,有小数就整数部分加1
                for (var i = 0; i < row; i++) {
                    wdr = wdr + "<dd class=fff>";
                    for (var j = 0; j < column; j++) {
                        var title="";
                        if (data[o] != undefined) {
                            if (data[o].infos.length > 1) {
                                $(data[o].infos).each(function (index, value) {
                                    if (index != data[o].infos.length) {
                                        title = title + "&nbsp;&nbsp;&nbsp;" + value.rq;
                                    }
                                });
                                wdr = wdr + "<a title='" + title + "' style='width:80px;display:inline-block;'>" + data[o].infos[0].name + "<font color=red>(" + data[o].infos.length + "次)</font></a>";
                            } else {
                                wdr = wdr + "<a title='" + data[o].infos[0].rq + "'style='width:80px;display:inline-block;'>" + data[o].infos[0].name + "</a>";
                            }
                        }
                        title = "";
                        o++;

                        /*	if(data[o]!=undefined){
                         if(data[o].count==1){
                         wdr=wdr+"<span title='"+data[o].rq+"'>"+data[o].name+" "+"</span>";
                         }else{
                         wdr=wdr+"<span title='"+data[o].rq+"'>"+data[o].name+" "+data[o].count+"</span>";
                         }
                         }else{
                         wdr=wdr+'<span>&nbsp;</span>';
                         }*/

                    }
                    wdr = wdr + "</dd>";
                }
                /* var wdr="<dd class=fff>"
                 $(data.categories).each(function(index,value){
                 wdr=wdr+'<span>'+value+'</span>';
                 })
                 wdr=wdr+"</dd>";*/
                $("#wdr1").html(wdr);
            }
        }
    });
    //console.log(obj)
    /*$('#p0').panel("setTitle","云搜索0登录人员：");
     $.ajax({
     type:'post',
     url:contextPath+"/statistics/cssinfo/nologjson",
     data:obj,
     success: function(data){
     if(data.categories!=null){
     var o=0;
     var wdr="";
     var column=18;
     var row=Math.ceil(data.categories.length/18);//Math.ceil()向上取整,有小数就整数部分加1
     for (var i=0;i<row;i++){
     wdr=wdr+"<dd class=fff>";
     for (var j=0;j<column;j++){
     if(data.categories[o]!=undefined){
     wdr=wdr+'<span>'+data.categories[o]+'</span>';
     }else{
     wdr=wdr+'<span>&nbsp;</span>';
     }
     o++;
     }
     wdr=wdr+"</dd>";
     } 
     var wdr="<dd class=fff>"
     $(data.categories).each(function(index,value){
     wdr=wdr+'<span>'+value+'</span>';
     })
     wdr=wdr+"</dd>";
     $("#wdr").html(wdr); 
     }

     }
     });*/

    $('#p1').panel("setTitle", "查询最多的人员");
    $.ajax({
        /* 	async:false, */
        type: 'post',
        url: contextPath + "/statistics/cssinfo/maxShjson",
        data: obj,
        success: function (data) {
            $('#chart1').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '查询最多的人员'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（次）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });
    $('#p2').panel("setTitle", "导出最多的人员");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/maxOutjson",
        data: obj,
        success: function (data) {
            $('#chart2').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '导出最多的人员'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（次）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });

    $('#p3').panel("setTitle", "查询_点击_导出_操作日志统计");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/ccojson",
        data: obj,
        success: function (data) {
            $('#chart3').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: '查询_点击_导出_操作日志统计'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '总数',
                    data: data.chartData
                }]
            });
        }
    });
    $('#p4').panel("setTitle", "各地市高级用户数统计");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/ucjson",
        data: obj,
        success: function (data) {
            $('#chart4').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '各地市高级用户数统计'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（人）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });

    $('#p5').panel("setTitle", "各地市查询次数统计");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/scjson",
        data: obj,
        success: function (data) {
            $('#chart5').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '各地市查询次数统计'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（次）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });

    $('#p6').panel("setTitle", "各地市导出次数统计");
    $.ajax({
        type: 'post',
        url: contextPath + "/statistics/cssinfo/scjson",
        data: obj,
        success: function (data) {
            $('#chart6').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '各地市导出次数统计'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（次）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });

    $('#p7').panel("setTitle", "操作日志最多的人员");
    $.ajax({
        /* 	async:false, */
        type: 'post',
        url: contextPath + "/statistics/cssinfo/rzcjson",
        data: obj,
        success: function (data) {
            $('#chart7').highcharts({
                chart: {
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '操作日志最多的人员'
                },
                xAxis: {
                    categories: data.categories,
                    labels: {
                        rotation: -45,
                        align: 'right',
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '单位：（次）'
                    }
                },
                legend: {
                    enabled: false
                },
                /*  tooltip: {
                 pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
                 }, */
                series: [{
                    name: '次数',
                    data: data.data,
                    dataLabels: {
                        enabled: true,
                        rotation: -45,
                        color: 'red',
                        align: 'center',
                        x: 4,
                        y: -10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px #fff'
                        }
                    }
                }]
            });
        }
    });
}
