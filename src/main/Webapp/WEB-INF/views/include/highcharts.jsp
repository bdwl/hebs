<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="${ctx}/static/plugins/Highcharts-4.0.3/js/highcharts.js"></script>
<script src="${ctx}/static/plugins/Highcharts-4.0.3/js/modules/exporting.src.js" charset="UTF-8"></script>
<script type="text/javascript">
<!--
Highcharts.setOptions({
    lang: {
          printChart:"打印图表",
          downloadJPEG: "下载JPEG 图片" , 
          downloadPDF: "下载PDF 文档"  ,
          downloadPNG: "下载PNG 图片"  ,
          downloadSVG: "下载SVG 矢量图" , 
          exportButtonTitle: "导出图片" 
    },
    credits:{//去除水印
 		enabled:false
 	},
    exporting: { enabled:false }//去除右上角导出功能
});
//-->
</script>
<!--
<script src="${ctx}/static/plugins/Highcharts-3.0.2/js/theme/dark-blue.js" type="text/javascript"></script>
<script src="${ctx}/static/plugins/Highcharts-3.0.2/js/theme/dark-green.js" type="text/javascript"></script>
<script src="${ctx}/static/plugins/Highcharts-3.0.2/js/theme/gray.js" type="text/javascript"></script>
<script src="${ctx}/static/plugins/Highcharts-3.0.2/js/theme/grid.js" type="text/javascript"></script>
<script src="${ctx}/static/plugins/Highcharts-3.0.2/js/theme/skies.js" type="text/javascript"></script>
-->