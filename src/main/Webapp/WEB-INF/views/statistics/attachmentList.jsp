<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">
            <input type="text" name="filter_LIKES_cssUser.sfzh" class="easyui-validatebox"
                   data-options="width:150,prompt: '身份证号'"/>
            <span class="toolbar-item dialog-tool-separator"></span>
            <a href="javascript(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="cx()">查询</a>
        </form>
        <shiro:hasPermission name="statistics:attachment:delete">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="delAtt"
               data-options="disabled:false" onclick="del()">删除</a>
        </shiro:hasPermission>
    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<div id="dd"></div>
<script type="text/javascript">
    var dg;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/attachment/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 50,
            pageList: [5, 10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {
                    field: 'yhxm', title: '用户姓名', width: 40,
                    formatter: function (value, row, index) {
                        return row.cssUser.name;
                    }
                },
                {
                    field: 'sfzh', title: '身份证号', width: 80,
                    formatter: function (value, row, index) {
                        return row.cssUser.sfzh;
                    }
                },
                {
                    field: 'dwmc', title: '单位', width: 120,
                    formatter: function (value, row, index) {
                        return row.cssUser.organization;
                    }
                },
                {
                    field: 'eltdate', title: '上传时间', width: 120
                },
                {
                    field: 'flag', title: '状态', width: 100,
                    formatter: function (value, row, index) {
                        if (value == 0) {
                            return "<font color='red'>待审核</font>";
                        } else {
                            return "已审核";
                        }
                    }
                },
                {
                    field: 'path', title: '附件', width: 100,
                    formatter: function (value, row, index) {
                        return "<a href='#'  onclick='showImg(" + row.id + ")'>查看</a>";
                    }
                }
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb',
            onClickRow: function (index, row) {
                if (row.flag == 0) {
                    $("#delAtt").linkbutton('enable');
                }
                if (row.flag == 1) {
                    $("#delAtt").linkbutton('disable');
                }

            }
        });
    });

    function showImg(data) {
        parent.openDlg(data);
    }

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/attachment/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/attachment/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }

    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/attachment/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

</script>
</body>
</html>