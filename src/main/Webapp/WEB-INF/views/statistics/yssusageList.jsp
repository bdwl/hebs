<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <form id="form" action="" method="post">
        <input type="text" name="batchrq" class="easyui-my97" datefmt="yyyy-MM"
               data-options="width:150,prompt: '批次'" value="${batchrq}"/>
        <input type="text" name="authDate" class="easyui-my97" datefmt="yyyy-MM-dd"
               data-options="width:150,prompt: '授权日期'" value="${authDate}"/>
        <%--<input type="text" name="quotaVersion" class="easyui-my97" datefmt="yyyy-MM"--%>
                                                                <%--data-options="width:150,prompt: '配额版本'"/>--%>
        <input id="cc" class="easyui-combobox" name="quotaVersion" value="${quotaVersion}"/>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit"
           plain="true"
           onclick="setParams()">设置</a>
    </form>
    <div>
        高级用户使用情况表
    </div>

</div>
<div id="tb2" style="padding:5px;height:auto">
    <div>
        各市应用情况综合得分表
    </div>

</div>
<table id="dg"></table>
<table id="dg2"></table>
<div id="dlg"></div>

<script type="text/javascript">
    function setParams() {
        $('#form').form('submit', {
            url: '${ctx}/statistics/yssusage/setParam',
            onSubmit: function () {
                var isValid = $(this).form('validate');
                return isValid;	// 返回false终止表单提交
            },
            success: function (data) {
                if (data == "success") {
                    $("#win").window("close");
                    window.location.reload(true);
                } else {
                    $.messager.alert("提示", data);
                }

            }
        });
    }
    /**
     *这里需要先用Number进行数据类型转换，然后去指定截取转换后的小数点后几位(按照四舍五入)，这里是截取一位，0.1266转换后会变成12.7%
     */
    function toPercent(point) {
        var str = Number(point * 100).toFixed(2);
        str += "%";
        return str;
    }


    var dg;
    var dg2;

    $(function () {
        var maxRate;
        var minRate;
        var maxCount;
        var minCount;
        var maxZeroPer;
        var minZeroPer;
        var maxLowUser;
        var minLowUser;
        $("#cc").combobox({
            valueField: 'label',
            textField: 'value',
            data: [{
                label: '第一季度',
                value: '第一季度',
                selected:true
            },{
                label: '第二季度',
                value: '第二季度'
            },{
                label: '第三季度',
                value: '第三季度'
            },{
                label: '第四季度',
                value: '第四季度'
            }]
        })


        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/yssusage/json',
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'areaName', title: '地市', sortable: true, width: 100},
                {field: 'amountTotal', title: '访问总量', sortable: true, width: 100},
                {field: 'userNum', title: '高级用户数', sortable: true, width: 100},
                {field: 'lt30UserNum', title: '授权时间小于30<br>天的高级用户人', sortable: true, width: 100},
                {
                    field: 'realUserNum', title: '实际统计高级用户数', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return row.userNum - row.lt30UserNum;
                    }
                },
                {field: 'loginZeroNum', title: '零登陆高级用户数', sortable: true, width: 100},
                {
                    field: 'loginZeroPer', title: '零登陆高级用户数占比', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return toPercent(row.loginZeroNum / (row.userNum - row.lt30UserNum));
                    }
                },
                {
                    field: 'useNum', title: '正常使用人数', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return row.userNum - row.lt30UserNum - row.loginZeroNum;
                    }
                },
                {
                    field: 'perCapita', title: '人均使用次数', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return Number(row.amountTotal / (row.userNum - row.lt30UserNum - row.loginZeroNum)).toFixed(0);
                    }
                },
                {field: 'lowNum', title: '低频使用户数', sortable: true, width: 100},
                {
                    field: 'lowNumPer', title: '低频使用用户数占比', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return toPercent(row.lowNum / (row.userNum - row.lt30UserNum));
                    }
                }
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb',
            loadFilter: function (data) {
                //当上报率大于1是则赋值为1
                if(data.maxRate>1){
                    maxRate = 1;
                }else{
                    maxRate = data.maxRate;
                }
                minRate = data.minRate;
                maxCount = data.maxCount;
                minCount = data.minCount;
                maxZeroPer = data.maxZeroPer;
                minZeroPer = data.minZeroPer;
                maxLowUser = data.maxLowUser;
                minLowUser = data.minLowUser;
                if (data.d) {
                    return data.d;
                } else {
                    return data;
                }
            }




        });
        dg2 = $('#dg2').datagrid({
            method: "post",
            url: '${ctx}/statistics/yssusage/json',
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'areaName', title: '地市', sortable: true, width: 100},
                {
                    field: 'quotaNum', title: '高级用户上报率', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return toPercent(row.userNum / value);
                    }
                },
                {
                    field: 'quotaPer', title: '得分（占总分20%）', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        var localRate = Number(row.userNum / row.quotaNum).toFixed(4);
                        return Number(20 * (1 - (maxRate - localRate) / (maxRate - minRate))).toFixed(2);
                    }
                },
                {
                    field: 'perCapita', title: '人均使用次数', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return Number(row.amountTotal / (row.userNum - row.lt30UserNum - row.loginZeroNum)).toFixed(0);
                    }
                },

                {field: 'capitaPointPer', title: '得分（占总分20%）', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        var localCount = Number(row.amountTotal / (row.userNum - row.lt30UserNum - row.loginZeroNum)).toFixed(0);

                        return Number(20 * (1 - (maxCount - localCount) / (maxCount - minCount))).toFixed(0);
                    }
                },
                {
                    field: 'loginZeroPer', title: '零登陆高级用户数占比', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return toPercent(row.loginZeroNum / (row.userNum - row.lt30UserNum));
                    }
                },
                {field: 'zeroPointPer', title: '得分（占总分30%）', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        var localPoint = Number(row.loginZeroNum / (row.userNum - row.lt30UserNum)).toFixed(4);
                        return Number(30 * ((maxZeroPer - localPoint) / (maxZeroPer - minZeroPer))).toFixed(2);
                    }
                },
                {
                    field: 'lowNumPer', title: '低频使用用户数占比', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return toPercent(row.lowNum / (row.userNum - row.lt30UserNum));
                    }
                },
                {field: 'lowPointPer', title: '得分（占总分30%）', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        var localLow = Number(row.lowNum / (row.userNum - row.lt30UserNum)).toFixed(4);
                        return Number(30 * (maxLowUser - localLow) / (maxLowUser - minLowUser)).toFixed(2);
                    }
                },
                {field: 'point', title: '总分', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        var ratePoint=0;
                        var countPoint=0;
                        var zeroPoint=0;
                        var lowPoint=0;
                        var localRate = Number(row.userNum / row.quotaNum).toFixed(4);
                        var localCount = Number(row.amountTotal / (row.userNum - row.lt30UserNum - row.loginZeroNum)).toFixed(0);
                        var localPoint = Number(row.loginZeroNum / (row.userNum - row.lt30UserNum)).toFixed(4);
                        var localLow = Number(row.lowNum / (row.userNum - row.lt30UserNum)).toFixed(4);
                         ratePoint = Number(20 * (1 - (maxRate - localRate) / (maxRate - minRate)) +20 * (1 - (maxCount - localCount) / (maxCount - minCount))
                       + 30 * ((maxZeroPer - localPoint) / (maxZeroPer - minZeroPer))
                        + 30 * (maxLowUser - localLow) / (maxLowUser - minLowUser)).toFixed(2);

                        return ratePoint;
                    }
                },
                {field: 'cnalnum', title: '采纳案例数', sortable: true, width: 100},
                {field: 'sqpatz', title: '授权配额调整', sortable: true, width: 100},


            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb2'
        });

    });

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/yssusage/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/yssusage/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }

    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/yssusage/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

</script>
</body>
</html>