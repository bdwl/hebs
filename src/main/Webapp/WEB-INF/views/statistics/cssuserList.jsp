<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>
<body>
<div id="tb" style="padding:5px;height:auto;display: none;">
    <div>
        <form id="searchFrom" action="${ctx}/statistics/cssUser/exportExcel" method="post">
            <input type="text" name="filter_LIKES_name" class="easyui-validatebox"
                   data-options="width:150,prompt: '姓名'"/>
            <input type="text" name="filter_LIKES_sfzh" class="easyui-validatebox"
                   data-options="width:150,prompt: '身份证号'"/>
            <input type="text" name="filter_LIKES_organization" class="easyui-validatebox"
                   data-options="width:150,prompt: '单位'"/>
            <input type="text" id="flag" name="filter_EQS_delFlag" class="easyui-validatebox"
                   data-options="width:80,prompt: '状态'"/>
            <input type="text" id="start" name="filter_GED_opdate" class="easyui-my97" datefmt="yyyy-MM-dd HH:mm"
                   data-options="width:150,prompt: '操作开始日期', maxDate: '%y-%M-%d'"/>
            - <input type="text" name="filter_LED_opdate" class="easyui-my97" datefmt="yyyy-MM-dd HH:mm"
                     data-options="width:150,prompt: '操作结束日期', maxDate: '%y-%M-%d'"/>

            <span class="toolbar-item dialog-tool-separator"></span>
            <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="cx()">查询</a>
            <a href="javascript(0)" class="easyui-linkbutton" iconCls="icon-standard-bin-empty" plain="true"
               onclick="reset()">清空</a>
        </form>

        <shiro:hasPermission name="statistics:cssuser:add">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-standard-user-add" plain="true"
               id="addbtn" onclick="add();">添加</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:reset">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-standard-key" plain="true" id="resbtn"
               data-options="disabled:false" onclick="resuser()">重新授权</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:delete">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-standard-database-delete" plain="true"
               data-options="disabled:false" onclick="del()">数据库删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:update">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
               onclick="upd()">修改</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:del">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-standard-user-delete" plain="true"
               id="delbtn" data-options="disabled:false" onclick="delf()">标记删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:mod">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-standard-user-edit" plain="true"
               onclick="mod()">变更</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:importInit">
            <a href="javascript:void(0)" onclick="$('#importInit_win').window('open')" class="easyui-linkbutton"
               data-options="iconCls:'icon-standard-vcard-add'" plain="true">导入云搜用户</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:import">
            <a href="javascript:void(0)" onclick="$('#import_win').window('open')" class="easyui-linkbutton"
               data-options="iconCls:'icon-standard-database-go'" plain="true">导入云搜数据</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:cssInfoView">
            <a href="javascript:void(0)" onclick="cssUserInfo()" class="easyui-linkbutton"
               data-options="iconCls:'icon-search'" plain="true">查看</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <%--         <shiro:hasPermission name="statistics:cssuser:cssInfoFlag">
                    <a href="javascript:void(0)" onclick="flag()" class="easyui-linkbutton" data-options="iconCls:'icon-search'" plain="true">查看</a>
                    <span class="toolbar-item dialog-tool-separator"></span>
                </shiro:hasPermission> --%>
        <shiro:hasPermission name="statistics:cssuser:exportAreaUser">
            <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-standard-page-excel"
               onclick="exportAreaUser()">导出Excel</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:export">
            <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-standard-page-excel"
               onclick="exportExcel()">导出Excel</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:exportuser">
            <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-standard-page-excel"
               onclick="exportExcelUser()">导出授权人员Excel</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:upatt">
            <a href="javascript:void(0)" onclick="updateWin()" class="easyui-linkbutton" id="upbtn"
               data-options="iconCls:'icon-standard-page-white-get'" plain="true">上传材料</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:audit">
            <a href="javascript:void(0)" onclick="audit()" class="easyui-linkbutton" id="auditbtn"
               data-options="iconCls:'icon-standard-tick'" plain="true">审核</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>

        <shiro:hasPermission name="statistics:cssuser:reject">
            <a href="javascript:void(0)" onclick="reject()" class="easyui-linkbutton" id="rejectbtn"
               data-options="iconCls:'icon-standard-arrow-undo'" plain="true">退回</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<div id="v_dlg"></div>
<div style="display: none;">
    <div id="importInit_win" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="_drhid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>导入数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="_drdiv">
                <form id="importInti_form" name="importInti_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <input id="_sj" type="file" name="sj" style="width: 260px;"/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="drsjInit()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <div id="import_win" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="drhid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>导入数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="drdiv">
                <form id="import_form" name="import_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2" style="padding: 10px 10px 10px 0px">
                                <input type="text" name="batchrq" class="easyui-my97" datefmt="yyyy-MM"
                                       data-options="width:150,prompt: '导入数据日期', maxDate: '%y-%M'"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input id="sj" type="file" name="sj" style="width: 260px;"/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="drsj()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <div id="importInfo_win" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="upid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>上传中数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="updiv">
                <form id="importInfo_form" name="importInfo_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <input id="files" type="file" name="sj" style="width: 260px;" multiple/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="uploadinfo()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!--右键菜单-->
    <div id="menu" class="easyui-menu" style="width: 50px; display: none;">
        <shiro:hasPermission name="statistics:cssuser:del">
            <div data-options="iconCls:'icon-standard-user-delete'" onclick="delf()">删除</div>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:update">
            <div data-options="iconCls:'icon-edit'" onclick="upd()">修改</div>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:mod">
            <div data-options="iconCls:'icon-standard-user-edit'" onclick="mod()">变更</div>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:audit">
            <div data-options="iconCls:'icon-standard-tick'" onclick="audit()">审核</div>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:cssuser:cssInfoView">
            <div data-options="iconCls:'icon-search'" onclick="cssUserInfo()">查看</div>
        </shiro:hasPermission>
    </div>
</div>
<script type="text/javascript">
    var dg;
    var d;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/cssUser/json',
            fit: true,
            fitColumns: true,
            border: false,
            idField: 'id',
            striped: true,
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 30,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                /* {field:'ck',checkbox:true}, */
                {field: 'id', title: 'id', hidden: true},
                {
                    field: 'userLeval', title: '用户级别', sortable: true, width: 60, align: 'center',
                    formatter: function (value, row, index) {
                        if (value == 1) {
                            return "<font color='red'><b>高级用户</b></font>";
                        }
                        if (value == 0) {
                            return "<font color='green'><b>普通用户</b></font>";
                        }
                    }
                },
                {field: 'name', title: '姓名', sortable: true, width: 60},
                {field: 'sfzh', title: '身份证号', sortable: true, width: 110},
                {field: 'policeClassification', title: '警种', sortable: true, width: 60,
                    formatter: function (value, row, index) {
                        if(value!=null){
                            return value.pcname;
                        }else{
                            return "";
                        }

                    }
                },
                {field: 'station', title: '岗位', sortable: true, width: 60,
                    formatter: function (value, row, index) {
                        if(value!=null){
                            return value.sname;
                        }else{
                            return "";
                        }
                    }},
                {field: 'organization', title: '单位', sortable: true, width: 160},
                {field: 'telphone', title: '联系电话', sortable: true, width: 80},
                {field: 'auditor', title: '审核人', sortable: true, width: 80},
                {field: 'auditunit', title: '审核单位', sortable: true, width: 120},
                {field: 'createdate', title: '创建时间', sortable: true, width: 100},
                {
                    field: 'expirydate', title: '有效期至', sortable: true, width: 80,
                    formatter: function (value, row, index) {
                        if (value != null && value != '') {
                            return "<font color='red'>" + value.substring(0, 10) + "</font>";
                        } else {
                            return "<font color='red'>永久</font>";
                        }
                    }
                },
                {field: 'opdate', title: '操作时间', sortable: true, width: 100},
                {
                    field: 'delFlag', title: '状态', sortable: true, width: 40, align: 'center',
                    formatter: function (value, row, index) {
                        if (value == "N") {
                            return "<font color='blue'>正常</font>";
                        } else if (value == "UP") {
                            return "<a href='#' onclick='updateWin()' style='text-decoration:none'><font color='#6CA6CD'>上传资料</font></a>";
                        } else if (value == "D") {
                            return "<font color='red'>删除</font>";
                        } else if (value == "ND") {
                            return "<font color='#993333'>待删除</font>";
                        } else if (value == "S") {
                            return "<font color='#CD1076'>待审核</font>";
                        } else if (value == "F") {
                            return "<font color='#2E8B57'>重新授权</font>";
                        }
                    }
                },

                {field: 'detail', title: '变更事由', sortable: true, width: 200,
                    formatter: function (value, row, index) {
                        if(value.search("退回")!= -1 ){
                            return "<font color='red'>"+value+"</font>";
                        }else{
                            return value;
                        }
                    }
                }
            ]],
            headerContextMenu: [
                {
                    text: "冻结该列", disabled: function (e, field) {
                    return dg.datagrid("getColumnFields", true).contains(field);
                },
                    handler: function (e, field) {
                        dg.datagrid("freezeColumn", field);
                    }
                },
                {
                    text: "取消冻结该列", disabled: function (e, field) {
                    return dg.datagrid("getColumnFields", false).contains(field);
                },
                    handler: function (e, field) {
                        dg.datagrid("unfreezeColumn", field);
                    }
                }
            ],
            enableHeaderClickMenu: true,
            enableHeaderContextMenu: true,
            enableRowContextMenu: false,
            rowTooltip: false,
            toolbar: '#tb',
            onRowContextMenu: function (e, rowIndex, rowData) { //右键时触发事件
                //三个参数：e里面的内容很多，真心不明白，rowIndex就是当前点击时所在行的索引，rowData当前行的数据
                e.preventDefault(); //阻止浏览器捕获右键事件
                $(this).datagrid("clearSelections"); //取消所有选中项
                $(this).datagrid("selectRow", rowIndex); //根据索引选中该行
                $('#menu').menu('show', {
//显示右键菜单
                    left: e.pageX,//在鼠标点击处显示菜单
                    top: e.pageY
                });
            },
            onDblClickRow: function () {
                view();
            },
            onClickRow: function (index, row) {
                if (row.delFlag == 'N') {
                    $("#resbtn").linkbutton('enable');
                    $("#delbtn").linkbutton('enable');
                    $("#upbtn").linkbutton('disable');
                    $("#auditbtn").linkbutton('disable');
                    $("#rejectbtn").linkbutton('disable');
                }
                if (row.delFlag == 'UP') {
                    $("#resbtn").linkbutton('disable');
                    //$("#delbtn").linkbutton('disable');
                    $("#upbtn").linkbutton('enable');
                    $("#auditbtn").linkbutton('disable');
                    $("#rejectbtn").linkbutton('disable');
                }
                if (row.delFlag == 'D') {
                    $("#resbtn").linkbutton('enable');
                    $("#delbtn").linkbutton('disable');
                    $("#upbtn").linkbutton('disable');
                    $("#auditbtn").linkbutton('disable');
                    $("#rejectbtn").linkbutton('disable');
                }
                if (row.delFlag == 'ND') {
                    $("#resbtn").linkbutton('disable');
                    $("#delbtn").linkbutton('disable');
                    $("#upbtn").linkbutton('disable');
                    $("#auditbtn").linkbutton('enable');
                    $("#rejectbtn").linkbutton('enable');
                }
                if (row.delFlag == 'S') {
                    $("#resbtn").linkbutton('disable');
                    $("#delbtn").linkbutton('disable');
                    $("#upbtn").linkbutton('disable');
                    $("#auditbtn").linkbutton('enable');
                    $("#rejectbtn").linkbutton('enable');
                }
                if (row.delFlag == 'F') {
                    $("#resbtn").linkbutton('enable');
                    $("#delbtn").linkbutton('disable');
                    $("#upbtn").linkbutton('enable');
                    $("#auditbtn").linkbutton('enable');
                    $("#rejectbtn").linkbutton('disable');
                }

            }
        });
        $('#importInit_win').window({
            title: '初始化导入数据',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 80
        });
        $('#import_win').window({
            title: '导入云搜数据',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 140
        });
        $('#importInfo_win').window({
            title: '上传资料',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 80
        });
        $('#flag').combobox({
            data: [{'id': '', 'text': '所有'}, {'id': 'N', 'text': '正常'}, {'id': 'D', 'text': '删除'}, {
                'id': 'S',
                'text': '待审核'
            }, {'id': 'ND', 'text': '待删除'}, {'id': 'UP', 'text': '上传资料'}, {'id': 'F', 'text': '重新授权'}],
            valueField: 'id',
            editable: false,
            textField: 'text',
            panelHeight: 'auto'
        });
    });

    //导入数据
    function drsjInit() {
        document.getElementById("_drhid").style.display = "block";
        document.getElementById("_drdiv").style.display = "none";
        $('#importInti_form').form('submit', {
            url: '${ctx}/statistics/cssUser/importInit',
            onSubmit: function () {
                var strs = [];
                var excel = $("#_sj").val();
                strs = excel.split('.');
                var suffix = strs[strs.length - 1];
                if (suffix != 'xls') {
                    parent.$.messager.alert("提示", "只支持*.xls文件。");
                    document.getElementById("_drhid").style.display = "none";
                    document.getElementById("_drdiv").style.display = "block";
                    return false;
                } else if (excel == '') {
                    show('请选择文件。');
                    document.getElementById("_drhid").style.display = "none";
                    document.getElementById("_drdiv").style.display = "block";
                    return false;
                } else {
                    return $('#importInit_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg, d);
            },
            success: function (data) {
                if (data == 'success') {
                    $("#importInit_win").window("close");
                    successTip(data, dg, d);
                    document.getElementById("_drhid").style.display = "none";
                    document.getElementById("_drdiv").style.display = "block";
                } else {
                    successTip(data, dg, d);
                    document.getElementById("_drhid").style.display = "none";
                    document.getElementById("_drdiv").style.display = "block";
                }
            }
        });
    }
    //导入数据
    function drsj() {
        document.getElementById("drhid").style.display = "block";
        document.getElementById("drdiv").style.display = "none";
        $('#import_form').form('submit', {
            url: '${ctx}/statistics/cssUser/import',
            onSubmit: function () {
                var strs = [];
                var excel = $("#sj").val();
                strs = excel.split('.');
                var suffix = strs[strs.length - 1];
                if (suffix != 'xls') {
                    parent.$.messager.alert("提示", "只支持*.xls文件。");
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else if (excel == '') {
                    show('请选择文件。');
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else {
                    return $('#import_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg, d);
            },
            success: function (data) {
                if (data == 'success') {
                    $("#import_win").window("close");
                    successTip(data, dg, d);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                } else {
                    successTip(data, dg, d);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                }
            }
        });
    }

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加信息',
            width: 540,
            height: 430,
            href: '${ctx}/statistics/cssUser/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/cssUser/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }
    //删除多个
    function delM() {
        var idList = [];
        var data = dg.datagrid('getSelections');
        for (var i = 0, j = data.length; i < j; i++) {
            idList.push(data[i].id);
        }
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'POST',
                    url: "${ctx}/statistics/cssUser/delete",
                    data: JSON.stringify(idList),
                    contentType: 'application/json;charset=utf-8',
                    success: function (data) {
                        if (data == 'success') {
                            dg.datagrid('reload');
                            dg.datagrid('clearSelections');
                            parent.$.messager.show({title: "提示", msg: "操作成功！", position: "bottomRight"});
                        } else {
                            parent.$.messager.alert('提示', data);
                        }
                    }
                });
                //dg.datagrid('reload'); //grid移除一行,不需要再刷新
            }
        });
    }
    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改信息',
            width: 540,
            height: 380,
            top: 80,
            href: '${ctx}/statistics/cssUser/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }
    //弹窗修改
    function view() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#v_dlg").dialog({
            title: '查看信息',
            width: 480,
            height: 620,
            top: 80,
            href: '${ctx}/statistics/cssUser/view/' + row.id,
            maximizable: true,
            modal: true
        });
    }

    function resuser() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '重新授权',
            width: 380,
            height: 200,
            top: 80,
            href: '${ctx}/statistics/cssUser/reset/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确定',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }
    //标记删除
    function delf() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        console.log(row)
        if(row.policeClassification==null||row.station==null){
            $.messager.alert("提示","请填写警种或岗位后再操作！！！");
            return ;
        }
        d = $("#dlg").dialog({
            title: '删除信息',
            width: 380,
            height: 160,
            top: 80,
            href: '${ctx}/statistics/cssUser/del/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确定',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }
    //弹窗修改
    function mod() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '变更信息',
            width: 400,
            height: 300,
            top: 80,
            href: '${ctx}/statistics/cssUser/mod/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //用户角色弹窗
    function cssUserInfo() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        $.ajaxSetup({type: 'GET'});
        d = $("#dlg").dialog({
            title: '信息详情',
            width: 1100,
            height: 350,
            href: '${ctx}/statistics/cssUser/' + row.id + '/cssinfo',
            maximizable: true,
            modal: true
        });
    }
    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('load', obj);
    }
    //导出excel
    function exportExcel() {
        $("#searchFrom").submit();
    }
    function exportExcelUser() {
        location.href = "${ctx}/statistics/cssUser/exportExcelUser";
    }
    function exportAreaUser() {
        location.href = "${ctx}/statistics/cssUser/exportAreaExcel";
    }

    function updateWin() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        $('#files').val("");
        $('#importInfo_win').window('open');
    }
    //审核
    function audit() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        $.ajax({
            type: 'POST',
            url: "${ctx}/statistics/cssUser/audit/" + row.id,
            success: function (data) {
                successTip(data, dg, d);
            }
        });

    }
    //标记删除
    function reject() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '退回',
            width: 380,
            height: 160,
            top: 80,
            href: '${ctx}/statistics/cssUser/reject/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确定',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }
    function uploadinfo() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        var fs = document.getElementById('files').files;
        // if (fs.length != 2) {
        //     $.messager.alert("提示", "请选择上传两个图片文件！");
        //     return;
        // }
        document.getElementById("upid").style.display = "block";
        document.getElementById("updiv").style.display = "none";
        $('#importInfo_form').form('submit', {
            url: '${ctx}/statistics/cssUser/importinfo/' + row.id,
            onSubmit: function () {
                var strs = [];
                var file = $("#files").val();
                strs = file.split('.');
                var suffix = strs[strs.length - 1].toLowerCase();
                if (suffix != 'jpg') {
                    parent.$.messager.alert("提示", "只支持*.jpg文件。");
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                    return false;
                } else if (file == '') {
                    show('请选择文件。');
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                    return false;
                } else {
                    return $('#importInfo_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg, d);
            },
            success: function (data) {

                if (data == 'success') {
                    $("#importInfo_win").window("close");
                    successTip(data, dg, d);
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";

                } else {
                    successTip(data, dg, d);
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                }
            }
        });
    }
    function reset() {
        document.getElementById("searchFrom").reset();
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('load', obj);
    }
</script>

</body>
</html>