<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Calendar" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <style type="text/css">
        dl, dt, dd {
            margin: 0;
            background: #fff5fa;
            font-size: 14px;
        }

        dl {
            margin: 0 auto;
            border: 1px solid #fff;
            border-bottom: none;
        }

        dt {
            background: #fff;
            color: #fff;
        }

        dt, dd {
            line-height: 30px;
            padding: 0 0 0 0px;
            border-bottom: 1px solid #fff;
            height: 30px;
            overflow: hidden
        }

        .fff {
            background: #fff
        }

        dt span, dd span {
            display: block;
            float: left;
            font-size: 14px;
            border-left: 1px solid #ffff;
            text-indent: 0em;
            width: 62px;
            text-align: center;
        }

        .hb {
            margin: 20px 20px 20px 20px;

        }
    </style>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <%@ include file="/WEB-INF/views/include/highcharts.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js"
            type="text/javascript"></script>
</head>
<%
    Calendar cal = Calendar.getInstance();

    cal.add(Calendar.MONTH, -1); // 你可以自己调整任何部分
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

    String datetime = sdf.format(cal.getTime());
    String months = datetime;
    String monthe = datetime;
%>


<body>
<div id="tb" style="padding:5px;height:auto" class="datagrid-toolbar">
    <div>
        <form id="searchFrom_c" action="${ctx}/statistics/cssinfo/exportExcel" method="post">
            <shiro:hasPermission name="statistics:ci:areaCode">
                <input id="areaCode" name="areaCode" data-options="width:120,prompt: '请选择地市'"/>
            </shiro:hasPermission>
            <shiro:hasPermission name="statistics:ci:date">
                <input type="text" name="startDate" class="easyui-my97" datefmt="yyyy-MM"
                       data-options="width:100,prompt: '<%=monthe %>'"/>
                - <input type="text" name="endDate" class="easyui-my97" datefmt="yyyy-MM"
                         data-options="width:100,prompt: '<%=monthe %>'"/>
            </shiro:hasPermission>
            <shiro:hasPermission name="statistics:ci:export">
                <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-standard-page-excel"
                   onclick="exportExcel()">导出Excel</a>
            </shiro:hasPermission>
            <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search"
               onclick="cx()">查询</a>

        </form>
    </div>
</div>
<div align="center">
    <!-- 	<div style="width: 1200px;height: auto; padding:3px 0px 3px 0px;">
                <div id="p0" class="easyui-panel" title="数据加载中..." style="padding: 5px; float: left;" data-options="closable:false">
                    <dl class="hb" id="wdr"></dl>
                </div>
            <div  class="easyui-panel" >
                <div style="line-height: 25px;color: red;font-weight: bold;font-family: 微软雅黑;float: left;">云搜索0登录人员：<dl class=hb id="wdr"></dl></div>
            </div>
        </div> -->
    <div style="width: 1200px;height: auto; padding:3px 0px 3px 0px;">
        <div id="p00" class="easyui-panel" title="数据加载中..." style="padding: 5px; float: left;"
             data-options="closable:false">
            <dl class="hb" id="wdr1"></dl>
        </div>
        <!-- <div  class="easyui-panel" >
            <div style="line-height: 25px;color: red;font-weight: bold;font-family: 微软雅黑;float: left;">云搜索0登录人员：<dl class=hb id="wdr"></dl></div>
        </div> -->
    </div>
    <div style="width: 1200px;height: 383px">
        <div style="float: left;">
            <div id="p1" class="easyui-panel" title="数据加载中..."
                 style="width:599px;height:380px;padding: 5px; float: left;" data-options="closable:false">
                <div id="chart1" style="height: 340px;"></div>
            </div>
        </div>
        <div style="float: right;">
            <div id="p2" class="easyui-panel" title="数据加载中..."
                 style="width:599px;height:380px;padding: 5px; float: right" data-options="closable:false">
                <div id="chart2" style="height: 340px;"></div>
            </div>
        </div>
    </div>
    <div style="width: 1200px;height: 383px">
        <div style="float: left;">
            <div id="p3" class="easyui-panel" title="数据加载中..."
                 style="width:599px;height:380px;padding: 5px; float: left;" data-options="closable:false">
                <div id="chart3" style="height: 340px;"></div>
            </div>
        </div>
        <div style="float: right;">
            <div id="p7" class="easyui-panel" title="数据加载中..."
                 style="width:599px;height:380px;padding: 5px; float: right" data-options="closable:false">
                <div id="chart7" style="height: 340px;"></div>
            </div>
        </div>
    </div>
    <c:if test="${user.name=='管理员' }">
        <div style="width: 1200px;height: 383px">
            <div style="float: left;">
                <div id="p5" class="easyui-panel" title="数据加载中..."
                     style="width:599px;height:380px;padding: 5px; float: left;" data-options="closable:false">
                    <div id="chart5" style="height: 340px;"></div>
                </div>
            </div>
            <div style="float: right;">
                <div id="p6" class="easyui-panel" title="数据加载中..."
                     style="width:599px;height:380px;padding: 5px; float: right" data-options="closable:false">
                    <div id="chart6" style="height: 340px;"></div>
                </div>
            </div>
        </div>
        <div style="width: 1200px;height: 383px">
            <div style="float: left;">
                <div id="p4" class="easyui-panel" title="数据加载中..."
                     style="width:599px;height:380px;padding: 5px; float: left;" data-options="closable:false">
                    <div id="chart4" style="height: 340px;"></div>
                </div>
            </div>
        </div>
    </c:if>
</div>
<script type="text/javascript" src="${ctx }/static/statistics/st.js"></script>
<script type="text/javascript">
    $(function () {

    });
    //导出excel
    function exportExcel() {
        $("#searchFrom_c").submit();
    }
    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom_c").serializeObject();
        /* 	console.log(obj) */
        loadData(obj);
    }


    //上级菜单
    $('#areaCode').combotree({
        width: 120,
        method: 'POST',
        url: '${ctx}/system/area/jsonpid?pid=2',
        textFiled: 'areaName',
        valueField: 'areaCode',
        editable: false
    });
</script>
</body>
</html>