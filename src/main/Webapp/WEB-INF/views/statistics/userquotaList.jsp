<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp"%>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">
            <!--<input type="text" name="filter_LIKES_name" class="easyui-validatebox" data-options="width:150,prompt: '名称'"/>-->
            <input id="cc1" class="easyui-combobox" name="filter_EQS_version"/>
            <span class="toolbar-item dialog-tool-separator"></span>
            <a href="javascript(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="cx()">查询</a>
        </form>

        <shiro:hasPermission name="statistics:userquota:add">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
               onclick="add();">添加</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:userquota:delete">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
               data-options="disabled:false" onclick="del()">删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:userquota:update">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
               onclick="upd()">修改</a>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:userquota:import">
            <a href="javascript:void(0)" onclick="$('#importWin').window('open')" class="easyui-linkbutton"
               data-options="iconCls:'icon-standard-database-go'" plain="true">导入人员配额</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<div style="display: none">

    <div id="importWin" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="drhid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>导入数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="drdiv">
                <form id="import_form" name="import_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2" style="padding: 10px 10px 10px 0px">
                                <%--<input type="text" name="batchrq" class="easyui-my97" datefmt="yyyy-MM"--%>
                                       <%--data-options="width:150,prompt: '导入数据日期', maxDate: '%y-%M'"/>--%>
                                <select id="cc" class="easyui-combobox" name="batchrq" style="width:200px;">
                                    <option value="第一季度">第一季度</option>
                                    <option value="第二季度">第二季度</option>
                                    <option value="第三季度">第三季度</option>
                                    <option value="第四季度">第四季度</option>
                                </select>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <input id="sj" type="file" name="sj" style="width: 260px;"/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="drsj()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dg;
    $(function () {
        $("#cc1").combobox({
            valueField: 'label',
            textField: 'value',
            data: [{
                label: '第一季度',
                value: '第一季度',
                selected:true
            },{
                label: '第二季度',
                value: '第二季度'
            },{
                label: '第三季度',
                value: '第三季度'
            },{
                label: '第四季度',
                value: '第四季度'
            }]
        })

        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/userquota/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'areaInfo', title: '地市', sortable: true, width: 100,
                    formatter: function (value, row, index) {
                        return value.areaName;
                    }
                },
                {field: 'num', title: '配额', sortable: true, width: 100},
                {field: 'version', title: '版本', sortable: true, width: 100}

            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb'
        });


        $('#importWin').window({
            title: '导入配额用户',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 140
        });
    });

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/userquota/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/userquota/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }

    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/userquota/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

    function drsj() {
        document.getElementById("drhid").style.display = "block";
        document.getElementById("drdiv").style.display = "none";
        $('#import_form').form('submit', {
            url: '${ctx}/statistics/userquota/import',
            onSubmit: function () {
                var strs = [];
                var excel = $("#sj").val();
                strs = excel.split('.');
                var suffix = strs[strs.length - 1];
                if (suffix != 'xls') {
                    parent.$.messager.alert("提示", "只支持*.xls文件。");
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else if (excel == '') {
                    show('请选择文件。');
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else {
                    return $('#import_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg);
            },
            success: function (data) {
                if (data == 'success') {
                    $("#importWin").window("close");
                    successTip(data, dg);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";

                } else {
                    successTip(data, dg);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                }
            }
        });
    }
</script>
</body>
</html>