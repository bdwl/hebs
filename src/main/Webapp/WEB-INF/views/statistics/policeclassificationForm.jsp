<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp"%>

</head>
<body>
<div>
    <form id="mainform" action="${ctx}/statistics/policeclassification/${action}" method="post">
        <table class="formTable">
            <tr>
                <td>警种代码：</td>
                <td>
                    <input type="hidden" name="id" value="${id}"/>
                    <input id="name" name="pccode" class="easyui-validatebox"
                           data-options="width: 150,required:'required'" value="${policeclassification.pccode }"/>
                </td>
            </tr>
            <tr>
                <td>警种名称：</td>
                <td>
                    <input id="code" name="pcname" class="easyui-validatebox"
                           data-options="width: 150,required:'required'" value="${policeclassification.pcname }"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function () {
        $('#mainform').form({
            onSubmit: function () {
                var isValid = $(this).form('validate');
                return isValid;	// 返回false终止表单提交
            },
            success: function (data) {
                successTip(data, dg, d);
            }
        });
    });

</script>
</body>
</html>