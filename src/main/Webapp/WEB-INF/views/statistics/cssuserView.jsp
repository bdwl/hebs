<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>

<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>
<body>
<div>
		<input type="hidden" name="id" value="${id }"/>
		<table class="formTable">
			<tr>
				<td>用户名：</td>
				<td>
					${CSSUser.name }
				</td>
			</tr>
			<tr>
				<td>身份证号：</td>
				<td>
				${CSSUser.sfzh }
				</td>
			</tr>
			<tr>
				<td>单位：</td>
				<td>${CSSUser.organization }</td>
			</tr>
			<tr>
				<td>所属地区：</td>
				<td><input id="uCode" value="${CSSUser.areaInfo.id }"/></td>
			</tr>
			<tr>
				<td>用户级别：</td>
				<td>
			       <input id="Leval" value="${CSSUser.userLeval }"/>
				</td>
			</tr>
			<tr>
				<td>有效期至：</td>
				<td> ${fn:substring(CSSUser.expirydate , 0, 10)}</td>
			</tr>
			<tr>
				<td valign="top" colspan="2"  style="padding: 10px" ><div style="border:1px #e0e0e0 solid;height:180px;width: 420px"><table id="d_dg" ></table></div><div style="border:1px #e0e0e0 solid;height:180px"><table id="fj_dg" ></table></div></td>
			</tr>
		</table>
</div>
<div id="win"></div>
<div id="dd"></div>
<div id="d_tb"><b>操作记录</b></div>
<div id="zl_tb"><b>上传资料详情</b></div>
<script type="text/javascript">
			var d_dg;
			var fj_dg;
			$(function(){   
				d_dg=$('#d_dg').datagrid({    
				method: "post",
			    url:'${ctx}/statistics/opinfo/json', 
			    fit : true,
				fitColumns : true,
				border : false,
				striped:true,
				idField : 'id',
				pagination:true,
				rownumbers:true,
				pageNumber:1,
				pageSize : 5,
				pageList : [5, 10, 20, 30, 40, 50 ],
				singleSelect:true,
				queryParams:{"filter_EQI_cssUser.id":"${id}"},
			    columns:[[    
					{field:'id',title:'id',hidden:true},
				    {field:'createTime',title:'创建时间',sortable:true,width:100},
			        {field:'status',title:'状态',sortable:true,width:100,align:'center',
			        	formatter: function (value, row, index) {
			        		if(value=="N"){
			            		return "<font color='blue'>正常</font>";
			            		}else if(value=="UP"){
			            			return "<font color='#6CA6CD'>上传资料</font>";
			            		}else if(value=="D"){
			            			return "<font color='red'>删除</font>";
			            		}else if(value=="ND"){
			            			return "<font color='#993333'>待删除</font>";
			            		}else if(value=="S"){
			            			return "<font color='#CD1076'>待审核</font>";
			            		}else if(value=="F"){
			            			return "<font color='#2E8B57'>重新授权</font>";
			            		}
			        	}		
			        },
			        {field:'detail',title:'变更事由',sortable:true,width:100}
		    	]],
			    enableHeaderClickMenu: false,
			    enableHeaderContextMenu: false,
			    enableRowContextMenu: false,
			    toolbar:'#d_tb',
			});
			fj_dg=$('#fj_dg').datagrid({    
			method: "post",
		    url:'${ctx}/statistics/attachment/json', 
		    fit : true,
			fitColumns : true,
			border : false,
			striped:true,
			idField : 'id',
			pagination:true,
			rownumbers:true,
			pageNumber:1,
			pageSize : 5,
			pageList : [5, 10, 20, 30, 40, 50 ],
			singleSelect:true,
			queryParams:{"filter_EQI_cssUser.id":"${id}"},
		    columns:[[    
				{field:'id',title:'id',hidden:true},
				{field:'flag',title:'状态',sortable:true,width:140,
					formatter: function (value, row, index) {
						if(value==0){
							return "待审核";
						}else{
							return "已审核";
						}
					
					}	
				},{
                    field: 'eltdate', title: '上传时间', width: 180
                },
				{field:'path',title:'查看',sortable:true,width:140,
					formatter: function (value, row, index) {
						return "<a href='#'  onclick='showImg("+row.id+")'>查看</a>";
					}
				},
	
		    ]],
		    enableHeaderClickMenu: false,
		    enableHeaderContextMenu: false,
		    enableRowContextMenu: false,
		    toolbar:'#zl_tb',
		});
});
			
function showImg(data){
    parent.openDlg(data);

}
$(function(){
	$('#Leval').combobox({
	    data:[{'id':1,'text':'高级用户'},{'id':0,'text':'普通用户'}],
	    valueField:'id',
	    textField:'text',
	    panelHeight:'auto'
	});
	$('#uCode').combotree({
		width:180,
		method:'POST',
	    url: '${ctx}/system/area/json',
	    textFiled : 'areaName',
	    valueField:'areaCode',
	    required: true,
	    editable:false
	}); 
	
	$("#uCode").combobox({ disabled: true });
	$("#uCode").css('background','#eee');
	$("#Leval").combobox({ disabled: true });
	$("#Leval").css('background','#eee');
});
</script>

</body>
</html>