<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">
            <!--<input type="text" name="filter_LIKES_name" class="easyui-validatebox" data-options="width:150,prompt: '名称'"/>-->
            <span class="toolbar-item dialog-tool-separator"></span>
            <a href="javascript(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="cx()">查询</a>
        </form>

        <shiro:hasPermission name="statistics:policeclassification:add">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
               onclick="add();">添加</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:policeclassification:delete">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
               data-options="disabled:false" onclick="del()">删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:policeclassification:update">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
               onclick="upd()">修改</a>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:policeclassification:import">
            <a href="javascript:void(0)" onclick="$('#importInfo_win').window('open')" class="easyui-linkbutton"
               id="upbtn"
               data-options="iconCls:'icon-standard-page-white-get'" plain="true">导入</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<div style="display: none">
    <div id="importInfo_win" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="upid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>上传中数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="updiv">
                <form id="importInfo_form" name="importInfo_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <input id="files" type="file" name="sj" style="width: 260px;" multiple/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="uploadinfo()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dg;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/policeclassification/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'pcname', title: '警种名称', width: 100},
                {field: 'pccode', title: '警种代码', width: 100}
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb'
        });

        $('#importInfo_win').window({
            title: '导入',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 80
        });
    });

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/policeclassification/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/policeclassification/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }

    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/policeclassification/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

    function uploadinfo() {
        document.getElementById("upid").style.display = "block";
        document.getElementById("upid").style.display = "none";
        $('#importInfo_form').form('submit', {
            url: '${ctx}/statistics/policeclassification/import',
            onSubmit: function () {
                var file = $("#files").val();
                strs = file.split('.');
                var suffix = strs[strs.length - 1].toLowerCase();
                if (suffix != 'xls') {
                    parent.$.messager.alert("提示", "只支持*.xls文件。");
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                    return false;
                } else if (file == '') {
                    show('请选择文件。');
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                    return false;
                } else {
                    return $('#importInfo_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg);
            },
            success: function (data) {

                if (data == 'success') {
                    $("#importInfo_win").window("close");
                    successTip(data, dg);
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";

                } else {
                    successTip(data, dg);
                    document.getElementById("upid").style.display = "none";
                    document.getElementById("updiv").style.display = "block";
                }
            }
        });
    }
</script>
</body>
</html>