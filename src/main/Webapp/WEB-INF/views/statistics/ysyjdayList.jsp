<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">
            <input type="text" name="filter_EQS_sfzh" class="easyui-validatebox"
                   data-options="width:150,prompt: '身份证'"/>
            <span class="toolbar-item dialog-tool-separator"></span>
            <input type="text" id="start" name="filter_GES_tbrq" class="easyui-my97" datefmt="yyyy-MM-dd"
                   data-options="width:150,prompt: '操作开始日期', maxDate: '%y-%M'"/>
            - <input type="text" name="filter_LES_tbrq" class="easyui-my97" datefmt="yyyy-MM-dd"
                     data-options="width:150,prompt: '操作结束日期', maxDate: '%y-%M'"/>

            <a href="javascript(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="cx()">查询</a>
        </form>
    </div>
    <shiro:hasPermission name="statistics:ysyjday:import">
        <a href="javascript:void(0)" onclick="$('#import_win').window('open')" class="easyui-linkbutton"
           data-options="iconCls:'icon-standard-vcard-add'" plain="true">导入云搜用户</a>
        <span class="toolbar-item dialog-tool-separator"></span>
    </shiro:hasPermission>
</div>
<div style="display: none;">
    <div id="import_win" class="easyui-window" iconCls="icon-search"
         style="width: 350px; height: 160px; padding: 5px; background: #fafafa;">
        <div class="easyui-layout" fit="true">
            <div style='line-height:25px;text-align:center;display: none;' id="drhid"><img
                    src='${ctx}/static/images/loading.gif' height='25' width='25'/>导入数据中...
            </div>
            <div region="center" border="false"
                 style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;" id="drdiv">
                <form id="import_form" name="import_form" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <input id="sj" type="file" name="sj" style="width: 260px;"/>
                            </td>
                            <td>
                                <input type="button" value="导入" onclick="drsj()"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<table id="dg"></table>
<div id="dlg"></div>
<script type="text/javascript">
    var dg;
    var d;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/ysyjday/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 50,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'tbrq', title: '日期', sortable: true, width: 100},
                {field: 'name', title: '姓名', sortable: true, width: 100},
                {field: 'sfzh', title: '身份证号', sortable: true, width: 100},
                {field: 'dwmc', title: '单位名称', sortable: true, width: 200},
                {field: 'count', title: '查询次数', sortable: true, width: 100},
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb'
        });

        $('#import_win').window({
            title: '导入数据',
            modal: true,
            shadow: false,
            closed: true,
            resizable: false,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            width: 350,
            height: 80
        });
    });


    //导入数据
    function drsj() {
        document.getElementById("drhid").style.display = "block";
        document.getElementById("drdiv").style.display = "none";
        $('#import_form').form('submit', {
            url: '${ctx}/statistics/ysyjday/import',
            onSubmit: function () {
                var strs = [];
                var excel = $("#sj").val();
                strs = excel.split('.');
                var suffix = strs[strs.length - 1];
                if (suffix != 'xls') {
                    parent.$.messager.alert("提示", "只支持*.xls文件。");
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else if (excel == '') {
                    show('请选择文件。');
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                    return false;
                } else {
                    return $('#import_form').form('validate');
                }
            },
            error: function (data) {
                successTip('数据异常', dg, d);
            },
            success: function (data) {
                if (data == 'success') {
                    $("#import_win").window("close");
                    successTip(data, dg, d);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                } else {
                    successTip(data, dg, d);
                    document.getElementById("drhid").style.display = "none";
                    document.getElementById("drdiv").style.display = "block";
                }
            }
        });
    }
    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

</script>
</body>
</html>