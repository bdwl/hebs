<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">

            <input type="text" id="flag" name="filter_EQS_status" class="easyui-validatebox"
                   data-options="width:80,prompt: '状态'"/>
            <input type="text" name="filter_LIKES_cssUser.sfzh" class="easyui-validatebox"
                   data-options="width:120,prompt: '身份证号'"/>
            <input type="text" id="start" name="filter_GED_createTime" class="easyui-my97" datefmt="yyyy-MM-dd HH:mm:ss"
                   data-options="width:150,prompt: '操作开始日期', maxDate: '%y-%M-%d'"/>
            - <input type="text" name="filter_LED_createTime" class="easyui-my97" datefmt="yyyy-MM-dd HH:mm:ss"
                     data-options="width:150,prompt: '操作结束日期', maxDate: '%y-%M-%d'"/>
            <span class="toolbar-item dialog-tool-separator"></span>
            <a href="javascript(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search" onclick="cx()">查询</a>
        </form>

        <shiro:hasPermission name="statistics:opinfo:add">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
               onclick="add();">添加</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:opinfo:delete">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
               data-options="disabled:false" onclick="del()">删除</a>
            <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="statistics:opinfo:update">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true"
               onclick="upd()">修改</a>
        </shiro:hasPermission>
    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<script type="text/javascript">
    var dg;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/opinfo/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {
                    field: 'yhxm', title: '用户姓名', width: 40,
                    formatter: function (value, row, index) {
                        return row.cssUser.name;
                    }
                },
                {
                    field: 'sfzh', title: '身份证号', width: 80,
                    formatter: function (value, row, index) {
                        return row.cssUser.sfzh;
                    }
                },
                {
                    field: 'dwmc', title: '单位', width: 120,
                    formatter: function (value, row, index) {
                        return row.cssUser.organization;
                    }
                },
                {field: 'createTime', title: '创建时间', sortable: true, width: 100},
                {
                    field: 'status', title: '状态', sortable: true, width: 100, align: 'center',
                    formatter: function (value, row, index) {
                        if (value == "N") {
                            return "<font color='blue'>正常</font>";
                        } else if (value == "UP") {
                            return "<font color='#6CA6CD'>上传资料</font>";
                        } else if (value == "D") {
                            return "<font color='red'>删除</font>";
                        } else if (value == "ND") {
                            return "<font color='#993333'>待删除</font>";
                        } else if (value == "S") {
                            return "<font color='#CD1076'>待审核</font>";
                        } else if (value == "F") {
                            return "<font color='#2E8B57'>重新授权</font>";
                        }
                    }
                },
                {field: 'detail', title: '变更事由', sortable: true, width: 100}
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb'
        });
        $('#flag').combobox({
            data: [{'id': '', 'text': '所有'}, {'id': 'N', 'text': '正常'}, {'id': 'D', 'text': '删除'}, {
                'id': 'S',
                'text': '待审核'
            }, {'id': 'UP', 'text': '上传资料'}, {'id': 'F', 'text': '重新授权'}],
            valueField: 'id',
            editable: false,
            textField: 'text',
            panelHeight: 'auto'
        });
    });

    //弹窗增加
    function add() {
        d = $("#dlg").dialog({
            title: '添加',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/opinfo/create',
            maximizable: true,
            modal: true,
            buttons: [{
                text: '确认',
                handler: function () {
                    $("#mainform").submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //删除
    function del() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        parent.$.messager.confirm('提示', '删除后无法恢复您确定要删除？', function (data) {
            if (data) {
                $.ajax({
                    type: 'get',
                    url: "${ctx}/statistics/opinfo/delete/" + row.id,
                    success: function (data) {
                        successTip(data, dg);
                    }
                });
            }
        });
    }

    //弹窗修改
    function upd() {
        var row = dg.datagrid('getSelected');
        if (rowIsNull(row)) return;
        d = $("#dlg").dialog({
            title: '修改',
            width: 380,
            height: 250,
            href: '${ctx}/statistics/opinfo/update/' + row.id,
            maximizable: true,
            modal: true,
            buttons: [{
                text: '修改',
                handler: function () {
                    $('#mainform').submit();
                }
            }, {
                text: '取消',
                handler: function () {
                    d.panel('close');
                }
            }]
        });
    }

    //创建查询对象并查询
    function cx() {
        var obj = $("#searchFrom").serializeObject();
        dg.datagrid('reload', obj);
    }

</script>
</body>
</html>