<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
</head>
<body>
<div>
    <form id="mainform" action="${ctx }/statistics/cssUser/${action}" method="post">
        <input type="hidden" name="id" value="${id }"/>
        <table class="formTable">
            <c:if test="${action != 'del'&&action !='reset'&&action !='reject'}">
                <tr>
                    <td>用户名：</td>
                    <td>

                        <input id="name" name="name" class="easyui-validatebox"
                               data-options="width: 150,required:'required'" value="${CSSUser.name }"/>
                    </td>
                </tr>
                <tr>
                    <td>身份证号：</td>
                    <td>
                        <input id="sfzh" name="sfzh" class="easyui-validatebox"
                               data-options="width: 150,required:true,validType:'idCard'" value="${CSSUser.sfzh }"/>
                    </td>
                </tr>
                 <%--</c:if>--%>
                <tr>
                    <td>手机号：</td>
                    <td>
                        <input id="telphone" name="telphone" class="easyui-validatebox"
                               data-options="width: 150,required:true,validType:'mobile'" value="${CSSUser.telphone }"/>
                    </td>
                </tr>
                 <%--<c:if test="${action != 'del'&&action !='reset'&&action !='reject'}">--%>
                <tr>
                    <td>单位：</td>
                    <td><input  class="easyui-validatebox" style="width: 360px" id="orgcode" name="organization"
                               value="${CSSUser.organization }" data-options="required:true"/></td>
                </tr>
                <tr>
                    <td>所属地区：</td>
                    <td><input id="unitCode" name="areaInfo.id" value="${CSSUser.areaInfo.id }"/></td>
                </tr>
                <%--<tr>--%>
                    <%--<td>组织机构：</td>--%>
                    <%--<td><input id="orgV" name="orgcode" value="${CSSUser.orgcode }"/><input id="orgK" type="hidden"--%>
                                                                                            <%--name="organization"--%>
                                                                                            <%--value="${CSSUser.organization }"/>--%>
                    <%--</td>--%>
                <%--</tr>--%>
                <tr>
                    <td>用户级别：</td>
                    <td>
                        <input id="userLeval" name="userLeval" value="${CSSUser.userLeval }"/>
                    </td>
                </tr>
                     <tr>
                    <td>警种：</td>
                    <td>
                        <input id="policeClassification"  data-options="width: 150,required:'required'" name="policeClassification.id" value="${CSSUser.policeClassification.id }"/>
                    </td>
                </tr>   <tr>
                    <td>岗位：</td>
                    <td>
                        <input id="station"  data-options="width: 150,required:'required'" name="station.id" value="${CSSUser.station.id }"/>
                    </td>
                </tr>
                <tr>
                    <td>创建时间：</td>
                    <td><input type="text" id="createdate" name="createdate" class="easyui-my97" datefmt="yyyy-MM-dd"
                               data-options="width:150,prompt: '创建时间'"
                               value="${fn:substring(CSSUser.createdate , 0, 10)}"/></td>
                </tr>
                <tr>
                    <td>有效期至：</td>
                    <td><input type="text" id="expirydate" name="expirydate" class="easyui-my97" datefmt="yyyy-MM-dd"
                               data-options="width:150,prompt: '有效期'"
                               value="${fn:substring(CSSUser.expirydate , 0, 10)}"/></td>
                </tr>
           	</c:if> 
            <c:if test="${action != 'view'}">
                <tr>
                    <td valign="top">事由：</td>
                    <td><textarea rows="3" cols="41" id="detail" name="detail" class="easyui-validatebox"
                                  data-options="width: 280,required:'required'"
                                  style="font-size: 12px;font-family: '微软雅黑'">${CSSUser.detail }</textarea></td>
                </tr>
            </c:if>
            <c:if test="${action == 'view'}">
                <tr>
                    <td valign="top" colspan="2" style="padding: 10px">
                        <div style="border:1px #e0e0e0 solid;height:180px">
                            <table id="d_dg"></table>
                        </div>
                    </td>
                </tr>
                <script type="text/javascript">
                    var d_dg;
                    $(function () {
                        d_dg = $('#d_dg').datagrid({
                            method: "post",
                            url: '${ctx}/statistics/opdetail/json',
                            fit: true,
                            fitColumns: true,
                            border: false,
                            striped: true,
                            idField: 'id',
                            pagination: true,
                            rownumbers: true,
                            pageNumber: 1,
                            pageSize: 5,
                            pageList: [5, 10, 20, 30, 40, 50],
                            singleSelect: true,
                            queryParams: {"filter_EQI_cssUser.id": "${id}"},
                            columns: [[
                                {field: 'id', title: 'id', hidden: true},
                                {field: 'createdate', title: '创建时间', sortable: true, width: 140},
                                {
                                    field: 'delflag', title: '状态', sortable: true, width: 40, align: 'center',
                                    formatter: function (value, row, index) {
                                        if (value == "N") {
                                            return "<font color='blue'>正常</font>";
                                        } else if (value == "UP") {
                                            return "<font color='#6CA6CD'>上传资料</font>";
                                        } else if (value == "D") {
                                            return "<font color='red'>删除</font>";
                                        } else if (value == "ND") {
                                            return "<font color='#993333'>待删除</font>";
                                        } else if (value == "S") {
                                            return "<font color='#CD1076'>待审核</font>";
                                        } else if (value == "F") {
                                            return "<font color='#2E8B57'>重新授权</font>";
                                        }
                                    }
                                },
                                {field: 'detail', title: '变更事由', sortable: true, width: 200}
                            ]],
                            enableHeaderClickMenu: false,
                            enableHeaderContextMenu: false,
                            enableRowContextMenu: false
                        });
                    });
                </script>
            </c:if>
        </table>
    </form>
</div>
<script type="text/javascript">
    $(function () {
        $('#userLeval').combobox({
            data: [{'id': 1, 'text': '高级用户', selected: true}, {'id': 0, 'text': '普通用户'}],
            valueField: 'id',
            textField: 'text',
            panelHeight: 'auto'
        });
        $('#policeClassification').combobox({
            url:"${ctx}/statistics/policeclassification/jsonAll",
            valueField: 'id',
            textField: 'pcname',
            panelHeight: 200
        });
        $('#station').combobox({
            url:"${ctx}/statistics/station/jsonAll",
            valueField: 'id',
            textField: 'sname',
            panelHeight: 200
        });
    });
    var action = "${action}";
    //提交表单
    $('#mainform').form({
        onSubmit: function () {
            var isValid = $(this).form('validate');
            var ai = $("input[name='areaInfo.id']").val();
            if (ai == "") {
                $.easyui.messager.alert("提示", "请选择所属地区！");
                isValid = false;
            }
            return isValid;	// 返回false终止表单提交
        },
        success: function (data) {
            successTip(data, dg, d);
        }
    });

    if (action == 'create') {
        $("#sfzh").blur(function () {
            var sfz = $("#sfzh").val();
            $.ajax({
                type: 'POST',
                url: "${ctx}/statistics/cssUser/checkSFZH",
                data: {sfzh: sfz},
                success: function (data) {
                    if (data == 'false') {
                        $.messager.alert('提示', "此身份证号已经存在！");
                    }
                }
            });

        });
    } else if (action == 'mod') {
        $("#name").attr('readonly', 'readonly');
        $("#name").css('background', '#eee');
        $("#sfzh").attr('readonly', 'readonly');
        $("#sfzh").css('background', '#eee');
        $("#userLeval").combobox({disabled: true});
        $("#userLeval").css('background', '#eee');
        $("#detail").val("");
    } else if (action == 'view') {
        $("#name").attr('readonly', 'readonly');
        $("#name").css('background', '#eee');
        $("#sfzh").attr('readonly', 'readonly');
        $("#sfzh").css('background', '#eee');
        $("#organization").attr('readonly', 'readonly');
        $("#organization").css('background', '#eee');
        $("#unitCode").combobox({disabled: true});
        $("#unitCode").css('background', '#eee');
        $("#detail").attr('readonly', 'readonly');
        $("#detail").css('background', '#eee');
        $("#expirydate").combobox({disabled: true});
        $("#expirydate").css('background', '#eee');
        $("#userLeval").combobox({disabled: true});
        $("#userLeval").css('background', '#eee');
    } else if (action == "del") {
        $("#detail").val("");
    } else if (action == "reset") {
        $("#detail").val("");
    }else if (action == "reject") {
        $("#detail").val("");
    }


    //上级菜单
    $('#unitCode').combotree({
        width: 180,
        method: 'POST',
        url: '${ctx}/system/area/jsonHb',
        textFiled: 'areaName',
        valueField: 'areaCode',
        required: true,
        editable: false,
        keyHandler: {
            query: queryHandler
        }
    });

    <%--var btsloader = function (param, success, error) {--%>
        <%--var q = param.q || "";--%>
        <%--if (q.length <= 0) {--%>
            <%--return false;--%>
        <%--}--%>
        <%--$.ajax({--%>
            <%--url: '${ctx}/system/organization/jsonKey',--%>
            <%--type: "post",--%>
            <%--data: {filter_LIKES_orgName_OR_orgCode: q},//后台使用param这个变量接收传值的，后台用了struts、spring后面就不拓展说明了--%>
            <%--dataType: "json",--%>
            <%--success: function (data) {--%>
                <%--//console.log("i am in success-->" + data);//返回的是数组对象data--%>
                <%--var items = $.each(data, function (value) {--%>
                    <%--return value; //遍历数组中的值--%>
                <%--});--%>
                <%--success(items);//调用loader的success方法，将items添加到下拉框中,这里是难点啊，之前后台已经返回数据了，但就是不添加到下拉框--%>
            <%--}--%>
        <%--});--%>
    <%--};--%>

    <%--$(function () {--%>
        <%--$("#orgcode").combobox({--%>
            <%--onSelect: function () {--%>
                <%--var k = $("#orgcode").combobox('getText');--%>
                <%--var v = $("#orgcode").combobox('getValue');--%>
                <%--$("#orgK").val(k);--%>
                <%--$("#orgV").val(v);--%>
            <%--}--%>
        <%--});--%>
    <%--});--%>

    function queryHandler(searchText, event) {
        searchText = $.trim(searchText);
        $('#unitCode').combotree('tree').tree("search", searchText);
//	}
    }

</script>
</body>
</html>