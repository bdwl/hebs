<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <%@ include file="/WEB-INF/views/include/easyui.jsp" %>
    <script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
        <form id="searchFrom" action="">
            用户统计
        </form>

    </div>

</div>
<table id="dg"></table>
<div id="dlg"></div>
<script type="text/javascript">
    var dg;
    $(function () {
        dg = $('#dg').datagrid({
            method: "post",
            url: '${ctx}/statistics/contrastuser/json',
            fit: true,
            fitColumns: true,
            border: false,
            striped: true,
            idField: 'id',
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 40, 50],
            singleSelect: true,
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {
                    field: 'areaInfo', title: '地市', sortable: true, width: 100, align: 'center',
                    formatter: function (value, row, index) {
                        if (value != null) {
                            return value.areaName;
                        }
                    }
                },
                {field: 'addNum', title: '实际授权人数', sortable: true, width: 100, align: 'center'},
                // {field: 'addNum', title: '新增', sortable: true, width: 100, align: 'center'},
                // {field: 'delNum', title: '删除', sortable: true, width: 100, align: 'center'},
                // {field: 'delnNum', title: '待删除', sortable: true, width: 100, align: 'center'}
            ]],
            enableHeaderClickMenu: false,
            enableHeaderContextMenu: false,
            enableRowContextMenu: false,
            toolbar: '#tb',
            rowStyler: function (index, row) {
                if (row.addNum > (row.delNum + row.delnNum)) {
                //    return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                }
            }
        });
    });


</script>
</body>
</html>