<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<script src="${ctx}/static/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>
<body>

<table id="cssInfo_dg"></table> 
<script type="text/javascript">
var cssInfo_dg;
$(function(){   
	cssInfo_dg=$('#cssInfo_dg').datagrid({    
	method: "post",
    url:'${ctx}/statistics/cssUser/cssinfojson/${cssUserId}', 
    fit : true,
	fitColumns : true,
	border : false,
	idField : 'id',
	striped:true,
	pagination:true,
	rownumbers:true,
	pageNumber:1,
	pageSize : 20,
	pageList : [ 10, 20, 30, 40, 50 ],
	singleSelect:true,
    columns:[[    
        {field:'id',title:'id',hidden:true},    
        {field:'yssLoginNum',title:'登录数',sortable:true,width:90,align:'center'},
        {field:'yssSearchNum',title:'查询数',sortable:true,width:90,align:'center'},
        {field:'yssClickNum',title:'点击数',sortable:true,width:90,align:'center'},
        {field:'yssOplogNum',title:'操作数',sortable:true,width:90,align:'center'},
        {field:'yssExportNum',title:'导出数',sortable:true,width:90,align:'center'},
        {field:'yssTotal',title:'总量',sortable:true,width:90,align:'center'},
        {field:'yssLF',title:'低频用户',sortable:true,width:90,align:'center',
            formatter: function (value, row, index) {
                var total = row.yssSearchNum+row.yssClickNum+row.yssOplogNum;
                if(100>total){
                    return "否";
                }else{
                    return "<font color='red'>是</font>";
                }
            }
        },
        {field:'authTime',title:'授权时间',sortable:true,width:120,align:'center'},
        {field:'batchrq',title:'批次时间',sortable:true,width:90,align:'center'},
        {field:'createTime',title:'导入时间',sortable:true,width:120}
    ]]
	});
});


</script>
</body>
</html>