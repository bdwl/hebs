<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
request.setAttribute("error", error);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>系统</title>
	
	<script src="${ctx}/static/plugins/easyui/jquery/jquery-1.11.1.min.js"></script>
	<script src="${ctx}/static/plugins/jquery-validation/1.11.1/jquery.validate.min.js"></script>
	<script src="${ctx}/static/plugins/jquery-validation/1.11.1/messages_bs_zh.js"></script>
	<script type="text/javascript" src="${ctx}/static/plugins/jquery/jquery.form.js"></script>
	<link rel="stylesheet" type="text/css" href="${ctx}/static/css/bglogin.css" />
	<style type="text/css">
	.header{height:80px;padding-top:20px;margin: auto;}
	.error{background:none;width:270px;font-weight:normal;color:inherit;margin:0;}
	
	.notification-info {
  background: #50c1f2;
  border: 1px solid #3dbaf1;
}
.notification-info h1:before {
  color: #11a5e5;
}
.notification-info .close {
  background: #50c1f2;
}
.notification-info .close:after {
  color: #97daf7;
}
.notification-info .close:hover, .notification-info .close:focus {
  background: #2ab4ef;
}
.notification-info .close:active {
  background: white;
}
.notification-info .close:active:after {
  color: #50c1f2;
}
.notification-info h1:before {
  content: '\2139';
}
	
	</style>
	<script>
	$(document).ready(function() {
		$("#loginForm").validate({
			rules: {
				validateCode: {remote: "${pageContext.request.contextPath}/servlet/validateCodeServlet"}
			},
			messages: {
				username: {required: "请填写用户名."},password: {required: "请填写密码."},
				validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
			},
			errorLabelContainer: "#messageBox",
			errorPlacement: function(error, element) {
				error.appendTo($("#messageBox").parent());
			} 
		});
	});
	
	/* var captcha;
	function refreshCaptcha(){  
	    document.getElementById("img_captcha").src="${ctx}/static/images/kaptcha.jpg?t=" + Math.random();  
	}   */
	</script>
</head>
<body>

	<div>
	<form id="loginForm" action="${ctx}/admin/login" method="post">
<%-- 	<form id="loginForm" action="${ctx}/a/login" method="post"> --%>
		<div class="login_top">
			<div class="login_title">
				绩效考核系统
			</div>
		</div>
			<div style="float:left;width:100%;">
			<div class="login_main">
				<div class="login_main_top"></div>
				<div class="login_main_errortip"><div id="messageBox"
					class="alert alert-error ${empty message ? 'hide' : ''}">
				</div></div>
				<div class="login_main_ln">
					<input type="text" id="username" name="username" value="admin" class="required"/>
				</div>
				<div class="login_main_pw">
					<input type="password" id="password" name="password" value="123456" class="required"/>
				</div>
				<c:if test="${isValidateCodeLogin}">
				<div class="login_main_yzm">
					<%-- <input type="text" id="captcha" name="captcha"/>
					<img alt="验证码" src="${ctx}/static/images/kaptcha.jpg" title="点击更换" id="img_captcha" onclick="javascript:refreshCaptcha();" style="height:45px;width:85px;float:right;margin-right:98px;"/> --%>
					<sys:validateCode name="validateCode" inputCssStyle="width:125px;"/>
				</div>
				</c:if>
				<div class="login_main_remb">
					<input id="rm" name="rememberMe" type="hidden"/><!-- <label for="rm"><span>记住我</span></label> -->
				</div>
				<div class="login_main_submit">
					<button onclick=""></button>
				</div>
			</div>
		</div>
	</form>
	</div>
<%-- 	<c:choose>
		<c:when test="${error eq 'com.guy.system.utils.CaptchaException'}">
			<script>
				$(".login_main_errortip").html("验证码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.UnknownAccountException'}">
			<script>
				$(".login_main_errortip").html("帐号或密码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.IncorrectCredentialsException'}">
			<script>
				$(".login_main_errortip").html("用户名不存在，请重试");
			</script>
		</c:when>
	</c:choose> --%>
</body>
</html>
