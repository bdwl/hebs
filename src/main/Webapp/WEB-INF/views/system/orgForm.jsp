<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>
<body>
<div>
	<form id="mainform" action="${ctx}/system/organization/${action}" method="post">
	<table  class="formTable" align="center" style="margin: auto; padding:10px 0 0 0; ">
		<tr>
			<td colspan="2" align="center" style="margin: auto" class="aa">
			<input name="orgCode" id="orgcode" type="text" value="${organization.orgCode }" data-options="required:true,validType:'myvalidate',valueField:'orgCode',textField:'orgCode',loader : btsloader,mode : 'remote',formatter:function(row){return row.orgName+' '+row.orgCode;	}"  style="width: 380px;height: 60px;" class="easyui-combobox" maxlength="12"/>
			</td>
		</tr>
		<tr>
			<td>机构名称：</td>
			<td>
			<input type="hidden" name="id" value="${id}" data-options="required:false"/>
			<input name="orgName" type="text" value="${organization.orgName }" class="easyui-validatebox"  style="width: 300px;font-size: 18px;" data-options="required:true,validType:['length[0,50]']" />
			</td>
		</tr>
	
		<tr>
			<td>所在区域：</td>
			<td>
				<input id="cc1" class="easyui-combobox" style="width: 110px"  data-options="valueField:'code',textField:'name',editable:false,url:'${ctx}/system/city/json?parentid=0',
				onChange:function(rec){
					$('#cc2').combobox('clear'); 	
					$('#cc3').combobox('clear'); 	
				},
				 onSelect:function(rec){
				  var url = '${ctx}/system/city/json?parentid='+rec.id;
				 	$('#orgcode').combobox('setValue',rec.code);
            	  $('#cc2').combobox('reload', url);
			  	}">
				<input id="cc2" class="easyui-combobox"  style="width: 100px;"  data-options="valueField:'code',textField:'name',editable:false,
				onChange:function(rec){
					$('#cc3').combobox('clear'); 	
				},
				onSelect:function(rec){
				  var url = '${ctx}/system/city/json?parentid='+rec.id;
				  		$('#orgcode').combobox('setValue',rec.code);
            		$('#cc3').combobox('reload', url);
			 	 }">
			<input id="cc3" class="easyui-combobox" style="width: 80px;"  data-options="valueField:'code',textField:'name',editable:false,
				onSelect:function(rec){
				  	$('#orgcode').combobox('setValue',rec.code);
			 	 }
			">
			</td>
		</tr>
			<tr>
			<td>机构：</td>
			<td>
			<input type="text" id="org" class="easyui-combobox" data-options="valueField:'id',textField:'text',panelHeight:'auto',editable:false,
			data:[{id:'T',text:'交通部'},{id:'M',text:'民航'},{id:'S',text:'森林'},{id:'H',text:'海关缉私'},{id:'B',text:'边防'},{id:'X',text:'消防'},{id:'J',text:'警卫部'}],
			onSelect:function(rec){
				  	var v=$('#orgcode').combobox('getValue');  
				  	if(v.length==6){
			  			$('#orgcode').combobox('setValue',v+rec.id);
				  	}else{
				  		lv = v.charAt(v.length - 1);
				  		v=v.replace(lv,rec.id);
				  		$('#orgcode').combobox('setValue',v);
				  	}
			 	 }" />
			</td>
		</tr>
	</table>
	</form>
</div>
<script type="text/javascript">
$.extend($.fn.validatebox.defaults.rules, {
    myvalidate : {
        validator : function(value, param) {
            var orgcode = $("#orgcode").combobox('getValue');
            var bool=false;
            $.ajax({
                type : 'post',
                async : false,
                url : '${ctx}/system/organization/isCode',
                data : {
                    "orgCode" : orgcode
                },
                success : function(data) {
                	bool = !data;
                }
            });
            return bool;
        },
        message : '用户名已经被占用'
    }
});
$(function(){
	//上级菜单
	$('#pid').combotree({
		width:180,
		method:'POST',
	    url: '${ctx}/system/organization/json',
	    idField : 'id',
	    textFiled : 'orgName',
		parentField : 'pid',
	    animate:true
	});  
	//区域菜单
	$('#areaId').combotree({
		width:180,
		method:'POST',
	    url: '${ctx}/system/area/json',
	    idField : 'id',
	    textFiled : 'areaName',
		parentField : 'pid',
	    animate:true
	});
	$('#mainform').form({    
	    onSubmit: function(){    
	    	$.ajax({
				url:'${ctx}/system/organization/isCode',
				type: "post",
				success: function (data) {
					console.log(data);
					return false;
				}
			});
	    	var isValid = $(this).form('validate');
	    	console.log(isValid);
			return isValid;	// 返回false终止表单提交
			
	    	
	    },    
	    success:function(data){   
	    	if(successTip(data,dg,d))
	    		dg.datagrid('reload');
	    }    
	}); 
	
});

var btsloader = function (param, success, error) {
	var q = param.q || "";
	if (q.length <= 0) {
		return false;
	}
	$.ajax({
		url: '${ctx}/system/organization/jsonKey',
		type: "post",
		data: {filter_LIKES_orgName_OR_orgCode: q},//后台使用param这个变量接收传值的，后台用了struts、spring后面就不拓展说明了
		dataType: "json",
		success: function (data) {
			//console.log("i am in success-->" + data);//返回的是数组对象data
			var items = $.each(data, function(value){
				return value; //遍历数组中的值
			});
			success(items);//调用loader的success方法，将items添加到下拉框中,这里是难点啊，之前后台已经返回数据了，但就是不添加到下拉框
		}
	});
};
</script>
</body>
</html>