<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>
<body>
<div>
	<div style="margin: 10px 10px 0px 10px;background-color: #FFFACD;font-size: 14px">
	包名：<font color="red">com.guy.项目名称.模块名称</font><br/>
	项目名称请用小写英文，模块名称请按照实体类规范命名。
	</div>
	<form id="mainform" action="${ctx}/sys/autoCode/${action}" method="post">
	<table  class="formTable">
		<tr>
			<td>项目名称</td>
			<td>
				<input id="packageName" name="packageName" type="text" class="easyui-validatebox"  data-options="width: 180,required:'required',validType:'english'"/>
			</td>
		</tr>
		<tr>
			<td>模块名称：</td>
			<td><input id="objectName" name="objectName" type="text" class="easyui-validatebox"  data-options="width: 180,required:'required',validType:'english'" class="easyui-validatebox"/></td>
		</tr>
	</table>
	</form>
	<div style="margin: 10px 10px 0px 10px;background-color: #FFFACD;font-size: 14px">
		简易代码生成
	</div>
</div> 
<script type="text/javascript">

$('#mainform').form({    
    onSubmit: function(){    
    	var isValid = $(this).form('validate');
		return isValid;	// 返回false终止表单提交
    },    
    success:function(data){   
    	if(successTip(data,dg,d))
    		dg.treegrid('reload');
    }    
});   


</script>
</body>
</html>