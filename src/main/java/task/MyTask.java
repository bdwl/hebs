package task;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.guy.statistics.cssuser.service.CSSUserService;

public class MyTask {
	@Autowired
	private CSSUserService cssUserService;
    /**  
     * 业务逻辑处理  
     */  
    public void execute() {  
    	System.out.println("任务开始");
    	 cssUserService.updateState(new Date());
    }  
}
