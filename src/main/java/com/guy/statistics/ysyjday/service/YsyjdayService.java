package com.guy.statistics.ysyjday.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.ysyjday.dao.YsyjdayDao;
import com.guy.statistics.ysyjday.entity.Ysyjday;

/** 
 * 类名称：YsyjdayService
 * 创建人：blank 
 * 创建时间：2016-08-05
 */
@Service
@Transactional(readOnly=true)
public class YsyjdayService extends BaseService<Ysyjday, Integer> {
	
	@Autowired
	private YsyjdayDao ysyjdayDao;

	@Override
	public HibernateDao<Ysyjday, Integer> getEntityDao() {
		return ysyjdayDao;
	}
}
