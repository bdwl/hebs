package com.guy.statistics.ysyjday.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.ysyjday.entity.Ysyjday;

/** 
 * 类名称：YsyjdayDao
 * 创建人：blank 
 * 创建时间：2016-08-05
 */
@Repository
public class YsyjdayDao extends HibernateDao<Ysyjday, Integer>{

}
