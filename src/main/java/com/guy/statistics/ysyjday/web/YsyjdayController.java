package com.guy.statistics.ysyjday.web;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.utils.DateUtils;
import com.guy.common.utils.IdGen;
import com.guy.common.web.BaseController;
import com.guy.statistics.ysyjday.entity.Ysyjday;
import com.guy.statistics.ysyjday.service.YsyjdayService;
import com.guy.util.excel.ExcelUtil;

/** 
 * 类名称：YsyjdayController
 * 创建人：blank 
 * 创建时间：2016-08-05
 */
@Controller
@RequestMapping(value="statistics/ysyjday")
public class YsyjdayController extends BaseController {

	@Autowired
	YsyjdayService ysyjdayService;
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list() {
		return "statistics/ysyjdayList";
	}

	/**
	 * 获取json
	 */
	@RequiresPermissions("statistics:ysyjday:view")
	@RequestMapping(value="json",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> ysyjdayList(HttpServletRequest request) {
		Page<Ysyjday> page = getPage(request);
		page.setOrderBy("tbrq");
		page.setOrder(Page.DESC);
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
		page = ysyjdayService.search(page, filters);
		return getEasyUIData(page);
	}
	
	@RequiresPermissions("statistics:ysyjday:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importXls(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        try {
            InputStream file = sj.getInputStream();
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);// 获取导入数据对象集合
            file.close();
            String batch = IdGen.randomBase62(8);
            for (List<Object> entity : list) {
                Ysyjday ysyj = new Ysyjday();
                ysyj.setBatch(batch);
                ysyj.setTbrq(entity.get(0).toString());
                ysyj.setName(entity.get(1).toString());
                ysyj.setSfzh(entity.get(2).toString());
                ysyj.setDwmc(entity.get(3).toString());
                ysyj.setCount(Integer.parseInt(entity.get(4).toString()));
                ysyj.setCreateTime(DateUtils.getSysTimestamp());
                ysyjdayService.save(ysyj);
                result = "success";
            }
        } catch (Exception e) {
            result = "导入数据异常！";
        }
        return result;
    }

	@ModelAttribute
	public void getYsyjday(@RequestParam(value = "id", defaultValue = "-1") Integer id,Model model) {
		if (id != -1) {
			model.addAttribute("ysyjday", ysyjdayService.get(id));
		}
	}

}
