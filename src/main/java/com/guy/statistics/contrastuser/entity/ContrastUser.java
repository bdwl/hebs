package com.guy.statistics.contrastuser.entity;

import com.guy.system.areainfo.entity.AreaInfo;


public class ContrastUser implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private AreaInfo areaInfo;
	private int addNum;
	private int delNum;
	private int delnNum;//待删除

	// Constructors

	/** default constructor */
	public ContrastUser() {
	}

    public AreaInfo getAreaInfo() {
        return areaInfo;
    }

    public void setAreaInfo(AreaInfo areaInfo) {
        this.areaInfo = areaInfo;
    }

    public int getAddNum() {
        return addNum;
    }

    public void setAddNum(int addNum) {
        this.addNum = addNum;
    }

    public int getDelNum() {
        return delNum;
    }

    public void setDelNum(int delNum) {
        this.delNum = delNum;
    }

    public int getDelnNum() {
        return delnNum;
    }

    public void setDelnNum(int delnNum) {
        this.delnNum = delnNum;
    }

    
	
}