package com.guy.statistics.contrastuser.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.contrastuser.entity.ContrastUser;

/** 
 * 类名称：ContrastUserDao
 * 创建人：blank 
 * 创建时间：2016-10-10
 */
@Repository
public class ContrastUserDao extends HibernateDao<ContrastUser, Integer>{

}
