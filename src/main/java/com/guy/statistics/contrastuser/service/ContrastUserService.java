package com.guy.statistics.contrastuser.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.contrastuser.dao.ContrastUserDao;
import com.guy.statistics.contrastuser.entity.ContrastUser;
import com.guy.statistics.cssuser.dao.CSSUserDao;
import com.guy.statistics.cssuser.entity.CSSUser;
import com.guy.system.areainfo.dao.AreaInfoDao;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.util.DateUtil;

/** 
 * 类名称：ContrastUserService
 * 创建人：blank 
 * 创建时间：2016-10-10
 */
@Service
@Transactional(readOnly=true)
public class ContrastUserService extends BaseService<ContrastUser, Integer> {
	
	@Autowired
	private ContrastUserDao contrastuserDao;
	@Autowired
	private CSSUserDao cssUserDao;

	@Override
	public HibernateDao<ContrastUser, Integer> getEntityDao() {
		return contrastuserDao;
	}
	
	/**
	 * 获取新增and删除集合
	 * @return
	 */
	public List<ContrastUser> getList(List<AreaInfo> areaInfos){
	    Date start=DateUtil.parse("2016-08-25","yyyy-MM-dd");
	    List<CSSUser> list=cssUserDao.getAllByRq(start);
	   
	    List<ContrastUser> contrastUsers= new ArrayList<ContrastUser>();
	    for (AreaInfo areaInfo : areaInfos) {
	        int addNum=0;
	        int delNum=0;
	        int delnNum=0;
	        ContrastUser contrastUser= new ContrastUser();
            for (CSSUser cssuser : list) {
                if (cssuser.getAreaInfo().equals(areaInfo)) {
                    if("D".equals(cssuser.getDelFlag())){
                        delNum++;
                    }
                    if("ND".equals(cssuser.getDelFlag())){
                        delnNum++;
                    }
                    if("N".equals(cssuser.getDelFlag())){
                        if(cssuser.getCreatedate().getTime()>start.getTime()){
                            addNum++;
                        }
                        
                    }
                } 
            }
            contrastUser.setAddNum(addNum);
            contrastUser.setDelnNum(delnNum);
            contrastUser.setDelNum(delNum);
            contrastUser.setAreaInfo(areaInfo);
            contrastUsers.add(contrastUser);
        }
        return contrastUsers;
	    
	}
}
