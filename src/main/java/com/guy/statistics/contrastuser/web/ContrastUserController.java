package com.guy.statistics.contrastuser.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.contrastuser.entity.ContrastUser;
import com.guy.statistics.contrastuser.service.ContrastUserService;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.areainfo.service.AreaInfoService;

/** 
 * 获取用户新增和删除数据
 * 类名称：ContrastUserController
 * 创建人：blank 
 * 创建时间：2016-10-10
 */
@Controller
@RequestMapping(value="statistics/contrastuser")
public class ContrastUserController extends BaseController {

	@Autowired
	ContrastUserService contrastuserService;
	@Autowired
	AreaInfoService areaInfoService;
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list() {
		return "statistics/contrastuserList";
	}

	/**
	 * 获取json
	 */
	@RequiresPermissions("statistics:contrastuser:view")
	@RequestMapping(value="json",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> contrastuserList(HttpServletRequest request) {
	    List<AreaInfo> areaInfos=areaInfoService.getHb();
	    List<ContrastUser> contrastUsers=contrastuserService.getList(areaInfos);
	    Page<ContrastUser> page= new Page<ContrastUser>();
	    page.setResult(contrastUsers);
	    page.setTotalCount(contrastUsers.size());
		return getEasyUIData(page);
	}
	
	
	@ModelAttribute
	public void getContrastUser(@RequestParam(value = "id", defaultValue = "-1") Integer id,Model model) {
		if (id != -1) {
			model.addAttribute("contrastuser", contrastuserService.get(id));
		}
	}

}
