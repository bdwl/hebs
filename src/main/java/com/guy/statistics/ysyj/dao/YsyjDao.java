package com.guy.statistics.ysyj.dao;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.ysyj.entity.Ysyj;

/** 
 * 类名称：YsyjDao
 * 创建人：blank 
 * 创建时间：2016-08-04
 */
@Repository
public class YsyjDao extends HibernateDao<Ysyj, Integer>{

    public int delAll() {
        String hql="delete from Ysyj";
        Query query=getSession().createQuery(hql);
        int result=query.executeUpdate();      
        return result;
    }

}
