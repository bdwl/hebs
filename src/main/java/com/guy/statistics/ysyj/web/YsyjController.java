package com.guy.statistics.ysyj.web;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.utils.DateUtils;
import com.guy.common.utils.IdGen;
import com.guy.common.web.BaseController;
import com.guy.statistics.ysyj.entity.Ysyj;
import com.guy.statistics.ysyj.service.YsyjService;
import com.guy.util.excel.ExcelUtil;

/**
 * 类名称：YsyjController 创建人：blank 创建时间：2016-08-04
 */
@Controller
@RequestMapping(value = "statistics/ysyj")
public class YsyjController extends BaseController {

    private static Logger log = Logger.getLogger(YsyjController.class);
    
    @Autowired
    YsyjService ysyjService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/ysyjList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:ysyj:view")
    @RequestMapping(value="json",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> ysyjList(HttpServletRequest request) {
        Page<Ysyj> page = getPage(request);
        page.setOrderBy("tbrq");
        page.setOrder(Page.DESC);
        List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
        page = ysyjService.search(page, filters);
        return getEasyUIData(page);
    }
    
    @RequiresPermissions("statistics:ysyj:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importXls(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        try {
            InputStream file = sj.getInputStream();
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);// 获取导入数据对象集合
            file.close();
//            int count= ysyjService.delAll();
//            log.info("【清空表】导入数据清空云搜预警表，删除"+count+"条数据！");
            String batch = IdGen.randomBase62(8);
            for (List<Object> entity : list) {
                Ysyj ysyj = new Ysyj();
                ysyj.setBatch(batch);
                ysyj.setTbrq(entity.get(0).toString());
                ysyj.setName(entity.get(1).toString());
                ysyj.setSfzh(entity.get(2).toString());
                ysyj.setDwmc(entity.get(3).toString());
                ysyj.setCount(Integer.parseInt(entity.get(4).toString()));
                ysyj.setCreateTime(DateUtils.getSysTimestamp());
                ysyjService.save(ysyj);
                result = "success";
            }
        } catch (Exception e) {
            result = "导入数据异常！";
        }
        return result;
    }

    @ModelAttribute
    public void getYsyj(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("ysyj", ysyjService.get(id));
        }
    }

}
