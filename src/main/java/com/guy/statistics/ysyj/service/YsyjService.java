package com.guy.statistics.ysyj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.ysyj.dao.YsyjDao;
import com.guy.statistics.ysyj.entity.Ysyj;

/** 
 * 类名称：YsyjService
 * 创建人：blank 
 * 创建时间：2016-08-04
 */
@Service
@Transactional(readOnly=true)
public class YsyjService extends BaseService<Ysyj, Integer> {
	
	@Autowired
	private YsyjDao ysyjDao;

	@Override
	public HibernateDao<Ysyj, Integer> getEntityDao() {
		return ysyjDao;
	}
	
	@Transactional(readOnly=false)
	public int delAll(){
	   return ysyjDao.delAll();
	}
}
