package com.guy.statistics.cssinfo.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.web.BaseController;
import com.guy.statistics.cssinfo.entity.Chart;
import com.guy.statistics.cssinfo.entity.InfoXls;
import com.guy.statistics.cssinfo.entity.LoginZeroInfo;
import com.guy.statistics.cssinfo.entity.NoLogin;
import com.guy.statistics.cssinfo.service.CSSInfoService;
import com.guy.statistics.cssuser.service.CSSUserService;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.areainfo.service.AreaInfoService;
import com.guy.system.user.entity.User;
import com.guy.system.user.service.UserRoleService;
import com.guy.system.utils.UserUtil;
import com.guy.util.DateUtil;
import com.guy.util.excelTools.ExcelUtils;
import com.guy.util.excelTools.JsGridReportBase;
import com.guy.util.excelTools.TableData;

@Controller
@RequestMapping("statistics/cssinfo")
public class CSSInfoController extends BaseController {
    private static Logger log = Logger.getLogger(CSSInfoController.class);
    
    @Autowired
    private CSSUserService cssUserService;
    @Autowired
    private CSSInfoService cssInfoService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    AreaInfoService areaInfoService;
    
    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String go() {
        return "statistics/cipanel";
    }

    /**
     * 查詢最多的人
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "maxShjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getmaxShData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("查詢最多的人==>参数："+start+","+end+",");
        return cssInfoService.getMaxShData(start,end,bool,user,areaCode);
    }

    /**
     * 导出最多的人
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "maxOutjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getmaxOutData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("导出最多的人==>参数："+start+","+end+",");
        return cssInfoService.getmaxOutData(start,end,bool,user,areaCode);
    }

    /**
     * 查詢——点击——导出 统计
     * @param request
     * @return
     */
    @RequestMapping(value = "ccojson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getCcoData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("查詢——点击——导出 统计==>参数："+start+","+end+",");
        return cssInfoService.getCcoData(start,end,bool,user,areaCode);
    }

    /**
     * 各地市高级用户统计
     * @param request
     * @return
     */
    @RequestMapping(value = "ucjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getmUserCountData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        log.info("各地市高级用户统计==>参数："+start+","+end+",");
        return cssInfoService.getmUserCountData(start,end);
    }

    /**
     * 各地市查询次数
     * @param request
     * @return
     */
    @RequestMapping(value = "scjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getSearchCountData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        log.info("各地市查询次数==>参数："+start+","+end+",");
        return cssInfoService.getSearchCountData(start,end);
    }

    /**
     * 各地市导出次数统计
     * @param request
     * @return
     */
    @RequestMapping(value = "ocjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getOutCountData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        log.info("各地市导出次数统计==>参数："+start+","+end+",");
        return cssInfoService.getOutCountData(start,end);
    }
    
    /**
     * 各地市操作日志统计
     * @param request
     * @return
     */
    @RequestMapping(value = "rzcjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getRzCountData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("各地市操作日志次数统计==>参数"+start+","+end+",");
        return cssInfoService.getRzCountData(start,end,bool,user,areaCode);
    }
    
    /**
     * 0登录人员
     * @param request
     * @return
     */
    @RequestMapping(value = "nologjson", method = RequestMethod.POST)
    @ResponseBody
    public Chart getNoLoginData(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("0登录人员==>参数："+start+","+end+",");
        return cssInfoService.getNoLoginData(start,end,bool,user,areaCode);
    }
    
    @RequestMapping(value = "noLogjson", method = RequestMethod.POST)
    @ResponseBody
    public  List<NoLogin> getNoLogin(HttpServletRequest request) {
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");
        String areaCode=request.getParameter("areaCode");
        User user = UserUtil.getCurrentUser();
        boolean bool = userRoleService.isNormal(user);
        log.info("0登录人员==>参数："+start+","+end+",");
        List<NoLogin> noLogins=new ArrayList<NoLogin>();
        Map<String, List<LoginZeroInfo>> map=cssInfoService.getNoLogin(start,end,bool,user,areaCode);
        for (Map.Entry<String, List<LoginZeroInfo>> entry : map.entrySet()) {  
            NoLogin noLogin = new NoLogin();
            noLogin.setName(entry.getKey());
            noLogin.setInfos(entry.getValue());
            noLogins.add(noLogin);
        }  
        return noLogins;
    }
   
    @RequiresPermissions(" statistics:ci:export")
    @RequestMapping(value = "exportExcel", method = RequestMethod.POST)
    @ResponseBody
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception{
        response.setContentType("application/msexcel;charset=UTF-8");
        String start =request.getParameter("startDate");
        String end =request.getParameter("endDate");
//        if(start!=null&&!"".equals(start)){
//            List<TableData> tds = new ArrayList<TableData>();
//            String title="";
//            String sheetTitle="";
//            if(start.equals(end)){
//                title="信息统计"+start;
//                sheetTitle=start+"云搜索平台用户数据统计";
//            }else{
//                title="信息统计"+start+"至"+end;
//                sheetTitle=start+"至"+end+"云搜索平台用户数据统计";
//            }
//            User user= UserUtil.getCurrentUser();
//            AreaInfo areaInfo= areaInfoService.getInfoByCode(user.getAreaCode());
//                Calendar cal = Calendar.getInstance();  
//                List<InfoXls> list = cssInfoService.getInfoXls(areaInfo,start,end);//获取数据
//                String[] hearders = new String[] {" 姓名 ", "  身份证号  ","       单位       ","资源平台登入数","云搜索登入数","云搜索查询数","云搜索点击数","云搜索导出数","云搜索操作日志"};//表头数组
//                String[] fields = new String[] {"name", "sfzh","organization","zyptLoginNum","yssLoginNum","yssSearchNum","yssClickNum","yssExportNum","yssOplogNum"};//People对象属性数组
//                TableData td = ExcelUtils.createTableData(list, ExcelUtils.createTableHeader(hearders),fields);
//                td.setSheetTitle(sheetTitle);
//                tds.add(td);
//            JsGridReportBase report = new JsGridReportBase(request, response);
//            report.exportToExcel(title, user.getName(), tds);
//        }else{
            List<TableData> tds = new ArrayList<TableData>();
            String title="信息统计"+DateUtil.format(new Date(), DateUtil.FORMAT_LONG_F);
            User user= UserUtil.getCurrentUser();
            AreaInfo areaInfo= areaInfoService.getInfoByCode(user.getAreaCode());
            List<String> months=getMonths(start, end);
            for (String month : months) {
                List<InfoXls> list = cssInfoService.getInfoXls(areaInfo,month);//获取数据
                String[] hearders = new String[] {" 姓名 ", "  身份证号  ","       单位       ","资源平台登入数","云搜索登入数","云搜索查询数","云搜索点击数","云搜索导出数","云搜索操作日志"};//表头数组
                String[] fields = new String[] {"name", "sfzh","organization","zyptLoginNum","yssLoginNum","yssSearchNum","yssClickNum","yssExportNum","yssOplogNum"};//People对象属性数组
                TableData td = ExcelUtils.createTableData(list, ExcelUtils.createTableHeader(hearders),fields);
                td.setSheetTitle(month+"云搜索平台用户数据统计");
                tds.add(td);
            }
           /* for (int i = 1; i <= 12; i++) {
                Calendar cal = Calendar.getInstance();  
                String month=cal.get(Calendar.YEAR)+ "-"+lpad(2, i);
                List<InfoXls> list = cssInfoService.getInfoXls(areaInfo,month);//获取数据
                String[] hearders = new String[] {" 姓名 ", "  身份证号  ","       单位       ","资源平台登入数","云搜索登入数","云搜索查询数","云搜索点击数","云搜索导出数","云搜索操作日志"};//表头数组
                String[] fields = new String[] {"name", "sfzh","organization","zyptLoginNum","yssLoginNum","yssSearchNum","yssClickNum","yssExportNum","yssOplogNum"};//People对象属性数组
                TableData td = ExcelUtils.createTableData(list, ExcelUtils.createTableHeader(hearders),fields);
                td.setSheetTitle(month+"云搜索平台用户数据统计");
                tds.add(td);
            }*/
            JsGridReportBase report = new JsGridReportBase(request, response);
            report.exportToExcel(title, user.getName(), tds);
//        }
        
    }
    
    public static List<String> getMonths(String start,String end){
        Date startdate= DateUtil.parse(start, "yyyy-MM");
        Date enddate= DateUtil.parse(end, "yyyy-MM");
        Calendar dd = Calendar.getInstance();//定义日期实例
        dd.setTime(startdate);
        List<String> mmStr=new ArrayList<String>();
        String mm=DateUtil.format(dd.getTime(),"yyyy-MM");
        mmStr.add(mm);
          while(dd.getTime().before(enddate)){
              dd.add(Calendar.MONTH, 1);//进行当前日期月份加1
              mm=DateUtil.format(dd.getTime(),"yyyy-MM");
              mmStr.add(mm);
          }
          return mmStr;
    }
    
  /*  public static String[] getLast12Months(){  
        
        String[] last12Months = new String[12];  
          
        Calendar cal = Calendar.getInstance();  
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1); //要先+1,才能把本月的算进去</span>  
        for(int i=0; i<12; i++){  
            last12Months[11-i] = cal.get(Calendar.YEAR)+ "-" + (cal.get(Calendar.MONTH)+1);  
        }  
        
        String start ="2016-06";
        String end ="2016-07";
        Date startdate= DateUtil.parse(start, "yyyy-MM");
        Date enddate= DateUtil.parse(end, "yyyy-MM");
        Calendar dd = Calendar.getInstance();//定义日期实例
        dd.setTime(startdate);
        List<String> mmStr=new ArrayList<String>();
          while(dd.getTime().before(enddate)){
              String mm=DateUtil.format(dd.getTime(),"MM");
              mmStr.add(mm);
              dd.add(Calendar.MONTH, 1);//进行当前日期月份加1
          }
         for (String string : mmStr) {
            System.out.println(string);
        }
        return last12Months;  
    }  */
    /*
    public static void main(String[] args) {
        String start ="2016-06";
        String end ="2016-10";
        Date startdate= DateUtil.parse(start, "yyyy-MM");
        Date enddate= DateUtil.parse(end, "yyyy-MM");
        Calendar dd = Calendar.getInstance();//定义日期实例
        dd.setTime(startdate);
        List<String> mmStr=new ArrayList<String>();
        String mm=DateUtil.format(dd.getTime(),"MM");
        mmStr.add(mm);
          while(dd.getTime().before(enddate)){
              dd.add(Calendar.MONTH, 1);//进行当前日期月份加1
              mm=DateUtil.format(dd.getTime(),"MM");
              mmStr.add(mm);
          }
         for (String string : mmStr) {
            System.out.println(string);
        }
    }*/
    
    /**
     * 补齐不足长度
     * @param length
     * @param number
     * @return
     */
    static String lpad(int length, int number) {
        String f = "%0" + length + "d";
        return String.format(f, number);
    }
}
