package com.guy.statistics.cssinfo.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.cssinfo.entity.CSSInfo;
import com.guy.statistics.cssinfo.entity.Chart;
import com.guy.statistics.cssinfo.entity.ChartData;
import com.guy.statistics.cssinfo.entity.InfoXls;
import com.guy.statistics.cssinfo.entity.LoginZeroInfo;
import com.guy.statistics.cssinfo.entity.ObjCount;
import com.guy.statistics.cssinfo.entity.ObjUser;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.utils.UserUtil;

/**
 * 云搜索统计信息DAO
 * 
 * @author TONY
 * 
 */
@Repository
public class CSSInfoDao extends HibernateDao<CSSInfo, Integer> {
    private static Logger log = Logger.getLogger(CSSInfoDao.class);
    /**
     * 查询最多的人员
     * 
     * @param start
     * @param end
     * @return
     */
    @SuppressWarnings("unchecked")
    public Chart getMaxShData(String start, String end, String areaCode) {
        Chart chart = new Chart();
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssSearchNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu,AreaInfo ai"
//                + " where  cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.createTime>=?0 and ci.createTime<?1) and cu.areaInfo=ai and ai.areaCode like ?2 group by cu.name order by sum(ci.yssSearchNum) desc";
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssSearchNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu,AreaInfo ai"
                + " where  cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2 group by cu.name order by sum(ci.yssSearchNum) desc";
        List<ObjUser> list = null;
        try {
            list = (List<ObjUser>) createQuery(hql, start, end, areaCode + "%").setMaxResults(10).list();
        } catch (Exception e1) {
            // e1.printStackTrace();
        }
        if (list != null) {
            String[] categories = new String[list.size()];
            int[] data = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
                data[i] = (int) list.get(i).getCount();
            }
            chart.setCategories(categories);
            chart.setData(data);
        }
        log.info("查询最多的人员DAO");
        return chart;

    }

    /**
     * 导出最多的人员
     * 
     * @param start
     * @param end
     * @return
     */
    @SuppressWarnings("unchecked")
    public Chart getmaxOutData(String start, String end, String areaCode) {
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssExportNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu,AreaInfo ai"
                + " where cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2 group by cu.name order by sum(ci.yssExportNum) desc";
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssExportNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu,AreaInfo ai"
//                + " where cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.createTime>=?0 and ci.createTime<?1) and cu.areaInfo=ai and ai.areaCode like ?2 group by cu.name order by sum(ci.yssExportNum) desc";

        // String hql =
        // "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssExportNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu "
        // +
        // "where ci.cssUser.id =cu.id and (ci.createTime>=?0 and ci.createTime<?1) group by cu.name order by sum(ci.yssExportNum) desc";
        List<ObjUser> list = null;
        try {
            list = (List<ObjUser>) createQuery(hql, start, end, areaCode + "%").setMaxResults(10).list();
        } catch (Exception e1) {
            // e1.printStackTrace();
        }
        if (list != null) {
            String[] categories = new String[list.size()];
            int[] data = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
                data[i] = (int) list.get(i).getCount();
            }
            chart.setCategories(categories);
            chart.setData(data);
        }
        log.info("导出最多的人员DAO");
        return chart;
    }

    /**
     * 查询-点击-导出统计
     * 
     * @param start
     * @param end
     * @return
     */
    @SuppressWarnings("unchecked")
    public Chart getCcoData(String start, String end, String areaCode) {
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjCount(sum(ci.yssExportNum) as ocount,sum(ci.yssSearchNum) as scount,sum(ci.yssClickNum) as ccount,sum(ci.yssOplogNum) as rzcount) from CSSInfo ci,CSSUser cu,AreaInfo ai "
                + "where cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2";
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjCount(sum(ci.yssExportNum) as ocount,sum(ci.yssSearchNum) as scount,sum(ci.yssClickNum) as ccount) from CSSInfo ci,CSSUser cu,AreaInfo ai "
//                + "where cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.createTime>=?0 and ci.createTime<?1) and cu.areaInfo=ai and ai.areaCode like ?2";
        List<ObjCount> list = null;
        try {
            list = (List<ObjCount>) createQuery(hql, start, end, areaCode + "%").setMaxResults(10).list();
        } catch (Exception e1) {
            // e1.printStackTrace();
        }
        if (list != null) {
            ChartData chartDataS = new ChartData();
            chartDataS.setName("查询数");
            chartDataS.setY(list.get(0).getScount());
            chartDataS.setSliced(true);
            chartDataS.setSelected(true);
            ChartData chartDataC = new ChartData();
            chartDataC.setName("点击数");
            chartDataC.setY(list.get(0).getCcount());
            chartDataC.setSliced(false);
            chartDataC.setSelected(false);
            ChartData chartDataO = new ChartData();
            chartDataO.setName("导出数");
            chartDataO.setY(list.get(0).getOcount());
            chartDataO.setSliced(false);
            chartDataO.setSelected(false);
            ChartData chartDataRz = new ChartData();
            chartDataRz.setName("操作数");
            chartDataRz.setY(list.get(0).getRzcount());
            chartDataRz.setSliced(false);
            chartDataRz.setSelected(false);
            ChartData[] data = new ChartData[] { chartDataC, chartDataS, chartDataO,chartDataRz };
            chart.setChartData(data);
        }
        log.info("查询-点击-导出统计DAO");
        return chart;
    }

    /**
     * 各地市高级用户数统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getmUserCountData(String start, String end) {
//      Date s = DateUtil.parse(start, DateUtil.FORMAT_SHORT_MONTH);
//      Date e = DateUtil.parse(end, DateUtil.FORMAT_SHORT_MONTH);
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(count(cu.id) as count,ai.areaName as name) from CSSUser cu, AreaInfo ai "
                + "where  cu.delFlag='N' and cu.areaInfo=ai  group by ai.areaName order by count(cu.id) desc";
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(count(cu.id) as count,ai.areaName as name) from CSSUser cu, AreaInfo ai "
//                + "where  cu.delFlag<>'D' and cu.areaInfo=ai and (cu.opdate>=?0 and cu.opdate<=?1) group by ai.areaName order by count(cu.id) desc";
        List<ObjUser> list=null;
        try {
            list = super.find(hql);
        } catch (Exception e1) {
//            e.printStackTrace();
        }
        if(list!=null){
            String[] categories = new String[list.size()];
            int[] data = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
                data[i] = (int) list.get(i).getCount();
            }
            chart.setCategories(categories);
            chart.setData(data);
        }
        log.info("各地市高级用户数统计DAO");
        return chart;
    }

    /**
     * 各地市查询次数统计 SELECT sum(t2.yss_search_num),t3.AREA_NAME,t3.AREA_CODE from
     * t_css_user t1,t_css_info t2,sys_area_info t3 where t2.css_user_id=t1.id
     * and t3.ID=t1.area_id group by t3.AREA_CODE order by
     * sum(t2.yss_search_num) desc
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getSearchCountData(String start, String end) {
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssSearchNum) as count,ai.areaName as name) from "
                + "CSSUser cu,CSSInfo ci,AreaInfo ai where  cu.delFlag<>'D' and ci.cssUser=cu and cu.areaInfo=ai and (ci.batchrq>=?0 and ci.batchrq<=?1) group by ai.areaCode order by sum(ci.yssSearchNum) desc";
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssSearchNum) as count,ai.areaName as name) from "
//                + "CSSUser cu,CSSInfo ci,AreaInfo ai where  cu.delFlag<>'D' and ci.cssUser=cu and cu.areaInfo=ai and (ci.createTime>=?0 and ci.createTime<?1) group by ai.areaCode order by sum(ci.yssSearchNum) desc";
        List<ObjUser> list=null;
        try {
            list = super.find(hql,start,end);
        } catch (Exception e1) {
//            e.printStackTrace();
        }
        if (list!=null) {
            String[] categories = new String[list.size()];
            int[] data = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
                data[i] = (int) list.get(i).getCount();
            }
            chart.setCategories(categories);
            chart.setData(data);
        }
        log.info("各地市查询次数统计DAO");
        return chart;
    }

    /**
     * 各地市导出次数统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getOutCountData(String start, String end) {
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssExportNum) as count,ai.areaName as name) from "
                + "CSSUser cu,CSSInfo ci,AreaInfo ai where  cu.delFlag<>'D' and ci.cssUser=cu and cu.areaInfo=ai and (ci.batchrq>=?0 and ci.batchrq<=?1) group by ai.areaCode order by sum(ci.yssSearchNum) desc";
//        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssExportNum) as count,ai.areaName as name) from "
//                + "CSSUser cu,CSSInfo ci,AreaInfo ai where  cu.delFlag<>'D' and ci.cssUser=cu and cu.areaInfo=ai and (ci.createTime>=?0 and ci.createTime<?1) group by ai.areaCode order by sum(ci.yssSearchNum) desc";
        List<ObjUser> list=null;
        try {
            list = super.find(hql,start,end);
        } catch (Exception e1) {
//            e.printStackTrace();
        }
        if (list!=null) {
            String[] categories = new String[list.size()];
            int[] data = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
                data[i] = (int) list.get(i).getCount();
            }
            chart.setCategories(categories);
            chart.setData(data);
        }
        log.info("各地市导出次数统计DAO");
        return chart;
    }

    /**
     * 零登录人员
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getNoLoginData(String start, String end, String areaCode) {
        Chart chart = new Chart();
        String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(cu.name as name) from CSSUser cu,CSSInfo ci,AreaInfo ai "
                + "where cu.delFlag<>'D' and ci.cssUser=cu and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2 and ci.yssLoginNum=0";        List<ObjUser> list = null;
        try {
            list = super.find(hql, start, end, areaCode + "%");
        } catch (Exception e1) {
             e1.printStackTrace();
        }
        if (list != null) {
            String[] categories = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                categories[i] = list.get(i).getName();
            }
            chart.setCategories(categories);
        }
        log.info("零登录人员DAO");
        return chart;
    }
    
    public List<LoginZeroInfo> getNoLogin(String start, String end, String areaCode) {
        List<LoginZeroInfo> infos = null;
        String hql = "select new com.guy.statistics.cssinfo.entity.LoginZeroInfo(cu.name as name,cu.sfzh as sfzh,ci.batchrq as rq) from CSSUser cu,CSSInfo ci,AreaInfo ai "
                + "where cu.delFlag<>'D' and ci.cssUser=cu and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2 and ci.yssLoginNum=0 and ci.zyptLoginNum=0";     
                try {
                    infos = super.find(hql, start, end, areaCode + "%");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
              /*  if (list != null) {
                    String[] categories = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        categories[i] = list.get(i).getName();
                    }
                    info.setCategories(categories);
                }*/
            return infos;
    }
    
    public Chart getRzCountData(String start, String end, String areaCode) {
            Chart chart = new Chart();
//            String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssOplogNum) as count,ai.areaName as name) from "
//                    + "CSSUser cu,CSSInfo ci,AreaInfo ai where  cu.delFlag<>'D' and ci.cssUser=cu and cu.areaInfo=ai and (ci.batchrq>=?0 and ci.batchrq<=?1) and ai.areaCode like ?2 group by ai.areaCode  order by sum(ci.yssOplogNum) desc";
            String hql = "select new com.guy.statistics.cssinfo.entity.ObjUser(sum(ci.yssOplogNum) as count,cu.name as name) from CSSInfo ci,CSSUser cu,AreaInfo ai"
                    + " where cu.delFlag<>'D' and ci.cssUser.id =cu.id and (ci.batchrq>=?0 and ci.batchrq<=?1) and cu.areaInfo=ai and ai.areaCode like ?2 group by cu.name order by sum(ci.yssOplogNum) desc";

            List<ObjUser> list = null;
            try {
                list = (List<ObjUser>) createQuery(hql, start, end, areaCode + "%").setMaxResults(10).list();
            } catch (Exception e1) {
                 e1.printStackTrace();
            }
            if (list != null) {
                String[] categories = new String[list.size()];
                int[] data = new int[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    categories[i] = list.get(i).getName();
                    data[i] = (int) list.get(i).getCount();
                }
                chart.setCategories(categories);
                chart.setData(data);
            }
            log.info("操作日志DAO");
            return chart;
    }
    
    
    public List<InfoXls> getInfoList(AreaInfo areaInfo,String month){
        StringBuffer hql=new StringBuffer();
        List<InfoXls> infoXls=null;      
        hql.append("select new com.guy.statistics.cssinfo.entity.InfoXls(cssUser.name as name,cssUser.sfzh as sfzh,cssUser.organization as organization,batch,zyptLoginNum,yssLoginNum,yssSearchNum,yssClickNum,yssExportNum,yssOplogNum)from CSSInfo"
                + " where batchrq = ?0");
        if(!UserUtil.getCurrentUser().getLoginName().equals("admin")){
            hql.append(" and cssUser.areaInfo=?1 ");
            infoXls = super.find(hql.toString(),month,areaInfo);
        }else{//查询全部地区信息
            infoXls = super.find(hql.toString(),month);
        }
        System.out.println(hql);
        return infoXls;
    }
    
    public List<InfoXls> getInfoList(AreaInfo areaInfo,String start,String end){
        StringBuffer hql=new StringBuffer();
        List<InfoXls> infoXls=null;      
        hql.append("select new com.guy.statistics.cssinfo.entity.InfoXls(cssUser.name as name,cssUser.sfzh as sfzh,cssUser.organization as organization,batch,zyptLoginNum,yssLoginNum,yssSearchNum,yssClickNum,yssExportNum,yssOplogNum)from CSSInfo"
                + " where batchrq <= ?0 and batchrq<=?1");
        if(!UserUtil.getCurrentUser().getLoginName().equals("admin")){
            hql.append(" and cssUser.areaInfo=?2 ");
            infoXls = super.find(hql.toString(),start,end,areaInfo);
        }else{//查询全部地区信息
            infoXls = super.find(hql.toString(),start,end);
        }
        System.out.println(hql);
        return infoXls;
    }
    
}
