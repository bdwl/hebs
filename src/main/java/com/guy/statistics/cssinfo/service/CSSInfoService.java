package com.guy.statistics.cssinfo.service;

import com.guy.common.mapper.JsonMapper;
import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.cssinfo.dao.CSSInfoDao;
import com.guy.statistics.cssinfo.entity.CSSInfo;
import com.guy.statistics.cssinfo.entity.Chart;
import com.guy.statistics.cssinfo.entity.InfoXls;
import com.guy.statistics.cssinfo.entity.LoginZeroInfo;
import com.guy.system.areainfo.dao.AreaInfoDao;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.user.entity.User;
import com.guy.util.DateUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(readOnly = true)
public class CSSInfoService extends BaseService<CSSInfo, Integer> {
    private static Logger log = Logger.getLogger(CSSInfoService.class);
    @Autowired
    private CSSInfoDao cssInfoDao;

    @Autowired
    private AreaInfoDao areaInfoDao;

    @Override
    public HibernateDao<CSSInfo, Integer> getEntityDao() {
        return cssInfoDao;
    }

    /**
     * 抽象方法
     * 
     * @param start
     * @param end
     * @param isNormal
     * @param user
     * @param areaCode
     * @return
     */
    private ObjParams getObj(String start, String end, boolean isNormal, User user, String areaCode) {
        Date nowDate = DateUtil.addMonth(new Date(), -1);
        String now = DateUtil.format(nowDate, DateUtil.FORMAT_SHORT_MONTH);
        ObjParams params = new ObjParams();
        if (start == null || "".equals(start)) {
            start = now;
        }
        if (end == null || "".equals(end)) {
            end = now;
        }
        if (user != null) {
            if (isNormal) {// 普通用户
                areaCode = user.getAreaCode();
            } else {
                if (areaCode == null || "".equals(areaCode)) {
                    areaCode = "1";// 管理员查看全部
                } else {
                    int id = Integer.parseInt(areaCode);
                    areaCode = areaInfoDao.find(id).getAreaCode();
                    areaCode = areaCode.substring(0, 5);
                }
            }
        }
        // Date s = DateUtil.parse(start, DateUtil.FORMAT_SHORT_MONTH);
        // Date e = DateUtil.parse(end, DateUtil.FORMAT_SHORT_MONTH);
        params.setStart(start);
        params.setEnd(end);
        params.setCode(areaCode);
        String p = (new JsonMapper()).toJson(params);
        log.info("抽象方法参数" + p);
        return params;
    }

    /**
     * 查询最多的人员
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getMaxShData(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        return cssInfoDao.getMaxShData(st, et, code);
    }

    /**
     * 导出最多的人员
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getmaxOutData(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        return cssInfoDao.getmaxOutData(st, et, code);
    }

    /**
     * 查询——点击——导出统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getCcoData(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        return cssInfoDao.getCcoData(st, et, code);
    }

    /**
     * 各地市高级用户数统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getmUserCountData(String start, String end) {
        ObjParams params = getObj(start, end, false, null, "");
        String st = params.getStart();
        String et = params.getEnd();
        return cssInfoDao.getmUserCountData(st, et);
    }

    /**
     * 各地市查询次数统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getSearchCountData(String start, String end) {
        ObjParams params = getObj(start, end, false, null, "");
        String st = params.getStart();
        String et = params.getEnd();
        return cssInfoDao.getSearchCountData(st, et);
    }

    /**
     * 各地市导出次数统计
     * 
     * @param start
     * @param end
     * @return
     */
    public Chart getOutCountData(String start, String end) {
        ObjParams params = getObj(start, end, false, null, "");
        String st = params.getStart();
        String et = params.getEnd();
        return cssInfoDao.getOutCountData(st, et);
    }

    public Chart getNoLoginData(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        return cssInfoDao.getNoLoginData(st, et, code);
    }

    public Map<String, List<LoginZeroInfo>> getNoLogin(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        List<LoginZeroInfo> infos = cssInfoDao.getNoLogin(st, et, code);
        List<LoginZeroInfo> _infos = cssInfoDao.getNoLogin(st, et, code);
        for (LoginZeroInfo loginZeroInfo : infos) {
             int i=1;
             List<String> rqs=new ArrayList<String>();
            for (LoginZeroInfo _loginZeroInfo : _infos) {
                if (_loginZeroInfo.getSfzh().equals(loginZeroInfo.getSfzh())) {
                    _loginZeroInfo.setCount(i);
                    rqs.add(loginZeroInfo.getRq());
                    _loginZeroInfo.setRqs(rqs);
                    i++;
                }
            }

        }
        return  this.groupBys(_infos);
    }

    public Chart getRzCountData(String start, String end, boolean isNormal, User user, String areaCode) {
        ObjParams params = getObj(start, end, isNormal, user, areaCode);
        String st = params.getStart();
        String et = params.getEnd();
        String code = params.getCode();
        return cssInfoDao.getRzCountData(st, et, code);
    }

    public List<InfoXls> getInfoXls(AreaInfo areaInfo, String month) {
        return cssInfoDao.getInfoList(areaInfo, month);
    }

    public List<InfoXls> getInfoXls(AreaInfo areaInfo, String start, String end) {
        return cssInfoDao.getInfoList(areaInfo, start, end);
    }

    public Map<String, List<LoginZeroInfo>> groupBys(List<LoginZeroInfo> lists) {
        Map<String, List<LoginZeroInfo>> map = new HashMap<String, List<LoginZeroInfo>>();
        String key_str = "";
        List<LoginZeroInfo> value_list;

        /*** 将list分组，存入map ***/
        for (LoginZeroInfo cycleData : lists) {
            key_str = cycleData.getSfzh();
            value_list = new ArrayList<LoginZeroInfo>();

            if (key_str != null) {
                if (map.containsKey(key_str)) {
                    value_list = map.get(key_str);
                }
                value_list.add(cycleData);
                map.put(key_str, value_list);

            }
        }
        return map;
    }

    /**
     * 条件参数
     *
     * @author blank
     *
     */
    class ObjParams {
        private String start;
        private String end;
        private String code;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }
}
