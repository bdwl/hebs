package com.guy.statistics.cssinfo.entity;

public class InfoXls {
    private String name;// 姓名
    private String sfzh;// 身份证号码
    private String organization;// 单位
    private String batch;// 批次
    private int zyptLoginNum;// 资源平台登录数
    private int yssLoginNum;// 云搜索登录数
    private int yssSearchNum;// 云搜索查询数
    private int yssClickNum;// 云搜索点击数
    private int yssExportNum;// 云搜索导出数
    private int yssOplogNum;// 操作日志数

    public InfoXls() {
        super();
    }

    public InfoXls(String name, String sfzh, String organization, String batch, int zyptLoginNum, int yssLoginNum,
            int yssSearchNum, int yssClickNum, int yssExportNum, int yssOplogNum) {
        super();
        this.name = name;
        this.sfzh = sfzh;
        this.organization = organization;
        this.batch = batch;
        this.zyptLoginNum = zyptLoginNum;
        this.yssLoginNum = yssLoginNum;
        this.yssSearchNum = yssSearchNum;
        this.yssClickNum = yssClickNum;
        this.yssExportNum = yssExportNum;
        this.yssOplogNum = yssOplogNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public int getZyptLoginNum() {
        return zyptLoginNum;
    }

    public void setZyptLoginNum(int zyptLoginNum) {
        this.zyptLoginNum = zyptLoginNum;
    }

    public int getYssLoginNum() {
        return yssLoginNum;
    }

    public void setYssLoginNum(int yssLoginNum) {
        this.yssLoginNum = yssLoginNum;
    }

    public int getYssSearchNum() {
        return yssSearchNum;
    }

    public void setYssSearchNum(int yssSearchNum) {
        this.yssSearchNum = yssSearchNum;
    }

    public int getYssClickNum() {
        return yssClickNum;
    }

    public void setYssClickNum(int yssClickNum) {
        this.yssClickNum = yssClickNum;
    }

    public int getYssExportNum() {
        return yssExportNum;
    }

    public void setYssExportNum(int yssExportNum) {
        this.yssExportNum = yssExportNum;
    }

    public int getYssOplogNum() {
        return yssOplogNum;
    }

    public void setYssOplogNum(int yssOplogNum) {
        this.yssOplogNum = yssOplogNum;
    }

}
