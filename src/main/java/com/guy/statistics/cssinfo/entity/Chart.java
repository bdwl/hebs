package com.guy.statistics.cssinfo.entity;

public class Chart {

    private String[] categories;
    private int[] data;
    private ChartData[] chartData;

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public ChartData[] getChartData() {
        return chartData;
    }

    public void setChartData(ChartData[] chartData) {
        this.chartData = chartData;
    }
/*    
    public static void main(String[] args) {
        int v=0;
        for (int i = 0; i < 18; i++) {
          
            for (int j = 0; j < 18; j++) {
                System.out.print(v+" ");
                v++;
            }
            System.out.println();
        }
    }*/
    
}
