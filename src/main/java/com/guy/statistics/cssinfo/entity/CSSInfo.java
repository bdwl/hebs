package com.guy.statistics.cssinfo.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.guy.statistics.cssuser.entity.CSSUser;

/**
 * @author TONY 云搜索统计 Cloud Search Statistics Info
 */
@Entity
@Table(name = "t_css_info")
@DynamicUpdate
@DynamicInsert
public class CSSInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    //批次
    private String batch;
    //导入批次日期
    private String batchrq;

    // 云搜索登录数
    private int yssLoginNum;
    // 云搜索查询数
    private int yssSearchNum;
    // 云搜索点击数
    private int yssClickNum;
    // 云搜索导出数
    private int yssExportNum;
    // 操作日志数
    private int yssOplogNum;
    // 导出数
    private int zyptLoginNum;
    //总量
    private int yssTotal;
    //授权时间
    private Timestamp authTime;
    // 创建时间
    private Timestamp createTime;
    private CSSUser cssUser;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name = "BATCH", nullable = false, length = 20)
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @Column(name = "BATCHRQ")
    public String getBatchrq() {
        return batchrq;
    }

    public void setBatchrq(String batchrq) {
        this.batchrq = batchrq;
    }

    @Column(name = "ZYPT_LOGIN_NUM")
    public int getZyptLoginNum() {
        return zyptLoginNum;
    }

    public void setZyptLoginNum(int zyptLoginNum) {
        this.zyptLoginNum = zyptLoginNum;
    }

    @Column(name = "YSS_LOGIN_NUM")
    public int getYssLoginNum() {
        return yssLoginNum;
    }

    public void setYssLoginNum(int yssLoginNum) {
        this.yssLoginNum = yssLoginNum;
    }

    @Column(name = "YSS_SEARCH_NUM")
    public int getYssSearchNum() {
        return yssSearchNum;
    }

    public void setYssSearchNum(int yssSearchNum) {
        this.yssSearchNum = yssSearchNum;
    }

    @Column(name = "YSS_CLICK_NUM")
    public int getYssClickNum() {
        return yssClickNum;
    }

    public void setYssClickNum(int yssClickNum) {
        this.yssClickNum = yssClickNum;
    }

    @Column(name = "YSS_EXPORT_NUM")
    public int getYssExportNum() {
        return yssExportNum;
    }

    public void setYssExportNum(int yssExportNum) {
        this.yssExportNum = yssExportNum;
    }


    @Column(name = "YSS_OPLOG_NUM")
    public int getYssOplogNum() {
        return yssOplogNum;
    }

    public void setYssOplogNum(int yssOplogNum) {
        this.yssOplogNum = yssOplogNum;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "CREATE_DATE", nullable = false, length = 19)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "CSS_USER_ID")
    public CSSUser getCssUser() {
        return cssUser;
    }

    public void setCssUser(CSSUser cssUser) {
        this.cssUser = cssUser;
    }

    @Column(name = "YSS_TOTAL")
    public int getYssTotal() {
        return yssTotal;
    }

    public void setYssTotal(int yssTotal) {
        this.yssTotal = yssTotal;
    }
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "AUTH_TIME")
    public Timestamp getAuthTime() {
        return authTime;
    }

    public void setAuthTime(Timestamp authTime) {
        this.authTime = authTime;
    }
}
