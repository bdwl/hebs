package com.guy.statistics.cssinfo.entity;

import java.util.List;

public class LoginZeroInfo {
    private String name;
    private String sfzh;
    private String rq;
    private int count=0;
    private List<String> rqs;
    
    
    public LoginZeroInfo(String name, String sfzh, String rq) {
        super();
        this.name = name;
        this.sfzh = sfzh;
        this.rq = rq;
    }
    
    public LoginZeroInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSfzh() {
        return sfzh;
    }
    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }
    public String getRq() {
        return rq;
    }
    public void setRq(String rq) {
        this.rq = rq;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

    public List<String> getRqs() {
        return rqs;
    }

    public void setRqs(List<String> rqs) {
        this.rqs = rqs;
    }

   
    
}
