package com.guy.statistics.cssinfo.entity;

public class ObjCount {
    private long scount;// 查询数
    private long ccount;// 点击数
    private long ocount;// 导出数
    private long rzcount;//日志数

    
    public ObjCount() {
        super();
        // TODO Auto-generated constructor stub
    }


    public ObjCount(long scount, long ccount, long ocount, long rzcount) {
        super();
        this.scount = scount;
        this.ccount = ccount;
        this.ocount = ocount;
        this.rzcount = rzcount;
    }



    public long getScount() {
        return scount;
    }

    public void setScount(long scount) {
        this.scount = scount;
    }

    public long getCcount() {
        return ccount;
    }

    public void setCcount(long ccount) {
        this.ccount = ccount;
    }

    public long getOcount() {
        return ocount;
    }

    public void setOcount(long ocount) {
        this.ocount = ocount;
    }

    public long getRzcount() {
        return rzcount;
    }

    public void setRzcount(long rzcount) {
        this.rzcount = rzcount;
    }
    
    

}
