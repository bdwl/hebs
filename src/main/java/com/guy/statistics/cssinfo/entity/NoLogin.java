package com.guy.statistics.cssinfo.entity;

import java.util.List;

public class NoLogin {
    private String name;
    private List<LoginZeroInfo> infos;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LoginZeroInfo> getInfos() {
        return infos;
    }

    public void setInfos(List<LoginZeroInfo> infos) {
        this.infos = infos;
    }

}
