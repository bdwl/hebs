package com.guy.statistics.cssinfo.entity;

import java.io.Serializable;

public class ObjUser implements Serializable {
    /**
         * 
         */
    private static final long serialVersionUID = 1L;

    private long count;
    private String name;

    public ObjUser() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    public ObjUser(String name) {
        super();
        this.name = name;
    }


    public ObjUser(long count, String name) {
        super();
        this.count = count;
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}