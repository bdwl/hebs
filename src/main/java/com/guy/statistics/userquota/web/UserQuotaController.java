package com.guy.statistics.userquota.web;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.guy.common.utils.ServletUtils;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.areainfo.service.AreaInfoService;
import com.guy.util.DateUtil;
import com.guy.util.excel.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.userquota.entity.UserQuota;
import com.guy.statistics.userquota.service.UserQuotaService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 类名称：UserQuotaController
 * 创建人：blank
 * 创建时间：2018-05-08
 */
@Controller
@RequestMapping(value = "statistics/userquota")
public class UserQuotaController extends BaseController {

    @Autowired
    UserQuotaService userquotaService;
    @Autowired
    AreaInfoService areaInfoService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/userquotaList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:userquota:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map
            <String, Object> userquotaList(HttpServletRequest request) {
        Page<UserQuota> page = getPage(request);
        List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);

        Map<String, Object> filterParamMap = ServletUtils.getParametersStartingWith(request,   "filter_");
        if(filterParamMap.isEmpty()){
            int season = DateUtil.getSeason(new Date());
            String seasonStr="";
            switch (season) {
                case 1:
                    seasonStr="第一季度";
                    break;
                case 2:
                    seasonStr="第二季度";
                    break;
                case 3:
                    seasonStr="第三季度";
                    break;
                default:
                    seasonStr="第四季度";
                    break;

            }
            PropertyFilter filter=  new PropertyFilter("EQS_version",seasonStr);
            filters.add(filter);
        }

        page = userquotaService.search(page, filters);
        return getEasyUIData(page);
    }

    /**
     * 添加跳转
     *
     * @param model
     */
    @RequiresPermissions("statistics:userquota:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("userquota", new UserQuota());
        model.addAttribute("action", "create");
        return "statistics/userquotaForm";
    }

    /**
     * 添加字典
     *
     * @param userquota
     * @param model
     */
    @RequiresPermissions("statistics:userquota:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid UserQuota userquota, Model model) {
        userquotaService.save(userquota);
        return "success";
    }

    /**
     * 修改跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:userquota:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("userquota", userquotaService.get(id));
        model.addAttribute("action", "update");
        return "statistics/userquotaForm";
    }

    /**
     * 修改
     *
     * @param userquota
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:userquota:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody UserQuota userquota, Model model) {
        userquotaService.update(userquota);
        return "success";
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:userquota:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        userquotaService.delete(id);
        return "success";
    }

    @RequiresPermissions("statistics:userquota:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importExcel(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        InputStream file = sj.getInputStream();
        try {
            String batchrq = request.getParameter("batchrq");
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);
            file.close();
            for (List<Object> entity : list) {
                UserQuota userQuota = new UserQuota();
                String city=entity.get(0).toString();
                AreaInfo areaInfo= areaInfoService.getInfoByName(city);
                userQuota.setAreaInfo(areaInfo);
                userQuota.setNum(Integer.parseInt(entity.get(1).toString()));
                userQuota.setVersion(batchrq);
                userquotaService.save(userQuota);
            }
            result="success";
        }catch (Exception e){
            result="导入数据异常";
        }
        return result;
    }

    @ModelAttribute
    public void getUserQuota(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("userquota", userquotaService.get(id));
        }
    }

}
