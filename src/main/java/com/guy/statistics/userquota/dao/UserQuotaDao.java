package com.guy.statistics.userquota.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.userquota.entity.UserQuota;

/**
* 类名称：UserQuotaDao
* 创建人：blank
* 创建时间：2018-05-08
*/
@Repository
public class UserQuotaDao extends HibernateDao<UserQuota, Integer>{

}
