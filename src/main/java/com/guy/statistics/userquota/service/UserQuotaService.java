package com.guy.statistics.userquota.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.userquota.dao.UserQuotaDao;
import com.guy.statistics.userquota.entity.UserQuota;

/**
* 类名称：UserQuotaService
* 创建人：blank
* 创建时间：2018-05-08
*/
@Service
@Transactional(readOnly=true)
public class UserQuotaService extends BaseService<UserQuota, Integer> {

@Autowired
private UserQuotaDao userquotaDao;

@Override
public HibernateDao<UserQuota, Integer> getEntityDao() {
return userquotaDao;
}
}
