package com.guy.statistics.attachment.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.guy.statistics.cssuser.entity.CSSUser;
import com.guy.system.user.entity.User;

/** 
 * 类名称：AttachmentEntity
 * 创建人：blank 
 * 创建时间：2016-07-05
 */
@Entity
@Table(name = "t_css_attachment")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate @DynamicInsert
public class Attachment implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String path;
	private int flag;//0待审核，1审核通过
	private CSSUser cssUser;
	private Timestamp eltdate;
	private User user;

	// Constructors

	/** default constructor */
	public Attachment() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "PATH", length = 1000)
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Column(name = "FLAG", nullable = false, length = 2,columnDefinition="int default 0")
    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CSS_USER_ID")
    public CSSUser getCssUser() {
        return cssUser;
    }

    public void setCssUser(CSSUser cssUser) {
        this.cssUser = cssUser;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "ETLDATE", nullable = false, length = 19,columnDefinition="datetime default CURRENT_TIMESTAMP")
    public Timestamp getEltdate() {
        return eltdate;
    }

    public void setEltdate(Timestamp eltdate) {
        this.eltdate = eltdate;
    }
    
    
	
	
}