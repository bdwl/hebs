package com.guy.statistics.attachment.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.attachment.dao.AttachmentDao;
import com.guy.statistics.attachment.entity.Attachment;
import com.guy.statistics.cssuser.entity.CSSUser;

/** 
 * 类名称：AttachmentService
 * 创建人：blank 
 * 创建时间：2016-07-05
 */
@Service
@Transactional(readOnly=true)
public class AttachmentService extends BaseService<Attachment, Integer> {
	
	@Autowired
	private AttachmentDao attachmentDao;

	@Override
	public HibernateDao<Attachment, Integer> getEntityDao() {
		return attachmentDao;
	}
	
	public List<Attachment> findByCssUser(CSSUser cssUser){
	    List<Attachment> attachments= new ArrayList<Attachment>();
	    attachments=attachmentDao.findBy("cssUser", cssUser);
	    return attachments;
	}
}
