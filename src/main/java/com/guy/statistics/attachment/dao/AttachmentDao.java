package com.guy.statistics.attachment.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.attachment.entity.Attachment;

/** 
 * 类名称：AttachmentDao
 * 创建人：blank 
 * 创建时间：2016-07-05
 */
@Repository
public class AttachmentDao extends HibernateDao<Attachment, Integer>{

}
