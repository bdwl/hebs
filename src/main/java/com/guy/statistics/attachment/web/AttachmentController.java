package com.guy.statistics.attachment.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.utils.DateUtils;
import com.guy.common.web.BaseController;
import com.guy.statistics.attachment.entity.Attachment;
import com.guy.statistics.attachment.service.AttachmentService;
import com.guy.statistics.cssuser.entity.CSSUser;
import com.guy.statistics.cssuser.service.CSSUserService;
import com.guy.statistics.opinfo.entity.Opinfo;
import com.guy.statistics.opinfo.service.OpinfoService;
import com.guy.system.utils.UserUtil;

/**
 * 类名称：AttachmentController 创建人：blank 创建时间：2016-07-05
 */
@Controller
@RequestMapping(value = "statistics/attachment")
public class AttachmentController extends BaseController {

    @Autowired
    AttachmentService attachmentService;
    @Autowired
    private CSSUserService cssUserService;
    @Autowired
    private OpinfoService opinfoService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/attachmentList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:attachment:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> attachmentList(HttpServletRequest request) {
        Page<Attachment> page = getPage(request);
        page.setOrder(Page.DESC);
        page.setOrderBy("eltdate");
        List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);

        if (!UserUtil.getCurrentUser().getLoginName().equals("admin")) {
            PropertyFilter filter = new PropertyFilter("EQS_cssUser.areaInfo.areaCode", UserUtil.getCurrentUser().getAreaCode());
            filters.add(filter);
        }
        page = attachmentService.search(page, filters);
        return getEasyUIData(page);
    }

    /**
     * 添加跳转
     * 
     * @param model
     */
    @RequiresPermissions("statistics:attachment:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("attachment", new Attachment());
        model.addAttribute("action", "create");
        return "statistics/attachmentForm";
    }

    /**
     * 添加字典
     * 
     * @param attachment
     * @param model
     */
    @RequiresPermissions("statistics:attachment:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid Attachment attachment, Model model) {
        attachmentService.save(attachment);
        return "success";
    }

    /**
     * 修改跳转
     * 
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:attachment:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("attachment", attachmentService.get(id));
        model.addAttribute("action", "update");
        return "statistics/attachmentForm";
    }

    /**
     * 修改
     * 
     * @param attachment
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:attachment:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody Attachment attachment, Model model) {
        attachmentService.update(attachment);
        return "success";
    }

    /**
     * 删除
     * 
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:attachment:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        Attachment attachment = this.attachmentService.get(id);
        CSSUser cssUser = attachment.getCssUser();
        // 添加操作记录
        Opinfo opinfo = new Opinfo();
        String opdetail = "[删除附件]重新上传";
        opinfo.setDetail(opdetail);
        opinfo.setStatus("UP");
        opinfo.setCssUser(cssUser);
        opinfoService.save(opinfo);
        cssUser.setDelFlag("UP");
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        cssUser.setDetail(opdetail);
        cssUserService.update(cssUser);
        attachmentService.delete(id);
        return "success";
    }

    @RequestMapping(value = "show/{id}")
    @ResponseBody
    public String loadImg(@PathVariable("id") Integer id, HttpServletRequest request) {
        String result = "";
        Attachment attachment = attachmentService.get(id);
        String basePath = request.getContextPath();
        if ("".equals(basePath)) {
            result = "<img src='" + attachment.getPath() + "'/>";
        } else {
            result = "<img src='" + basePath + "/" + attachment.getPath() + "'/>";
        }
        return result;
    }

    @ModelAttribute
    public void getAttachment(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("attachment", attachmentService.get(id));
        }
    }

}
