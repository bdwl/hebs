package com.guy.statistics.station.web;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.station.entity.Station;
import com.guy.statistics.station.service.StationService;
import com.guy.util.excel.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类名称：StationController
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Controller
@RequestMapping(value = "statistics/station")
public class StationController extends BaseController {

    @Autowired
    StationService stationService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/stationList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:station:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map
            <String, Object> stationList(HttpServletRequest request) {
        Page<Station> page = getPage(request);
        List
                <PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
        page = stationService.search(page, filters);
        return getEasyUIData(page);
    }

    /**
     * 添加跳转
     *
     * @param model
     */
    @RequiresPermissions("statistics:station:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("station", new Station());
        model.addAttribute("action", "create");
        return "statistics/stationForm";
    }

    /**
     * 添加字典
     *
     * @param station
     * @param model
     */
    @RequiresPermissions("statistics:station:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid Station station, Model model) {
        stationService.save(station);
        return "success";
    }

    /**
     * 修改跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:station:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("station", stationService.get(id));
        model.addAttribute("action", "update");
        return "statistics/stationForm";
    }

    /**
     * 修改
     *
     * @param station
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:station:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody Station station, Model model) {
        stationService.update(station);
        return "success";
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:station:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        stationService.delete(id);
        return "success";
    }

    @RequiresPermissions("statistics:station:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importExcel(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        InputStream file = sj.getInputStream();

        try {
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);
            file.close();
            List<String> codes = new ArrayList<String>();
            for (List<Object> entity : list) {
                codes.add(entity.get(0).toString());
            }
            if (!codes.isEmpty()) {
                for (List<Object> entity : list) {
                    Station station = stationService.findStationByCode(entity.get(0).toString().trim());
                    if (station == null) {
                        station = new Station();
                        station.setScode(entity.get(0).toString().trim());
                        station.setSname(entity.get(1).toString().trim());
                        stationService.save(station);
                    } else {
                        station.setSname(entity.get(1).toString().trim());
                        stationService.update(station);
                    }
                }
            }
            result = "success";
        } catch (Exception e) {
            result = "数据异常！";
        }
        return result;
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:station:view")
    @RequestMapping(value = "jsonAll", method = RequestMethod.POST)
    @ResponseBody
    public List<Station> stationAllList(HttpServletRequest request) {
        return stationService.getAll();
    }

    @ModelAttribute
    public void getStation(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("station", stationService.get(id));
        }
    }

}
