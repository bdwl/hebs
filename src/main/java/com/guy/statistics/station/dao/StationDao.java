package com.guy.statistics.station.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.station.entity.Station;

/**
 * 类名称：StationDao
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Repository
public class StationDao extends HibernateDao<Station, Integer> {

}
