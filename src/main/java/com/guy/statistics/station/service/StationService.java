package com.guy.statistics.station.service;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.station.dao.StationDao;
import com.guy.statistics.station.entity.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 类名称：StationService
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Service
@Transactional(readOnly = true)
public class StationService extends BaseService<Station, Integer> {

    @Autowired
    private StationDao stationDao;

    @Override
    public HibernateDao<Station, Integer> getEntityDao() {
        return stationDao;
    }

    /**
     * 通过id获取
     *
     * @param code
     * @return
     */
    public Station findStationByCode(String code) {
        return stationDao.findUnique("from Station where scode=?0", code);
    }
    public Station findStationByName(String name) {
        return stationDao.findUnique("from Station where sname=?0", name);
    }
}
