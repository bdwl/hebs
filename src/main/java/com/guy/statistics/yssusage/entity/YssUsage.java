package com.guy.statistics.yssusage.entity;

import javax.persistence.Column;


public class YssUsage implements java.io.Serializable {
    private Integer id;
    /**
     * 地区
     */
    private String areaName;
    /**
     * 授权小于30天的用户
     */
    private int lt30UserNum;
    /**
     * 0登陆的用户
     */
    private int loginZeroNum;
    /**
     * 用户总数
     */
    private int userNum;
    /**
     * 访问总量
     */
    private int amountTotal;

    /**
     * 低频用户
     */
    private int lowNum;

    /**
     * 配额数
     */
    private int quotaNum;
// Constructors

    /**
     * default constructor
     */
    public YssUsage() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
    @Column(nullable = true)
    public int getLt30UserNum() {
        return lt30UserNum;
    }

    public void setLt30UserNum(int lt30UserNum) {
        this.lt30UserNum = lt30UserNum;
    }

    @Column(nullable = true)
    public int getLoginZeroNum() {
        return loginZeroNum;
    }

    public void setLoginZeroNum(int loginZeroNum) {
        this.loginZeroNum = loginZeroNum;
    }

    @Column(nullable = true)
    public int getUserNum() {
        return userNum;
    }

    public void setUserNum(int userNum) {
        this.userNum = userNum;
    }

    @Column(nullable = true)
    public int getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(int amountTotal) {
        this.amountTotal = amountTotal;
    }

    @Column(nullable = true)
    public int getLowNum() {
        return lowNum;
    }

    public void setLowNum(int lowNum) {
        this.lowNum = lowNum;
    }

    public int getQuotaNum() {
        return quotaNum;
    }

    public void setQuotaNum(int quotaNum) {
        this.quotaNum = quotaNum;
    }
}