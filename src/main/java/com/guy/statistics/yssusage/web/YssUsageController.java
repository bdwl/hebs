package com.guy.statistics.yssusage.web;

import java.math.BigDecimal;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.yssusage.entity.YssUsage;
import com.guy.statistics.yssusage.service.YssUsageService;

/**
 * 类名称：YssUsageController
 * 创建人：blank
 * 创建时间：2018-05-07
 */
@Controller
@RequestMapping(value = "statistics/yssusage")
public class YssUsageController extends BaseController {

    @Autowired
    YssUsageService yssusageService;
    String batchrq="2018-01-01";
    String authDate="2018-01-01";
    String quotaVersion="第一季度";

    @RequestMapping(value = "setParam", method = RequestMethod.POST)
    @ResponseBody
    public String setParam(HttpServletRequest request) {
        String batch = (String) request.getParameter("batchrq");
        String auth = (String) request.getParameter("authDate");
        String quota = (String) request.getParameter("quotaVersion");
        request.getSession().setAttribute("batchrq",batch);
        request.getSession().setAttribute("authDate",auth);
        request.getSession().setAttribute("quotaVersion",quota);
        batchrq=batch;
        authDate=auth;
        quotaVersion =quota;
        return "success";
    }
    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/yssusageList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:yssusage:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> yssusageList(HttpServletRequest request) {
        List<YssUsage> yssUsageList = yssusageService.getUsage(batchrq, authDate, quotaVersion);
        Map<String, Object> map = new HashMap<String, Object>();
        if(yssUsageList.size()>0){
            //获取上报率的最大最小值
            double maxReportRate;
            double minReportRate;
            List<Double> reportRate = new ArrayList<>(0);
            for (YssUsage yssUsage : yssUsageList) {
                double result = (double) yssUsage.getUserNum() / (double) yssUsage.getQuotaNum();
                double f1 = getF1(result);
                reportRate.add(f1);
            }
            maxReportRate = Collections.max(reportRate);
            minReportRate = Collections.min(reportRate);
            //获取人均使用次数的最大最小值
            double maxUsageCount;
            double minUsageCount;
            List<Integer> usageCount = new ArrayList<>(0);
            for (YssUsage yssUsage : yssUsageList) {
                double count=(double)yssUsage.getAmountTotal()/((double)yssUsage.getUserNum()-(double)yssUsage.getLt30UserNum()-(double)yssUsage.getLoginZeroNum());
                int c=(int)Math.rint(count);
                usageCount.add(c);
            }
            maxUsageCount = Collections.max(usageCount);
            minUsageCount = Collections.min(usageCount);

            //获取0登陆用户占比的最大最小值
            double maxZeroPer;
            double minZeroPer;
            List<Double> zeroPer = new ArrayList<>(0);
            for (YssUsage yssUsage : yssUsageList) {
                double zero=(double)yssUsage.getLoginZeroNum()/((double)yssUsage.getUserNum()-(double)yssUsage.getLt30UserNum());
                double f1 = getF1(zero);
                zeroPer.add(f1);
            }
            maxZeroPer = Collections.max(zeroPer);
            minZeroPer = Collections.min(zeroPer);


            //低频用户占比的最大最小值
            double maxLowUser;
            double minLowUser;
            List<Double> lowUser = new ArrayList<>(0);
            for (YssUsage yssUsage : yssUsageList) {
                double low=(double)yssUsage.getLowNum()/((double)yssUsage.getUserNum()-(double)yssUsage.getLt30UserNum());
                double f1 = getF1(low);
                lowUser.add(f1);
            }
            maxLowUser = Collections.max(lowUser);
            minLowUser = Collections.min(lowUser);


            map.put("maxRate", maxReportRate);
            map.put("minRate", minReportRate);
            map.put("maxCount", maxUsageCount);
            map.put("minCount", minUsageCount);
            map.put("maxZeroPer", maxZeroPer);
            map.put("minZeroPer", minZeroPer);
            map.put("maxLowUser", maxLowUser);
            map.put("minLowUser", minLowUser);
        }
        map.put("rows", yssUsageList);
        map.put("total", yssUsageList.size());


        return map;
    }

    public double getF1(double result) {
        BigDecimal bg = new BigDecimal(result);
        return bg.setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 添加跳转
     *
     * @param model
     */
    @RequiresPermissions("statistics:yssusage:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("yssusage", new YssUsage());
        model.addAttribute("action", "create");
        return "statistics/yssusageForm";
    }

    /**
     * 添加字典
     *
     * @param yssusage
     * @param model
     */
    @RequiresPermissions("statistics:yssusage:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid YssUsage yssusage, Model model) {
        yssusageService.save(yssusage);
        return "success";
    }

    /**
     * 修改跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:yssusage:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("yssusage", yssusageService.get(id));
        model.addAttribute("action", "update");
        return "statistics/yssusageForm";
    }

    /**
     * 修改
     *
     * @param yssusage
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:yssusage:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody YssUsage yssusage, Model model) {
        yssusageService.update(yssusage);
        return "success";
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:yssusage:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        yssusageService.delete(id);
        return "success";
    }

    @ModelAttribute
    public void getYssUsage(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("yssusage", yssusageService.get(id));
        }
    }

}
