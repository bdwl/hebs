package com.guy.statistics.yssusage.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.yssusage.entity.YssUsage;

/**
* 类名称：YssUsageDao
* 创建人：blank
* 创建时间：2018-05-07
*/
@Repository
public class YssUsageDao extends HibernateDao<YssUsage, Integer>{

}
