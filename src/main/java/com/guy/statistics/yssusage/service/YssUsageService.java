package com.guy.statistics.yssusage.service;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.cssinfo.dao.CSSInfoDao;
import com.guy.statistics.yssusage.dao.YssUsageDao;
import com.guy.statistics.yssusage.entity.YssUsage;
import com.guy.util.DateUtil;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 类名称：YssUsageService
 * 创建人：blank
 * 创建时间：2018-05-07
 */
@Service
@Transactional(readOnly = true)
public class YssUsageService extends BaseService<YssUsage, Integer> {

    @Autowired
    private YssUsageDao yssusageDao;
    @Autowired
    private CSSInfoDao cssInfoDao;

    @Override
    public HibernateDao<YssUsage, Integer> getEntityDao() {
        return yssusageDao;
    }

    /**
     * 根据批次统计
     *
     * @param batchrq     批次
     * @param authDate 授权日期
     * @return
     */
    public List<YssUsage> getUsage(String batchrq, String authDate,String quotaVersion) {
        List<YssUsage> yssUsageListFinal = new ArrayList<>(0);
        Date authTime = DateUtil.parse(authDate, "yyyy-MM-dd");
        List<YssUsage> yssUsageList = getYssUsages(batchrq,quotaVersion);
        List<YssUsage> lt30yssUsageList = getLt30YssUsages(authTime,batchrq);
        List<YssUsage> zeroyssUsageList = getZeroYssUsages(authTime,batchrq);
        List<YssUsage> lowyssUsageList = getLowYssUsages(authTime,batchrq);
        //拼最终用户
        for (YssUsage yssUsage : yssUsageList) {
            YssUsage yssUsageFinal = new YssUsage();
            yssUsageFinal.setAreaName(yssUsage.getAreaName());
            yssUsageFinal.setAmountTotal(yssUsage.getAmountTotal());
            yssUsageFinal.setUserNum(yssUsage.getUserNum());
            yssUsageFinal.setQuotaNum(yssUsage.getQuotaNum());
            for (YssUsage lt30yssUsage : lt30yssUsageList) {
                if (yssUsage.getId().equals(lt30yssUsage.getId())) {
                    yssUsageFinal.setAmountTotal(yssUsage.getAmountTotal() - lt30yssUsage.getAmountTotal());
                    yssUsageFinal.setLt30UserNum(lt30yssUsage.getUserNum());
                }
            }
            for (YssUsage zeroyssUsage : zeroyssUsageList) {
                if (yssUsage.getId().equals(zeroyssUsage.getId())) {
                    yssUsageFinal.setLoginZeroNum(zeroyssUsage.getUserNum());
                }
            }
            for (YssUsage lowysUsage : lowyssUsageList) {
                if (yssUsage.getId().equals(lowysUsage.getId())) {
                    yssUsageFinal.setLowNum(lowysUsage.getUserNum());
                }
            }
            yssUsageListFinal.add(yssUsageFinal);
        }
        System.out.println(yssUsageListFinal.size());
        return yssUsageListFinal;
    }

    public List<YssUsage> getLt30YssUsages(Date authTime, String batchrq) {
        String sqlStr;
        SQLQuery sqlQuery; /**
         * 获取小于三十天的用户访问量及人数
         */
        sqlStr = "SELECT  t2.id , " +
                "sum(t1.yss_click_num) + sum(t1.yss_search_num)  + sum(t1.yss_oplog_num) amountTotal,t2.area_name areaName,  count(*) userNum " +
                "FROM " +
                "t_css_info t1,sys_area_info t2, t_css_user t3 " +
                "WHERE t1.css_user_id = t3.id AND t2.ID = t3.area_id and  t1.auth_time >?0 and t1.batchrq=?1  GROUP BY t3.area_id";

        sqlQuery = cssInfoDao.createSQLQuery(sqlStr, authTime,batchrq);
        sqlQuery.addScalar("id", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("amountTotal", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("areaName", StandardBasicTypes.STRING);
        sqlQuery.addScalar("userNum", StandardBasicTypes.INTEGER);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(YssUsage.class));
        //小于30天授权的用户
        return (List<YssUsage>) sqlQuery.list();
    }

    public List<YssUsage> getYssUsages(String batchrq, String quotaVersion) {
        /**
         * 获取总访问量及人数
         */
        String sqlStr = "SELECT  t2.id ,t4.num quotaNum, " +
                "sum(t1.yss_click_num) + sum(t1.yss_search_num)  + sum(t1.yss_oplog_num) amountTotal,t2.area_name areaName,  count(*) userNum " +
                "FROM " +
                "t_css_info t1,sys_area_info t2, t_css_user t3,t_css_userquota t4 " +
                "WHERE t1.css_user_id = t3.id AND t2.ID = t3.area_id and t4.area_id=t3.area_id and t1.batchrq=?0 and t4.version=?1  GROUP BY t3.area_id order by amountTotal desc";
        SQLQuery sqlQuery = cssInfoDao.createSQLQuery(sqlStr,batchrq,quotaVersion);
        sqlQuery.addScalar("id", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("amountTotal", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("areaName", StandardBasicTypes.STRING);
        sqlQuery.addScalar("userNum", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("quotaNum", StandardBasicTypes.INTEGER);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(YssUsage.class));
        return (List<YssUsage>) sqlQuery.list();
    }

    public List<YssUsage> getZeroYssUsages(Date authTime, String batchrq) {
        String sqlStr;
        SQLQuery sqlQuery; /**
         * 零登陆
         */
        sqlStr = "SELECT  t2.id , " +
                "sum(t1.yss_click_num) + sum(t1.yss_search_num)  + sum(t1.yss_oplog_num) amountTotal,t2.area_name areaName,  count(*) userNum " +
                "FROM " +
                "t_css_info t1,sys_area_info t2, t_css_user t3 " +
                "WHERE t1.css_user_id = t3.id AND t2.ID = t3.area_id and  t1.auth_time <?0  and t1.yss_login_num=0  and t1.batchrq=?1  GROUP BY t3.area_id";
        sqlQuery = cssInfoDao.createSQLQuery(sqlStr, authTime,batchrq);
        sqlQuery.addScalar("id", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("amountTotal", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("areaName", StandardBasicTypes.STRING);
        sqlQuery.addScalar("userNum", StandardBasicTypes.INTEGER);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(YssUsage.class));
        //零登陆用户集合
        return (List<YssUsage>) sqlQuery.list();
    }

    public List<YssUsage> getLowYssUsages(Date authTime, String batchrq) {
        String sqlStr;
        SQLQuery sqlQuery; /**
         * 低频用户
         */
        sqlStr = "SELECT  t2.id , " +
                " t2.area_name areaName,  count(*) userNum " +
                "FROM " +
                "t_css_info t1,sys_area_info t2, t_css_user t3 " +
                "WHERE t1.css_user_id = t3.id AND t2.ID = t3.area_id and  t1.auth_time <?0 " +
                " and t1.yss_total<100 and t1.batchrq=?1  GROUP BY t3.area_id";
        sqlQuery = cssInfoDao.createSQLQuery(sqlStr,authTime,batchrq);
        sqlQuery.addScalar("id", StandardBasicTypes.INTEGER);
        sqlQuery.addScalar("areaName", StandardBasicTypes.STRING);
        sqlQuery.addScalar("userNum", StandardBasicTypes.INTEGER);
        sqlQuery.setResultTransformer(Transformers.aliasToBean(YssUsage.class));
        //低频用户集合
        return (List<YssUsage>) sqlQuery.list();
    }
}
