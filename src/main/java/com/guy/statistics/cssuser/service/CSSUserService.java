package com.guy.statistics.cssuser.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.common.utils.DateUtils;
import com.guy.statistics.cssuser.dao.CSSUserDao;
import com.guy.statistics.cssuser.entity.CSSUser;
import com.guy.statistics.opinfo.dao.OpinfoDao;
import com.guy.statistics.opinfo.entity.Opinfo;
import com.guy.system.areainfo.entity.AreaInfo;

@Service
@Transactional(readOnly=true)
public class CSSUserService extends BaseService<CSSUser, Integer>{

	@Autowired
	private CSSUserDao cssUserDao;
	@Autowired
	private OpinfoDao opinfoDao;
	
	
	@Override
	public HibernateDao<CSSUser, Integer> getEntityDao() {
		return cssUserDao;
	}
	@Override
	@Transactional(readOnly = false)
	public void save(CSSUser entity) {
		// TODO Auto-generated method stub
		Timestamp timestamp=DateUtils.getSysTimestamp();
		if (entity.getCreatedate()==null) {
		    entity.setCreatedate(timestamp);
        }
		entity.setOpdate(timestamp);
		entity.setDelFlag("UP");
		super.save(entity);
	}
	/**
	 * 查询是否已经存在用户信息
	 * @param sfzh
	 * @return
	 */
	public CSSUser findBySfz(String sfzh) {
		List<CSSUser> cssUsers=cssUserDao.find("from CSSUser where sfzh=?0", sfzh);
		if(!cssUsers.isEmpty()){
			return cssUsers.get(0);
		}else{
			return null;
		}
		
	}
	
	/**
	 * 批量删除云搜人员
	 * @param idList
	 */
	@Transactional(readOnly=false)
	public void deleteCssUser(List<Integer> idList){
		cssUserDao.deleteBatch(idList);
	}
	
	/**
	 * 按身份证号码获取对象
	 * @param sfzh
	 * @return
	 */
	public CSSUser getUser(String sfzh) {
		CSSUser cssUser=cssUserDao.findUniqueBy("sfzh", sfzh);
		return cssUser;
	}
	
	@Transactional(readOnly=false)
	public List<CSSUser> updateState(Date now){
	    String hql="from CSSUser where expirydate<?0 and delFlag='N'";
	    List<CSSUser> cssUsers=null;
	    cssUsers=cssUserDao.find(hql, now);
	    for (CSSUser cssUser : cssUsers) {
//	        cssUser.set
            cssUser.setDelFlag("ND");
            cssUser.setDetail("【定时任务】有效期到期！");
            cssUser.setOpdate(DateUtils.getSysTimestamp());
           
            
            Opinfo opinfo= new Opinfo();
            opinfo.setCssUser(cssUser);
            opinfo.setCreateTime(DateUtils.getSysTimestamp());
            opinfo.setStatus(cssUser.getDelFlag());
            opinfo.setDetail("【定时任务】有效期到期！");
            
            cssUserDao.save(cssUser);
            cssUserDao.getSession().flush();
            opinfoDao.save(opinfo);
            //添加详情
        }
	    
	    return cssUsers;
	}
	
    /**
     * 导出所有正常人员
     * @return
     */
    public List<CSSUser> getNomalUsers() {
        List<CSSUser> cssUsers= cssUserDao.findBy("delFlag", "N");
        return cssUsers;
        
    }
    
    /**
     * 导出所属地区正常人员
     * @return
     */
    public List<CSSUser> getAreaUsers(AreaInfo areaInfo) {
        List<CSSUser> cssUsers= cssUserDao.find("from CSSUser where areaInfo=?0 and delFlag='N'", areaInfo);
        return cssUsers;
    }
}
