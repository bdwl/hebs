package com.guy.statistics.cssuser.web;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.utils.DateUtils;
import com.guy.common.utils.IdGen;
import com.guy.common.utils.IdcardValidator;
import com.guy.common.web.BaseController;
import com.guy.statistics.attachment.entity.Attachment;
import com.guy.statistics.attachment.service.AttachmentService;
import com.guy.statistics.cssinfo.entity.CSSInfo;
import com.guy.statistics.cssinfo.service.CSSInfoService;
import com.guy.statistics.cssuser.entity.CSSUser;
import com.guy.statistics.cssuser.service.CSSUserService;
import com.guy.statistics.opinfo.entity.Opinfo;
import com.guy.statistics.opinfo.service.OpinfoService;
import com.guy.statistics.policeclassification.entity.PoliceClassification;
import com.guy.statistics.policeclassification.service.PoliceClassificationService;
import com.guy.statistics.station.entity.Station;
import com.guy.statistics.station.service.StationService;
import com.guy.system.areainfo.entity.AreaInfo;
import com.guy.system.areainfo.service.AreaInfoService;
import com.guy.system.user.entity.User;
import com.guy.system.utils.UserUtil;
import com.guy.util.DateUtil;
import com.guy.util.FileUtil;
import com.guy.util.excel.ExcelUtil;
import com.guy.util.excelTools.ExcelUtils;
import com.guy.util.excelTools.JsGridReportBase;
import com.guy.util.excelTools.TableData;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("statistics/cssUser")
public class CSSUserController extends BaseController {

    private static Logger log = Logger.getLogger(CSSUserController.class);
    @Autowired
    private CSSUserService cssUserService;
    @Autowired
    private CSSInfoService cssInfoService;
    @Autowired
    private PoliceClassificationService policeClassificationService;
    @Autowired
    private StationService stationService;
    @Autowired
    private AreaInfoService areaInfoService;
    @Autowired
    private OpinfoService opinfoService;
    @Autowired
    private AttachmentService attachmentService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/cssuserList";
    }

    /**
     * 获取云搜人员信息类型json
     */
    @RequiresPermissions("statistics:cssuser:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getData(HttpServletRequest request) {
        Page<CSSUser> page = getPage(request);
        page.setOrder(Page.DESC);
        page.setOrderBy("opdate");
        List<PropertyFilter> filters = null;
        try {
            filters = PropertyFilter.buildFromHttpRequest(request);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            if (!UserUtil.getCurrentUser().getLoginName().equals("admin")) {
                PropertyFilter filter1 = new PropertyFilter("NES_delFlag", "D");
                PropertyFilter filter2 = new PropertyFilter("EQS_areaInfo.areaCode", UserUtil.getCurrentUser().getAreaCode());

                filters.add(filter1);
                filters.add(filter2);
            }
            page = cssUserService.search(page, filters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getEasyUIData(page);
    }

    /**
     * 添加云搜人员信息跳转
     *
     * @param model
     */
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("CSSUser", new CSSUser());
        model.addAttribute("action", "create");
        return "statistics/cssuserForm";
    }

    /**
     * 添加云搜人员信息
     *
     * @param model
     */
    @RequiresPermissions("statistics:cssuser:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid CSSUser cssUser, Model model) {
        cssUserService.save(cssUser);
        Opinfo opinfo = new Opinfo();
        opinfo.setCssUser(cssUser);
        opinfo.setStatus(cssUser.getDelFlag());
        opinfo.setDetail(cssUser.getDetail());
        opinfoService.save(opinfo);
        return "success";
    }

    /**
     * 修改云搜人员信息跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "update");
        return "statistics/cssuserForm";
    }


    /**
     * 查看云搜人员信息跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "view/{id}", method = RequestMethod.GET)
    public String viewForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "view");
        return "statistics/cssuserView";
    }

    @RequestMapping(value = "upload/{id}", method = RequestMethod.GET)
    public String uploadForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        return "statistics/upload";
    }

    /**
     * 修改云搜人员信息
     *
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:cssuser:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody CSSUser cssUser, Model model) {
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        try {
            cssUserService.update(cssUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Opinfo opinfo= new Opinfo();
//        opinfo.setCssUser(cssUser);
//        opinfo.setStatus(cssUser.getDelFlag());
//        opinfo.setDetail("【管理员】修改用户信息");
//        opinfoService.save(opinfo);
        return "success";
    }

    /**
     * 标记删除云搜人员信息跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "del/{id}", method = RequestMethod.GET)
    public String delForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "del");
        return "statistics/cssuserForm";
    }

    /**
     * 标记删除云搜人员信息
     *
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:cssuser:del")
    @RequestMapping(value = "del", method = RequestMethod.POST)
    @ResponseBody
    public String del(@Valid @ModelAttribute @RequestBody CSSUser cssUser, Model model) {
        cssUser.setDelFlag("ND");
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        cssUser.setExpirydate(DateUtils.getSysTimestamp());//删除时把有效期设置为当前
        cssUserService.update(cssUser);
        Opinfo opinfo = new Opinfo();
        opinfo.setCssUser(cssUser);
        opinfo.setStatus(cssUser.getDelFlag());
        opinfo.setDetail(cssUser.getDetail());
        opinfoService.save(opinfo);
        return "success";
    }

    @RequestMapping(value = "reset/{id}", method = RequestMethod.GET)
    public String resetForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "reset");
        return "statistics/cssuserForm";
    }

    /**
     * 云搜人员信息重新授权
     *
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:cssuser:reset")
    @RequestMapping(value = "reset", method = RequestMethod.POST)
    @ResponseBody
    public String resetUser(@Valid @ModelAttribute @RequestBody CSSUser cssUser, Model model) {
        cssUser.setDelFlag("F");
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        cssUserService.update(cssUser);
        Opinfo opinfo = new Opinfo();
        opinfo.setCssUser(cssUser);
        opinfo.setStatus(cssUser.getDelFlag());
        opinfo.setDetail(cssUser.getDetail());
        opinfoService.save(opinfo);
        return "success";
    }

    /**
     * 变更云搜人员信息跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "mod/{id}", method = RequestMethod.GET)
    public String modForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "mod");
        return "statistics/cssuserForm";
    }

    /**
     * 变更云搜人员信息
     *
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:cssuser:mod")
    @RequestMapping(value = "mod", method = RequestMethod.POST)
    @ResponseBody
    public String mod(@Valid @ModelAttribute @RequestBody CSSUser cssUser, Model model) {
        cssUser.setDelFlag("M");
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        cssUserService.update(cssUser);
        Opinfo opinfo = new Opinfo();
        opinfo.setCssUser(cssUser);
        opinfo.setStatus(cssUser.getDelFlag());
        opinfo.setDetail(cssUser.getDetail());
        opinfoService.save(opinfo);
        return "success";
    }

    /**
     * 删除云搜人员信息
     *
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:cssuser:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        try {
            CSSUser cssUser = cssUserService.get(id);
            cssUserService.delete(cssUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 批量删除云搜人员信息
     *
     * @param idList
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody List<Integer> idList) {
        cssUserService.deleteCssUser(idList);
        return "success";
    }

    /**
     * 初始化人员信息 （首次导入）
     *
     * @param sj
     * @param request
     * @return
     * @throws Exception select * from t_css_user t1,sys_area_info t2 where t1.organization like '%石家庄%' and t2.AREA_NAME='石家庄';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%唐山%' and t2.AREA_NAME='唐山';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%秦皇岛%' and t2.AREA_NAME='秦皇岛';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%邯郸%' and t2.AREA_NAME='邯郸';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%邢台%' and t2.AREA_NAME='邢台';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%保定%' and t2.AREA_NAME='保定';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%张家口%' and t2.AREA_NAME='张家口';
     *                   vselect * from t_css_user t1,sys_area_info t2 where t1.organization like '%承德%' and t2.AREA_NAME='承德';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%沧州%' and t2.AREA_NAME='沧州';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%廊坊%' and t2.AREA_NAME='廊坊';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%衡水%' and t2.AREA_NAME='衡水';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%冀中%' and t2.AREA_NAME='冀中';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%定州%' and t2.AREA_NAME='定州';
     *                   select * from t_css_user t1,sys_area_info t2 where t1.organization like '%辛集%' and t2.AREA_NAME='辛集';
     *                   <p>
     *                   update t_css_user t1,sys_area_info t2 set t1.area_id=t2.ID where t1.organization like '%石家庄%' and t2.AREA_NAME='石家庄';
     */
    @RequiresPermissions("statistics:cssuser:importInit")
    @RequestMapping(value = "importInit")
    @ResponseBody
    public String importExcelInit(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        InputStream file = sj.getInputStream();
        try {
            // 获取导入数据对象集合
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);
            file.close();
            //验证身份证号码
            IdcardValidator iv = new IdcardValidator();
            List<String> sfzs = new ArrayList<String>();
            for (List<Object> entity : list) {
                boolean bool = iv.isIdcard(entity.get(1).toString());
                if (!bool) {
                    sfzs.add(entity.get(1).toString());
                }
            }
            if (sfzs.isEmpty()) {
                for (List<Object> entity : list) {
                    //查询是否存在用户信息
                    CSSUser cssUser = cssUserService.findBySfz(entity.get(1).toString());
                    //如果不存在则保存
                    if (cssUser == null) {
                        //保存人员信息
                        cssUser = new CSSUser();
                        cssUser.setName(entity.get(0).toString());
                        cssUser.setSfzh(entity.get(1).toString());
                        PoliceClassification policeClassification = policeClassificationService.findPcByName(entity.get(2).toString().trim());
                        cssUser.setPoliceClassification(policeClassification);
                        Station station =stationService.findStationByName(entity.get(3).toString().trim());
                        cssUser.setStation(station);
                        cssUser.setOrganization(entity.get(4).toString());
                        cssUser.setTelphone(entity.get(5).toString());
                        cssUser.setCreatedate(DateUtils.getSysTimestamp());
                        cssUser.setOpdate(DateUtils.getSysTimestamp());
                        cssUser.setAuditor(entity.get(6).toString());
                        cssUser.setAuditunit(entity.get(7).toString());
                        AreaInfo areaInfo = null;
                        if (cssUser.getOrganization().indexOf("石家庄") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("石家庄");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("唐山") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("唐山");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("秦皇岛") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("秦皇岛");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("邯郸") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("邯郸");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("邢台") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("邢台");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("保定") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("保定");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("张家口") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("张家口");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("承德") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("承德");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("沧州") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("沧州");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("廊坊") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("廊坊");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("衡水") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("衡水");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("冀中") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("冀中");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("定州") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("定州");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getOrganization().indexOf("辛集") >= 0) {
                            areaInfo = areaInfoService.getInfoByName("辛集");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        if (cssUser.getAreaInfo() == null) {
                            areaInfo = areaInfoService.getInfoByName("省厅");
                            cssUser.setAreaInfo(areaInfo);
                        }
                        cssUser.setUserLeval(1);
                        cssUser.setDelFlag("UP");
                        cssUserService.save(cssUser);
                        log.info("[批量导入][新增]姓名：" + cssUser.getName() + "，身份证号码：" + cssUser.getSfzh());
                        //存在则修改操作时间信息
                    } else {
                        cssUser.setName(entity.get(0).toString());
                        //更新警种 岗位
                        PoliceClassification policeClassification = policeClassificationService.findPcByName(entity.get(2).toString().trim());
                        cssUser.setPoliceClassification(policeClassification);
                        Station station =stationService.findStationByName(entity.get(3).toString().trim());
                        cssUser.setStation(station);
                        cssUser.setOrganization(entity.get(4).toString());
                        cssUser.setTelphone(entity.get(5).toString());
                        cssUser.setOpdate(DateUtils.getSysTimestamp());
                        cssUser.setAuditor(entity.get(6).toString());
                        cssUser.setAuditunit(entity.get(7).toString());
                        cssUserService.update(cssUser);
                        log.info("[批量导入][更新]姓名：" + cssUser.getName() + "，身份证号码：" + cssUser.getSfzh());
                    }
                    //保存操作记录
                    Opinfo opinfo = new Opinfo();
                    String opdetail = "[批量导入]初始化人员信息";
                    opinfo.setDetail(opdetail);
                    opinfo.setStatus("UP");
                    opinfo.setCssUser(cssUser);
                    opinfoService.save(opinfo);
                    //更新用户操作记录
                    cssUser.setDetail(opdetail);
                    cssUserService.update(cssUser);
                }
                System.out.println(list.size());
                result = "success";
            } else {
                StringBuffer sb = new StringBuffer();
                for (String sfz : sfzs) {
                    sb.append(sfz + " ");
                }
                result = "身份证号码异常：" + sb.toString();
            }
        } catch (Exception e) {
            log.info("初始化导入数据异常：" + e.toString());
            result = "数据异常！";
        }
        return result;
    }

    /**
     * 导入数据
     *
     * @param sj
     * @param request
     * @return
     * @throws Exception
     */
    @RequiresPermissions("statistics:cssuser:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importExcel(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        InputStream file = sj.getInputStream();
        String batchrq = request.getParameter("batchrq");
        try {
            //生成批次
            String batch = IdGen.randomBase62(8);
            //获取导入数据对象集合
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);
            file.close();
            //验证身份证号码
            IdcardValidator iv = new IdcardValidator();
            //有问题的身份证号码
            List<String> sfzs = new ArrayList<String>();
            for (List<Object> entity : list) {
                boolean bool = iv.isIdcard(entity.get(1).toString());
                if (!bool) {
                    sfzs.add(entity.get(1).toString());
                }
            }
            //不存在的身份证号码
            List<String> nsfz = new ArrayList<String>();
            if (sfzs.isEmpty()) {
                boolean bool = false;
                for (List<Object> entity : list) {
                    CSSUser cssUser = cssUserService.findBySfz(entity.get(1).toString());
                    if (cssUser == null) {
                        nsfz.add(entity.get(1).toString());
                        bool = true;
                    }
                }
                if (bool) {
                    StringBuffer nsb = new StringBuffer();
                    for (String ns : nsfz) {
                        nsb.append(ns + " ");
                    }
                    result = "不存在的身份证号码：" + nsb.toString();
                } else {
                    for (List<Object> entity : list) {
                        log.info(entity.get(1).toString());
                        //查询是否存在用户信息
                        CSSUser cssUser = cssUserService.findBySfz(entity.get(1).toString());

                        nsfz.add(entity.get(1).toString());
                        //保存云搜索信息
                        CSSInfo cssInfo = new CSSInfo();
                        cssInfo.setCssUser(cssUser);
                        cssInfo.setBatchrq(batchrq);
                        cssInfo.setYssLoginNum((int) Double.parseDouble(entity.get(3).toString()));
                        cssInfo.setYssSearchNum((int) Double.parseDouble(entity.get(4).toString()));
                        cssInfo.setYssClickNum((int) Double.parseDouble(entity.get(5).toString()));
                        cssInfo.setYssOplogNum((int) Double.parseDouble(entity.get(6).toString()));
                        cssInfo.setYssExportNum((int) Double.parseDouble(entity.get(7).toString()));
                        cssInfo.setYssTotal((int) Double.parseDouble(entity.get(8).toString()));
                        Date authDate = DateUtil.parse(entity.get(9).toString(), DateUtil.FORMAT_FULL);
                        cssInfo.setAuthTime(new Timestamp(authDate.getTime()));
                        cssInfo.setCreateTime(DateUtils.getSysTimestamp());
                        cssInfo.setBatch(batch);
                        cssInfoService.save(cssInfo);
                    }
                    result = "success";
                }

            } else {
                StringBuffer sb = new StringBuffer();
                for (String sfz : sfzs) {
                    sb.append(sfz + " ");
                }

                result = "身份证号码异常：" + sb.toString();
            }
        } catch (Exception e) {
            log.info("统计数据导入数据异常：" + e.toString());
            result = "数据异常！";
        }
        return result;
    }


    /**
     * 弹窗页-云搜信息
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "{cssUserId}/cssinfo")
    public String getcssUserInfo(@PathVariable("cssUserId") Integer id, Model model) {
        model.addAttribute("cssUserId", id);
        return "statistics/cssinfoList";
    }

    @RequiresPermissions("statistics:cssuser:cssInfoView")
    @RequestMapping(value = "cssinfojson/{cssUserId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getCssInfoData(@PathVariable("cssUserId") Integer id, HttpServletRequest request) {
        Page<CSSInfo> page = getPage(request);
        page.setOrderBy("batchrq");
        page.setOrder(Page.DESC);
        List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
        try {
            PropertyFilter filter = new PropertyFilter("EQI_cssUser.id", id);
            filters.add(filter);
            page = cssInfoService.search(page, filters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getEasyUIData(page);
    }


    /**
     * 导出没有删除的用户
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequiresPermissions("statistics:cssuser:exportAreaUser")
    @RequestMapping("exportAreaExcel")
    public void exportAreaExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<CSSUser> userList = new ArrayList<CSSUser>();
        User user = (User) request.getSession().getAttribute("user");
        String areacode = user.getAreaCode();
        AreaInfo areaInfo = areaInfoService.getInfoByCode(areacode);
        userList = cssUserService.getAreaUsers(areaInfo);
        List<TableData> tds = new ArrayList<TableData>();//多sheet
        String title = areaInfo.getAreaName() + "人员信息";
        String[] hearders = new String[]{" 姓名 ", "  身份证号  ", "        单位        "};//表头数组
        String[] fields = new String[]{"name", "sfzh", "organization"};//People对象属性数组
        TableData td = ExcelUtils.createTableData(userList, ExcelUtils.createTableHeader(hearders), fields);
        td.setSheetTitle(areaInfo.getAreaName() + "的人员");
        tds.add(td);
        JsGridReportBase report = new JsGridReportBase(request, response);

        report.exportToExcel(title, user.getName(), tds);
    }

    /**
     * 导出没有删除的用户
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequiresPermissions("statistics:cssuser:export")
    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String startTime = request.getParameter("filter_GED_opdate");
        String endTime = request.getParameter("filter_LED_opdate");
        Date start = null;
        Date end = null;
        if (!"".equals(startTime) && startTime != null) {
            start = DateUtil.parse(startTime);
        }
        if (!"".equals(endTime) && endTime != null) {
            end = DateUtil.parse(endTime);
        }

        List<Opinfo> list = new ArrayList<Opinfo>();
        list = opinfoService.getOpinfosN(start, end);
        List<CSSUser> userList = new ArrayList<CSSUser>();
        for (Opinfo op : list) {
            userList.add(op.getCssUser());
        }
        List<TableData> tds = new ArrayList<TableData>();//多sheet
        String title = "人员信息";
        String[] hearders = new String[]{" 姓名 ", "  身份证号  ", "手机号", "        单位        "};//表头数组
        String[] fields = new String[]{"name", "sfzh", "telphone", "organization"};//People对象属性数组
        TableData td = ExcelUtils.createTableData(userList, ExcelUtils.createTableHeader(hearders), fields);
        td.setSheetTitle("新增");
        tds.add(td);

        list = new ArrayList<Opinfo>();
        list = opinfoService.getOpinfosD(start, end);
        userList = new ArrayList<CSSUser>();
        for (Opinfo op : list) {
            userList.add(op.getCssUser());
        }
        hearders = new String[]{" 姓名 ", "  身份证号  ", "手机号", "        单位        "};//表头数组
        fields = new String[]{"name", "sfzh", "telphone", "organization"};//People对象属性数组
        td = ExcelUtils.createTableData(userList, ExcelUtils.createTableHeader(hearders), fields);
        td.setSheetTitle("删除");
        tds.add(td);

        list = new ArrayList<Opinfo>();
        list = opinfoService.getOpinfosR(start, end);
        userList = new ArrayList<CSSUser>();
        for (Opinfo op : list) {
            userList.add(op.getCssUser());
        }
        hearders = new String[]{" 姓名 ", "  身份证号  ", "手机号", "        单位        "};//表头数组
        fields = new String[]{"name", "sfzh", "telphone", "organization"};//People对象属性数组
        td = ExcelUtils.createTableData(userList, ExcelUtils.createTableHeader(hearders), fields);
        td.setSheetTitle("重新授权");
        tds.add(td);


        JsGridReportBase report = new JsGridReportBase(request, response);
        User user = (User) request.getSession().getAttribute("user");
        report.exportToExcel(title, user.getName(), tds);
    }

    /**
     * 导出正常用户用户
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequiresPermissions("statistics:cssuser:exportuser")
    @RequestMapping("exportExcelUser")
    public void exportExcelUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<CSSUser> userList = new ArrayList<CSSUser>();
        userList = cssUserService.getNomalUsers();
        List<TableData> tds = new ArrayList<TableData>();//多sheet
        String title = "人员信息";
        String[] hearders = new String[]{" 姓名 ", "  身份证号  ", "        单位        "};//表头数组
        String[] fields = new String[]{"name", "sfzh", "organization"};//People对象属性数组
        TableData td = ExcelUtils.createTableData(userList, ExcelUtils.createTableHeader(hearders), fields);
        td.setSheetTitle("所有状态为正常的人员");
        tds.add(td);
        JsGridReportBase report = new JsGridReportBase(request, response);
        User user = (User) request.getSession().getAttribute("user");
        report.exportToExcel(title, user.getName(), tds);
    }


    /**
     * Ajax请求校验sfzh是否唯一。
     */
    @RequestMapping(value = "checkSFZH")
    @ResponseBody
    public String checkSFZH(String sfzh) {
        if (cssUserService.getUser(sfzh) == null) {
            return "true";
        } else {
            return "false";
        }
    }


    /**
     * 标记删除云搜人员信息跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "reject/{id}", method = RequestMethod.GET)
    public String rejectForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("CSSUser", cssUserService.get(id));
        model.addAttribute("action", "reject");
        return "statistics/cssuserForm";
    }
    /**
     * 退回
     */

    @RequiresPermissions("statistics:cssuser:reject")
    @RequestMapping(value = "reject", method = RequestMethod.POST)
    @ResponseBody
    public String rejectCssUser(@Valid @ModelAttribute @RequestBody CSSUser cssUser, Model model) {
        cssUser.setDelFlag("UP");
        cssUser.setOpdate(DateUtils.getSysTimestamp());
        String detail="[退回]"+cssUser.getDetail().replace("[退回]","");
        cssUser.setDetail(detail);
        cssUserService.update(cssUser);
        //添加操作记录
        Opinfo opinfo = new Opinfo();
        opinfo.setDetail(detail);
        opinfo.setStatus("UP");
        opinfo.setCssUser(cssUser);
        opinfoService.save(opinfo);
        return "success";
    }
    @RequiresPermissions("statistics:cssuser:audit")
    @RequestMapping(value = "audit/{id}")
    @ResponseBody
    public String auditCssUser(@PathVariable("id") Integer id) {
        String result = "";
        try {
            CSSUser cssUser = cssUserService.get(id);
            if ("S".equals(cssUser.getDelFlag())) {
                cssUser.setDelFlag("N");
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
                //添加操作记录
                Opinfo opinfo = new Opinfo();
                String opdetail = "[审核]通过提交材料";
                opinfo.setDetail(opdetail);
                opinfo.setStatus("N");
                opinfo.setCssUser(cssUser);
                opinfoService.save(opinfo);
                //更新用户操作记录
                cssUser.setDetail(opdetail);
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
                //修改审核材料状态
                List<Attachment> attachments = attachmentService.findByCssUser(cssUser);
                for (Attachment attachment : attachments) {
                    attachment.setFlag(1);
                    attachmentService.update(attachment);
                }
            } else if ("F".equals(cssUser.getDelFlag())) {
                cssUser.setDelFlag("N");
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
                //添加操作记录
                Opinfo opinfo = new Opinfo();
                String opdetail = "[审核]审核通过重新授权";
                opinfo.setDetail(opdetail);
                opinfo.setStatus("N");
                opinfo.setCssUser(cssUser);
                opinfoService.save(opinfo);
                //更新用户操作记录
                cssUser.setDetail(opdetail);
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
            } else if ("ND".equals(cssUser.getDelFlag())) {
                cssUser.setDelFlag("D");
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
                //添加操作记录
                Opinfo opinfo = new Opinfo();
                String opdetail = "[审核]审核通过删除人员";
                opinfo.setDetail(opdetail);
                opinfo.setStatus("D");
                opinfo.setCssUser(cssUser);
                opinfoService.save(opinfo);
                //更新用户操作记录
                cssUser.setDetail(opdetail);
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                cssUserService.update(cssUser);
            }
            result = "success";
        } catch (Exception e) {
            e.printStackTrace();
            result = "fail";
        }
        return result;
    }


    /**
     * 上传附件
     *
     * @param id
     * @param files
     * @param request
     * @return
     * @throws Exception
     */
    @RequiresPermissions("statistics:cssuser:upatt")
    @RequestMapping(value = "importinfo/{id}")
    @ResponseBody
    public String uploadInfo(@PathVariable("id") Integer id, @RequestParam(value = "sj") CommonsMultipartFile[] files, HttpServletRequest request) throws Exception {
        String result = null;
        try {

            String basePath = request.getSession().getServletContext().getRealPath("/");
            String path = "/upload/" + DateUtil.format(new Date(), "yyyyMMdd") + "/";
            FileUtil.createDir(basePath + path);
            CSSUser cssUser = null;

            for (CommonsMultipartFile file : files) {
                String fileName = DateUtils.getDateRandom();
                FileOutputStream os = new FileOutputStream(basePath + path + "/" + fileName + ".jpg");

                //拿到上传文件的输入流
                FileInputStream in = (FileInputStream) file.getInputStream();
                //拿到输出流，同时重命名上传的文件
                //以写字节的方式写文件
                int b = 0;
                while ((b = in.read()) != -1) {
                    os.write(b);
                }
                os.flush();
                os.close();
                in.close();
                cssUser = cssUserService.get(id);
                cssUser.setDelFlag("S");
                cssUser.setDetail("[上传资料]审核材料");
                cssUser.setOpdate(DateUtils.getSysTimestamp());
                Attachment attachment = new Attachment();
                attachment.setCssUser(cssUser);
                attachment.setPath(path + "/" + fileName + ".jpg");
                attachment.setFlag(0);
                attachment.setUser(UserUtil.getCurrentUser());
                attachmentService.save(attachment);
                cssUserService.update(cssUser);

            }
            Opinfo opinfo = new Opinfo();
            opinfo.setDetail("[上传资料]审核材料");
            opinfo.setStatus("S");
            opinfo.setCssUser(cssUser);
            opinfoService.save(opinfo);
            result = "success";
        } catch (Exception e) {
            log.info(e.toString());
            result = "fail";
        }
        return result;
    }


    /**
     * 所有RequestMapping方法调用前的Model准备方法, 实现Struts2
     * Preparable二次部分绑定的效果,先根据form的id从数据库查出Task对象,再把Form提交的内容绑定到该对象上。
     * 因为仅update()方法的form中有id属性，因此仅在update时实际执行.
     */
    @ModelAttribute
    public void getCssUser(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("CSSUser", cssUserService.get(id));
        }
    }
}
