package com.guy.statistics.cssuser.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.cssuser.entity.CSSUser;

@Repository
public class CSSUserDao extends HibernateDao<CSSUser, Integer>{

	/**
	 * 批量删除云搜人员信息
	 * @param ids 云搜id列表
	 */
	public void deleteBatch(List<Integer> idList){
		String hql="delete from CSSUser cu where cu.id in (:idList)";
		Query query=getSession().createQuery(hql);
		query.setParameterList("idList", idList);
		query.executeUpdate();
	}

    /**
     * 从某一节点获取用户数据
     * @param start
     * @return
     */
    public List<CSSUser> getAllByRq(Date start) {
        String hql="from CSSUser cu where cu.opdate>?0";
        Query query=createQuery(hql,start);
        return  query.list();
    }
	

}
