package com.guy.statistics.cssuser.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.guy.statistics.attachment.entity.Attachment;
import com.guy.statistics.cssinfo.entity.CSSInfo;
import com.guy.statistics.opinfo.entity.Opinfo;
import com.guy.statistics.policeclassification.entity.PoliceClassification;
import com.guy.statistics.station.entity.Station;
import com.guy.system.areainfo.entity.AreaInfo;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author TONY 云搜索人员信息
 */
@Entity
@Table(name = "t_css_user")
@DynamicUpdate
@DynamicInsert
public class CSSUser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;// 姓名
    private String sfzh;// 身份证号码
    private String organization;// 单位
    private Timestamp createdate;// 创建时间
    private Timestamp opdate;// 操作时间
    private Timestamp expirydate;// 有效期
    //警种
    private PoliceClassification policeClassification;
    //岗位
    private Station station;
    // private String unitCode;//行政区划
    private String telphone;//手机号
    private AreaInfo areaInfo;
    private String orgcode;
    private int userLeval;// 用户级别 0普通1高级
    private String delFlag;// 删除标记 N 正常 D删除 M变更 ND待删除 S待审核
    private String detail;// 事由详情
    private boolean abnormal;// 异常
    private String auditor;//审核人
    private String auditunit;//审核单位
    @JsonIgnore
    private Set<CSSInfo> cssInfos = new HashSet<CSSInfo>(0);
    @JsonIgnore
    private Set<Opinfo> opinfos = new HashSet<Opinfo>(0);
    @JsonIgnore
    private Set<Attachment> attachments = new HashSet<Attachment>(0);

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "SFZH", nullable = false, length = 18, unique = true)
    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    @Column(name = "ORGANIZATION", nullable = false, length = 200)
    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "CREATE_DATE", nullable = false, length = 19)
    public Timestamp getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "OP_DATE", nullable = false, length = 19, columnDefinition = "datetime default CURRENT_TIMESTAMP")
    public Timestamp getOpdate() {
        return opdate;
    }

    public void setOpdate(Timestamp opdate) {
        this.opdate = opdate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "EXPIRY_DATE", length = 19)
    public Timestamp getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(Timestamp expirydate) {
        this.expirydate = expirydate;
    }

    @Column(name = "TELPHONE", length = 11)
    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "AREA_ID")
    public AreaInfo getAreaInfo() {
        return areaInfo;
    }

    public void setAreaInfo(AreaInfo areaInfo) {
        this.areaInfo = areaInfo;
    }

    @Column(name = "DELFLAG", length = 2)
    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Column(name = "DETAIL", length = 1000)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Column(name = "USER_LEVAL", columnDefinition = "int default 1")
    public int getUserLeval() {
        return userLeval;
    }

    public void setUserLeval(int userLeval) {
        this.userLeval = userLeval;
    }

    @Column(name = "ABNORMAL", columnDefinition = "bit default 0")
    public boolean isAbnormal() {
        return abnormal;
    }

    public void setAbnormal(boolean abnormal) {
        this.abnormal = abnormal;
    }

    @OneToMany(mappedBy = "cssUser", fetch = FetchType.LAZY)
    public Set<CSSInfo> getCssInfos() {
        return cssInfos;
    }

    public void setCssInfos(Set<CSSInfo> cssInfos) {
        this.cssInfos = cssInfos;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cssUser", fetch = FetchType.LAZY)
    public Set<Opinfo> getOpinfos() {
        return opinfos;
    }

    public void setOpinfos(Set<Opinfo> opinfos) {
        this.opinfos = opinfos;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cssUser", fetch = FetchType.LAZY)
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getAuditunit() {
        return auditunit;
    }

    public void setAuditunit(String auditunit) {
        this.auditunit = auditunit;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CSS_PC_ID")
    public PoliceClassification getPoliceClassification() {
        return policeClassification;
    }

    public void setPoliceClassification(PoliceClassification policeClassification) {
        this.policeClassification = policeClassification;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CSS_STATION_ID")
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
