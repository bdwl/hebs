package com.guy.statistics.opinfo.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.opinfo.entity.Opinfo;

/**
 * 类名称：OpinfoDao 创建人：blank 创建时间：2016-06-08
 */
@Repository
public class OpinfoDao extends HibernateDao<Opinfo, Integer> {

    /**
     * 状态导出待确定 确定状态
     * @param start
     * @param end
     * @return
     */
    public List<Opinfo> getNew(Date start, Date end) {
        String hql="";
        Query query=null;
        if(start==null&&end==null){
            hql="from Opinfo where status='N'";
            query=createQuery(hql);
        }else{
            hql="from Opinfo where createTime>=?0 and createTime<=?1 and status='N' and detail<>'[删除附件]重新上传'";
            query=createQuery(hql,start,end);
        }
        @SuppressWarnings("unchecked")
        List<Opinfo> list=query.list();
        return  list;
    }

    public List<Opinfo> getDel(Date start, Date end) {
        String hql="";
        Query query=null;
        if(start==null&&end==null){
            hql="from Opinfo where status='D'";
            query=createQuery(hql);
        }else{
            hql="from Opinfo where createTime>=?0 and createTime<=?1 and status='D'";
            query=createQuery(hql,start,end);
        }
        @SuppressWarnings("unchecked")
        List<Opinfo> list=query.list();
        return  list;
    }

    public List<Opinfo> getRe(Date start, Date end) {
        String hql="";
        Query query=null;
        if(start==null&&end==null){
            hql="from Opinfo where status='F'";
            query=createQuery(hql);
        }else{
            hql="from Opinfo where createTime>=?0 and createTime<=?1 and status='F'";
            query=createQuery(hql,start,end);
        }
        @SuppressWarnings("unchecked")
        List<Opinfo> list=query.list();
        return  list;
    }

}
