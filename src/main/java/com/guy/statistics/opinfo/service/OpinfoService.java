package com.guy.statistics.opinfo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.opinfo.dao.OpinfoDao;
import com.guy.statistics.opinfo.entity.Opinfo;

/** 
 * 类名称：OpinfoService
 * 创建人：blank 
 * 创建时间：2016-06-08
 */
@Service
@Transactional(readOnly=true)
public class OpinfoService extends BaseService<Opinfo, Integer> {
	
	@Autowired
	private OpinfoDao opinfoDao;

	@Override
	public HibernateDao<Opinfo, Integer> getEntityDao() {
		return opinfoDao;
	}
	
	/**
	 * 新增
	 * @return
	 */
	public List<Opinfo> getOpinfosN(Date start,Date end){
	    return  opinfoDao.getNew(start,end);
	}
	
	/**
	 * 删除
	 * @return
	 */
	public List<Opinfo> getOpinfosD(Date start,Date end){
	    return  opinfoDao.getDel(start,end);
	}
	
	/**
	 * 重新授权
	 * @return
	 */
	public List<Opinfo> getOpinfosR(Date start,Date end){
        return opinfoDao.getRe(start,end);
	}
}
