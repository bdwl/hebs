package com.guy.statistics.opinfo.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.guy.statistics.cssuser.entity.CSSUser;

/** 
 * 类名称：OpinfoEntity
 * 创建人：blank 
 * 创建时间：2016-06-08
 */
@Entity
@Table(name = "t_css_opinfo")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate @DynamicInsert
public class Opinfo implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Timestamp createTime;
	private String status;
	private String detail;
	private CSSUser cssUser;
	// Constructors

	/** default constructor */
	public Opinfo() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    @Column(name = "CREATE_TIME", nullable = false, length = 19,columnDefinition="datetime default CURRENT_TIMESTAMP")
	public Timestamp getCreateTime() {
        return createTime;
    }

	
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "DETAIL", length = 1000)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CSS_USER_ID")
    public CSSUser getCssUser() {
        return cssUser;
    }

    public void setCssUser(CSSUser cssUser) {
        this.cssUser = cssUser;
    }
	
	
}