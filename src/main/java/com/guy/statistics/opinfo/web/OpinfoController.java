package com.guy.statistics.opinfo.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.opinfo.entity.Opinfo;
import com.guy.statistics.opinfo.service.OpinfoService;

/** 
 * 类名称：OpinfoController
 * 创建人：blank 
 * 创建时间：2016-06-08
 */
@Controller
@RequestMapping(value="statistics/opinfo")
public class OpinfoController extends BaseController {

	@Autowired
	OpinfoService opinfoService;
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list() {
		return "statistics/opinfoList";
	}

	/**
	 * 获取json
	 */
	@RequiresPermissions("statistics:opinfo:view")
	@RequestMapping(value="json",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> opinfoList(HttpServletRequest request) {
		Page<Opinfo> page = getPage(request);
		page.setOrder(Page.DESC);
		page.setOrderBy("createTime");
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
		page = opinfoService.search(page, filters);
		return getEasyUIData(page);
	}
	
	/**
	 * 添加跳转
	 * 
	 * @param model
	 */
	@RequiresPermissions("statistics:opinfo:add")
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		model.addAttribute("opinfo", new Opinfo());
		model.addAttribute("action", "create");
		return "statistics/opinfoForm";
	}

	/**
	 * 添加字典
	 * 
	 * @param opinfo
	 * @param model
	 */
	@RequiresPermissions("statistics:opinfo:add")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(@Valid Opinfo opinfo, Model model) {
		opinfoService.save(opinfo);
		return "success";
	}

	/**
	 * 修改跳转
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("statistics:opinfo:update")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("opinfo", opinfoService.get(id));
		model.addAttribute("action", "update");
		return "statistics/opinfoForm";
	}

	/**
	 * 修改
	 * 
	 * @param opinfo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("statistics:opinfo:update")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@Valid @ModelAttribute @RequestBody Opinfo opinfo,Model model) {
		opinfoService.update(opinfo);
		return "success";
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("statistics:opinfo:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id") Integer id) {
		opinfoService.delete(id);
		return "success";
	}
	
	@ModelAttribute
	public void getOpinfo(@RequestParam(value = "id", defaultValue = "-1") Integer id,Model model) {
		if (id != -1) {
			model.addAttribute("opinfo", opinfoService.get(id));
		}
	}

}
