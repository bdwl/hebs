package com.guy.statistics.policeclassification.web;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.statistics.policeclassification.entity.PoliceClassification;
import com.guy.statistics.policeclassification.service.PoliceClassificationService;
import com.guy.util.excel.ExcelUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类名称：PoliceClassificationController
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Controller
@RequestMapping(value = "statistics/policeclassification")
public class PoliceClassificationController extends BaseController {

    @Autowired
    PoliceClassificationService policeclassificationService;

    /**
     * 默认页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list() {
        return "statistics/policeclassificationList";
    }

    /**
     * 获取json
     */
    @RequiresPermissions("statistics:policeclassification:view")
    @RequestMapping(value = "json", method = RequestMethod.POST)
    @ResponseBody
    public Map
            <String, Object> policeclassificationList(HttpServletRequest request) {
        Page<PoliceClassification> page = getPage(request);
        List
                <PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
        page = policeclassificationService.search(page, filters);
        return getEasyUIData(page);
    }
    /**
     * 获取json
     */
    @RequiresPermissions("statistics:policeclassification:view")
    @RequestMapping(value = "jsonAll", method = RequestMethod.POST)
    @ResponseBody
    public List<PoliceClassification> policeclassificationAllList(HttpServletRequest request) {

        return policeclassificationService.getAll();
    }

    /**
     * 添加跳转
     *
     * @param model
     */
    @RequiresPermissions("statistics:policeclassification:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
        model.addAttribute("policeclassification", new PoliceClassification());
        model.addAttribute("action", "create");
        return "statistics/policeclassificationForm";
    }

    /**
     * 添加字典
     *
     * @param policeclassification
     * @param model
     */
    @RequiresPermissions("statistics:policeclassification:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid PoliceClassification policeclassification, Model model) {
        policeclassificationService.save(policeclassification);
        return "success";
    }

    /**
     * 修改跳转
     *
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:policeclassification:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("policeclassification", policeclassificationService.get(id));
        model.addAttribute("action", "update");
        return "statistics/policeclassificationForm";
    }

    /**
     * 修改
     *
     * @param policeclassification
     * @param model
     * @return
     */
    @RequiresPermissions("statistics:policeclassification:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody PoliceClassification policeclassification, Model model) {
        policeclassificationService.update(policeclassification);
        return "success";
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequiresPermissions("statistics:policeclassification:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        policeclassificationService.delete(id);
        return "success";
    }


    @RequiresPermissions("statistics:policeclassification:import")
    @RequestMapping(value = "import")
    @ResponseBody
    public String importExcel(@RequestParam(value = "sj") MultipartFile sj, HttpServletRequest request) throws Exception {
        String result = null;
        InputStream file = sj.getInputStream();

        try {
            List<List<Object>> list = ExcelUtil.readExcelByList(file, 1);
            file.close();
            List<String> codes = new ArrayList<String>();
            for (List<Object> entity : list) {
                codes.add(entity.get(0).toString());
            }
            if (!codes.isEmpty()) {
                for (List<Object> entity : list) {
                    PoliceClassification station = policeclassificationService.findPcByCode(entity.get(1).toString().trim());
                    if (station == null) {
                        station = new PoliceClassification();
                        station.setPccode(entity.get(1).toString().trim());
                        station.setPcname(entity.get(0).toString().trim());
                        policeclassificationService.save(station);
                    } else {
                        station.setPcname(entity.get(0).toString().trim());
                        policeclassificationService.update(station);
                    }
                }
            }
            result = "success";
        } catch (Exception e) {
            result = "数据异常！";
        }
        return result;
    }

    @ModelAttribute
    public void getPoliceClassification(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
        if (id != -1) {
            model.addAttribute("policeclassification", policeclassificationService.get(id));
        }
    }

}
