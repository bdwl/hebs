package com.guy.statistics.policeclassification.service;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.statistics.policeclassification.dao.PoliceClassificationDao;
import com.guy.statistics.policeclassification.entity.PoliceClassification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 类名称：PoliceClassificationService
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Service
@Transactional(readOnly = true)
public class PoliceClassificationService extends BaseService<PoliceClassification, Integer> {

    @Autowired
    private PoliceClassificationDao policeclassificationDao;

    @Override
    public HibernateDao<PoliceClassification, Integer> getEntityDao() {
        return policeclassificationDao;
    }

    public PoliceClassification findPcByCode(String code) {
        return policeclassificationDao.findUnique("from PoliceClassification where pccode=?0", code);
    }
    public PoliceClassification findPcByName(String name) {
        return policeclassificationDao.findUnique("from PoliceClassification where pcname=?0", name);
    }
}
