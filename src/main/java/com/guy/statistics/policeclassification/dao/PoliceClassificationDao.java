package com.guy.statistics.policeclassification.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.statistics.policeclassification.entity.PoliceClassification;

/**
 * 类名称：PoliceClassificationDao
 * 创建人：blank
 * 创建时间：2018-05-15
 */
@Repository
public class PoliceClassificationDao extends HibernateDao<PoliceClassification, Integer> {

}
