package com.guy.system.areainfo.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.guy.statistics.cssuser.entity.CSSUser;

/**
 * 区域entity
 * @author blank
 * @date 2015年5月9日 
 */
@Entity
@Table(name = "sys_area_info",indexes={ @Index(columnList = "area_code"), @Index(columnList = "area_name")})
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate @DynamicInsert
public class AreaInfo implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String areaCode;
    private String areaName;
    private Integer pid;
    private Integer sort;
    @JsonIgnore
    private Set<CSSUser> cssUsers = new HashSet<CSSUser>(0);

    // Constructors

    /** default constructor */
    public AreaInfo() {
    }

    /** minimal constructor */
    public AreaInfo(String areaCode, String areaName, Integer pid) {
        this.areaCode = areaCode;
        this.areaName = areaName;
        this.pid = pid;
    }

    /** full constructor */
    public AreaInfo(String areaCode, String areaName, Integer pid, Integer sort) {
        this.areaCode = areaCode;
        this.areaName = areaName;
        this.pid = pid;
        this.sort = sort;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "AREA_CODE", nullable = false, length = 12)
    public String getAreaCode() {
        return this.areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    @Column(name = "AREA_NAME", nullable = false, length = 50)
    public String getAreaName() {
        return this.areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Column(name = "PID", nullable = false)
    public Integer getPid() {
        return this.pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    @Column(name = "SORT")
    public Integer getSort() {
        return this.sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @OneToMany(cascade = { CascadeType.REFRESH, CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "areaInfo")
    public Set<CSSUser> getCssUsers() {
        return cssUsers;
    }

    public void setCssUsers(Set<CSSUser> cssUsers) {
        this.cssUsers = cssUsers;
    }
    
    

}