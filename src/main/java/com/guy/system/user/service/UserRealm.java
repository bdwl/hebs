package com.guy.system.user.service;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import com.google.common.base.Objects;
import com.guy.common.utils.PropertiesLoader;
import com.guy.common.utils.UserUtils;
import com.guy.common.utils.security.Encodes;
import com.guy.system.permission.entity.Permission;
import com.guy.system.permission.service.PermissionService;
import com.guy.system.user.entity.User;
import com.guy.system.user.entity.UserRole;
import com.guy.system.utils.CaptchaException;
import com.guy.system.utils.UsernamePasswordCaptchaToken;

/**
 * 用户登录授权service(shrioRealm)
 * @author blank
 * @date 2015年1月14日
 */
@Service
@DependsOn({"userDao","permissionDao","rolePermissionDao"})
public class UserRealm extends AuthorizingRealm {

	@Autowired
	private UserService userService;
	
	@Autowired
	private PermissionService permissionService;

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
	    
		UsernamePasswordCaptchaToken token=null;
        User user=null;
        try {
            token = (UsernamePasswordCaptchaToken) authcToken; 
            user = userService.getUser(token.getUsername());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		if (user != null&&doCaptchaValidate(token)) {
			byte[] salt = Encodes.decodeHex(user.getSalt());
			ShiroUser shiroUser=new ShiroUser(user.getId(), user.getLoginName(), user.getName(),token.getHost());
			//设置用户session
			Session session =SecurityUtils.getSubject().getSession();
			session.setAttribute("user", user);
            //读取websocket配置
            String fileName= "/websocket.properties";
            PropertiesLoader loader= new PropertiesLoader(fileName);
            session.setAttribute("WIMIP",  loader.getProperty("WIMIP"));
            session.setAttribute("WIMPORT",  loader.getProperty("WIMPORT"));
            session.setAttribute("OLIP",  loader.getProperty("OLIP"));
            session.setAttribute("OLPORT",  loader.getProperty("OLPORT"));
			//ckfinder 添加权限
			//start
			Set<UserRole> userRoles=user.getUserRoles();
			for(UserRole userRole:userRoles){
				String roleName=userRole.getRole().getName();
				if(roleName.equals("admin")){
					session.setAttribute("CKFinder_UserRole", "admin");
				}else{
					session.setAttribute("CKFinder_UserRole", "user");
				}
			}
			//end
			 user.setLoginIP(shiroUser.getHost());
		        //设置登录次数、时间
		      userService.updateUserLogin(user);
			//ckfinder 添加权限
			return new SimpleAuthenticationInfo(shiroUser,user.getPassword(), ByteSource.Util.bytes(salt), getName());
		} else {
			return null;
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//		Principal principal = (Principal) getAvailablePrincipal(principals);
		ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
		User user = userService.getUser(shiroUser.loginName);
		//把principals放session中 key=userId value=principals
		SecurityUtils.getSubject().getSession().setAttribute(String.valueOf(user.getId()),SecurityUtils.getSubject().getPrincipals());
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		//赋予角色
		for(UserRole userRole:user.getUserRoles()){
			info.addRole(userRole.getRole().getName());
		}
		//赋予权限
		for(Permission permission:permissionService.getPermissions(user.getId())){
			if(StringUtils.isNotBlank(permission.getPermCode()))
			info.addStringPermission(permission.getPermCode());
		}
//		user.setLoginIP(shiroUser.getHost());
//		//设置登录次数、时间
//		userService.updateUserLogin(user);
		return info;
	}
	
	/**
	 * 验证码校验
	 * @param token
	 * @return boolean
	 */
	protected boolean doCaptchaValidate(UsernamePasswordCaptchaToken token)
	{
		String captcha = (String) SecurityUtils.getSubject().getSession().getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		if (captcha != null &&!captcha.equalsIgnoreCase(token.getCaptcha())){
			throw new CaptchaException("验证码错误！");
		}
		return true;
	}
		

	/**
	 * 设定Password校验的Hash算法与迭代次数.
	 */
	@SuppressWarnings("static-access")
	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher(userService.HASH_ALGORITHM);
		matcher.setHashIterations(userService.HASH_INTERATIONS);
		setCredentialsMatcher(matcher);
	}

	/**
	 * 自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息.
	 */
	public static class ShiroUser implements Serializable {
		private static final long serialVersionUID = -1373760761780840081L;
		public Integer id;
		public String loginName;
		public String name;
		public String realName;
		public String host;

		
		public ShiroUser(Integer id, String loginName, String name,String host) {
			this.id = id;
			this.loginName = loginName;
			this.name = name;
			this.host=host;
		}

		public Integer getId(){
			return id;
		}

		public String getName() {
			return name;
		}

		
		public String getHost() {
			return host;
		}

		/**
		 * 本函数输出将作为默认的<shiro:principal/>输出.
		 */
		@Override
		public String toString() {
			return loginName;
		}

		/**
		 * 重载hashCode,只计算loginName;
		 */
		@Override
		public int hashCode() {
			return Objects.hashCode(loginName);
		}

		/**
		 * 重载equals,只计算loginName;
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ShiroUser other = (ShiroUser) obj;
			if (loginName == null) {
				if (other.loginName != null) {
					return false;
				}
			} else if (!loginName.equals(other.loginName)) {
				return false;
			}
			return true;
		}
	}
	
	@Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }
 
    
	/**
	 * 授权用户信息
	 */
	public static class Principal implements Serializable {

		private static final long serialVersionUID = 1L;
		
		private Integer id; // 编号
		private String loginName; // 登录名
		private String name; // 姓名
		private boolean mobileLogin; // 是否手机登录
		
//		private Map<String, Object> cacheMap;

		public Principal(User user, boolean mobileLogin) {
			this.id = user.getId();
			this.loginName = user.getLoginName();
			this.name = user.getName();
			this.mobileLogin = mobileLogin;
		}

		public Integer getId() {
			return id;
		}

		public String getLoginName() {
			return loginName;
		}

		public String getName() {
			return name;
		}

		public boolean isMobileLogin() {
			return mobileLogin;
		}

//		@JsonIgnore
//		public Map<String, Object> getCacheMap() {
//			if (cacheMap==null){
//				cacheMap = new HashMap<String, Object>();
//			}
//			return cacheMap;
//		}

		/**
		 * 获取SESSIONID
		 */
		public String getSessionid() {
			try{
				return (String) UserUtils.getSession().getId();
			}catch (Exception e) {
				return "";
			}
		}
		

	}
}
