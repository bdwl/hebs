package com.guy.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class codeMerge {
	public static void main(String[] args) {
		File folder = new File("D:\\work\\hebs\\src\\main\\java\\com\\guy\\statistics");// 要统计代码的工程路径
		File outFile = new File("d:\\out.txt");// 输出目标文件

		recur(folder, outFile);
		System.out.println("merge done");
	}

	/**
	 * 递归工程目录，找到所有的java源文件并合并输出
	 * 
	 * @param file
	 *            源代码目录或文件
	 * @param outFile
	 *            目标txt文件，本方法中用它参数完全是因为merge方法需要该参数
	 */
	public static void recur(File file, File outFile) {
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 如果files[i]为目录，则递归，直到最里面那一层
			if (files[i].isDirectory() == true) {
				recur(files[i], outFile);
			}
			// 如果files[i]为文件且为java源文件，则读取其代码并合并 用到了正则表达式
			else if ((files[i].isFile() == true) && files[i].getName().matches(".*\\.java$")) {
				merge(files[i], outFile);
			}
		}
	}

	/**
	 * 读取java源文件中的每一行代码，输出至目标文件
	 * 
	 * @param file
	 *            要读取的java源文件
	 * @param outFile
	 *            目标文件 .txt文件
	 */
	public static void merge(File file, File outFile) {
		// 缓冲输出和输入流
		BufferedWriter bw = null;
		BufferedReader br = null;

		try {
			// 注意此处用了FileWriter带参数true的构造方法，这样才能在.txt文件的末尾加入内容，否则会覆盖其以前的内容
			bw = new BufferedWriter(new FileWriter(outFile, true));
			br = new BufferedReader(new FileReader(file));

			// 读取一个文件第一行代码之前，先在.txt文件中做好标记，表明此处是哪个源文件
			bw.newLine();
			bw.write("读取文件" + file.getName());
			bw.newLine();

			String line = "";
			while ((line = br.readLine()) != null) {

				bw.write(line);// 挨行从源文件读取并写入目标文件
				bw.newLine();
				bw.flush();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null) {
					// 注意：此处的两句代码不能调换，若bw=null在前，则会出现空指针错误
					bw.close();
					bw = null;
				}
				if (br != null) {
					br.close();
					br = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}