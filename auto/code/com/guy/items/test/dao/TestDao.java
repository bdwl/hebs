package com.guy.items.test.dao;

import org.springframework.stereotype.Repository;

import com.guy.common.persistence.HibernateDao;
import com.guy.items.test.entity.Test;

/**
* 类名称：TestDao
* 创建人：blank
* 创建时间：2017-04-07
*/
@Repository
public class TestDao extends HibernateDao<Test, Integer>{

}
