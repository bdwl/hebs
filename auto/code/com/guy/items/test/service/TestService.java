package com.guy.items.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guy.common.persistence.HibernateDao;
import com.guy.common.service.BaseService;
import com.guy.items.test.dao.TestDao;
import com.guy.items.test.entity.Test;

/**
* 类名称：TestService
* 创建人：blank
* 创建时间：2017-04-07
*/
@Service
@Transactional(readOnly=true)
public class TestService extends BaseService<Test, Integer> {

@Autowired
private TestDao testDao;

@Override
public HibernateDao<Test, Integer> getEntityDao() {
return testDao;
}
}
