package com.guy.items.test.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guy.common.persistence.Page;
import com.guy.common.persistence.PropertyFilter;
import com.guy.common.web.BaseController;
import com.guy.items.test.entity.Test;
import com.guy.items.test.service.TestService;

/**
* 类名称：TestController
* 创建人：blank
* 创建时间：2017-04-07
*/
@Controller
@RequestMapping(value="items/test")
public class TestController extends BaseController {

@Autowired
TestService testService;
/**
* 默认页面
*/
@RequestMapping(method = RequestMethod.GET)
public String list() {
return "items/testList";
}

/**
* 获取json
*/
@RequiresPermissions("items:test:view")
@RequestMapping(value="json",method = RequestMethod.POST)
@ResponseBody
public Map
<String, Object> testList(HttpServletRequest request) {
Page<Test> page = getPage(request);
List
<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
    page = testService.search(page, filters);
    return getEasyUIData(page);
    }

    /**
    * 添加跳转
    *
    * @param model
    */
    @RequiresPermissions("items:test:add")
    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createForm(Model model) {
    model.addAttribute("test", new Test());
    model.addAttribute("action", "create");
    return "items/testForm";
    }

    /**
    * 添加字典
    *
    * @param test
    * @param model
    */
    @RequiresPermissions("items:test:add")
    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@Valid Test test, Model model) {
testService.save(test);
    return "success";
    }

    /**
    * 修改跳转
    *
    * @param id
    * @param model
    * @return
    */
    @RequiresPermissions("items:test:update")
    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Integer id, Model model) {
    model.addAttribute("test", testService.get(id));
    model.addAttribute("action", "update");
    return "items/testForm";
    }

    /**
    * 修改
    *
    * @param test
    * @param model
    * @return
    */
    @RequiresPermissions("items:test:update")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@Valid @ModelAttribute @RequestBody Test test,Model model) {
testService.update(test);
    return "success";
    }

    /**
    * 删除
    *
    * @param id
    * @return
    */
    @RequiresPermissions("items:test:delete")
    @RequestMapping(value = "delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
testService.delete(id);
    return "success";
    }

    @ModelAttribute
    public void getTest(@RequestParam(value = "id", defaultValue = "-1") Integer id,Model model) {
    if (id != -1) {
    model.addAttribute("test", testService.get(id));
    }
    }

    }
